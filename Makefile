CC = cl
CFLAGS = /EHsc /W3 /MD /O2
LFLAGS =
OBJFLAGS = /c /Fo
LIBFLAGS = /LD
TARGET = heuristika_titulku.exe
SHAREDBASIC = chyba.obj chyby.obj slovnik.obj titulek.obj titulky.obj plugin.obj pluginy.obj timer.obj konfigurace.obj text.obj logger.obj
SHARED = $(SHAREDBASIC) log_system.lib konf_system.lib
SHAREDDEP = $(SHAREDBASIC) log_system.dll konf_system.dll
PLUGINS = libdlouhy_titulek.dll libkratke_zobrazeni.dll libprekryvajici_titulky.dll libanglicka_notace_cisel.dll libmale_pismeno.dll libspatne_mezery.dll libspatne_uvozovky.dll libcisla_zapsana_slovne.dll libcasta_doplnkova_slova.dll libspatna_pomlcka.dll libspatny_zapis_cisel.dll libkontrola_preklepu_wordlist.dll libkontrola_preklepu_kombinace.dll
PLUGINDEST = .\pluginy
SRCPLUGINS = .\src_plugins
LIBDEP = create_plugin_dest $(SHAREDDEP)
INCLBOOST = /I "C:\Program Files (x86)\boost\boost_1_57_0"
LNKBOOST = /link /LIBPATH:"C:\Program Files (x86)\boost\boost_1_57_0\stage\lib"


##################################################
#            PREKLAD RIDICIHO PROGRAMU           #
##################################################

$(TARGET): main.cpp engine.h engine.cpp $(SHAREDDEP) $(PLUGINS)
	$(CC) $(CFLAGS) $(LFLAGS) $(SHARED) main.cpp engine.cpp /Fe$@ $(INCLBOOST) $(LNKBOOST)

log_system.dll: logger.obj ./shared/log_system.cpp ./shared/log_system.h
	$(CC) $(CFLAGS) $(LIBFLAGS) /D EXPORT_HEURISTIKA_BUILD ./shared/log_system.cpp logger.obj $(INCLBOOST) $(LNKBOOST)
	
konf_system.dll: konfigurace.obj ./shared/konf_system.cpp ./shared/konf_system.h
	$(CC) $(CFLAGS) $(LIBFLAGS) /D EXPORT_HEURISTIKA_BUILD ./shared/konf_system.cpp konfigurace.obj $(INCLBOOST) $(LNKBOOST)
	
create_plugin_dest:
	-mkdir $(PLUGINDEST)

chyba.obj: ./shared/chyba.cpp ./shared/chyba.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/chyba.cpp $(INCLBOOST)

chyby.obj: ./shared/chyby.cpp ./shared/chyby.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/chyby.cpp $(INCLBOOST)

slovnik.obj: ./shared/slovnik.cpp ./shared/slovnik.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/slovnik.cpp $(INCLBOOST)

titulek.obj: ./shared/titulek.cpp ./shared/titulek.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/titulek.cpp $(INCLBOOST)

titulky.obj: ./shared/titulky.cpp ./shared/titulky.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/titulky.cpp $(INCLBOOST)

plugin.obj: ./shared/plugin.cpp ./shared/plugin.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/plugin.cpp $(INCLBOOST)

pluginy.obj: ./shared/pluginy.cpp ./shared/pluginy.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/pluginy.cpp $(INCLBOOST)

timer.obj: ./shared/timer.cpp ./shared/timer.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/timer.cpp

konfigurace.obj: ./shared/konfigurace.cpp ./shared/konfigurace.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/konfigurace.cpp $(INCLBOOST)

text.obj: ./shared/text.cpp ./shared/text.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/text.cpp $(INCLBOOST)
	
logger.obj: ./shared/logger.cpp ./shared/logger.h
	$(CC) $(CFLAGS) $(OBJFLAGS) ./shared/logger.cpp $(INCLBOOST)


##################################################
#                PREKLAD KNIHOVEN                #
##################################################
# SABLONA:
#  $(LIB): $(LIBFILES) $(LIBDEP)
#  	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@
#

LIBFILES3 = $(SRCPLUGINS)\dlouhy_titulek\dlouhy_titulek.cpp
libdlouhy_titulek.dll: $(LIBFILES3) $(LIBDEP) $(SRCPLUGINS)\dlouhy_titulek\dlouhy_titulek.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES3) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES4 = $(SRCPLUGINS)\kratke_zobrazeni\kratke_zobrazeni.cpp $(SRCPLUGINS)\doba_zobrazeni.cpp
libkratke_zobrazeni.dll: $(LIBFILES4) $(LIBDEP) $(SRCPLUGINS)\kratke_zobrazeni\kratke_zobrazeni.h $(SRCPLUGINS)\doba_zobrazeni.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES4) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES5 = $(SRCPLUGINS)\prekryvajici_titulky\prekryvajici_titulky.cpp $(SRCPLUGINS)\doba_zobrazeni.cpp
libprekryvajici_titulky.dll: $(LIBFILES5) $(LIBDEP) $(SRCPLUGINS)\prekryvajici_titulky\prekryvajici_titulky.h $(SRCPLUGINS)\doba_zobrazeni.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES5) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES6 = $(SRCPLUGINS)\anglicka_notace_cisel\anglicka_notace_cisel.cpp
libanglicka_notace_cisel.dll: $(LIBFILES6) $(LIBDEP) $(SRCPLUGINS)\anglicka_notace_cisel\anglicka_notace_cisel.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES6) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES7 = $(SRCPLUGINS)\male_pismeno\male_pismeno.cpp
libmale_pismeno.dll: $(LIBFILES7) $(LIBDEP) $(SRCPLUGINS)\male_pismeno\male_pismeno.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES7) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES8 = $(SRCPLUGINS)\spatne_mezery\spatne_mezery.cpp
libspatne_mezery.dll: $(LIBFILES8) $(LIBDEP) $(SRCPLUGINS)\spatne_mezery\spatne_mezery.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES8) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES9 = $(SRCPLUGINS)\spatne_uvozovky\spatne_uvozovky.cpp
libspatne_uvozovky.dll: $(LIBFILES9) $(LIBDEP) $(SRCPLUGINS)\spatne_uvozovky\spatne_uvozovky.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES9) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES10 = $(SRCPLUGINS)\cisla_zapsana_slovne\cisla_zapsana_slovne.cpp $(SRCPLUGINS)\cisla_zapsana_slovne\cislovky.cpp
libcisla_zapsana_slovne.dll: $(LIBFILES10) $(LIBDEP) $(SRCPLUGINS)\cisla_zapsana_slovne\cisla_zapsana_slovne.h $(SRCPLUGINS)\cisla_zapsana_slovne\cislovky.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES10) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES11 = $(SRCPLUGINS)\casta_doplnkova_slova\casta_doplnkova_slova.cpp
libcasta_doplnkova_slova.dll: $(LIBFILES11) $(LIBDEP) $(SRCPLUGINS)\casta_doplnkova_slova\casta_doplnkova_slova.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES11) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@
	
LIBFILES12 = $(SRCPLUGINS)\spatna_pomlcka\spatna_pomlcka.cpp
libspatna_pomlcka.dll: $(LIBFILES12) $(LIBDEP) $(SRCPLUGINS)\spatna_pomlcka\spatna_pomlcka.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES12) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES13 = $(SRCPLUGINS)\kontrola_preklepu_wordlist\kontrola_preklepu_wordlist.cpp $(SRCPLUGINS)\nejlepsi_opravy.cpp
libkontrola_preklepu_wordlist.dll: $(LIBFILES13) $(LIBDEP) $(SRCPLUGINS)\kontrola_preklepu_wordlist\kontrola_preklepu_wordlist.h $(SRCPLUGINS)\nejlepsi_opravy.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES13) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES14 = $(SRCPLUGINS)\spatny_zapis_cisel\spatny_zapis_cisel.cpp
libspatny_zapis_cisel.dll: $(LIBFILES14) $(LIBDEP) $(SRCPLUGINS)\spatny_zapis_cisel\spatny_zapis_cisel.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES14) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@

LIBFILES15 = $(SRCPLUGINS)\kontrola_preklepu_kombinace\kontrola_preklepu_kombinace.cpp $(SRCPLUGINS)\nejlepsi_opravy.cpp
libkontrola_preklepu_kombinace.dll: $(LIBFILES15) $(LIBDEP) $(SRCPLUGINS)\kontrola_preklepu_kombinace\kontrola_preklepu_kombinace.h $(SRCPLUGINS)\nejlepsi_opravy.h
	$(CC) $(CFLAGS) $(LIBFLAGS) $(LIBFILES15) $(SHARED) $(IMPORT_SYMBOLS) $(INCLBOOST) $(LNKBOOST) /out:$(PLUGINDEST)\$@


##################################################
#                   CLEAN APOD.                  #
##################################################

clean:
	del *.dll *.obj *.lib *.exp $(TARGET)
	-rmdir /S /Q $(PLUGINDEST)
