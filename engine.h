#ifndef ENGINE_H
#define ENGINE_H
#define BOOST_FILESYSTEM_NO_DEPRECATED

#include "./shared/titulky.h"
#include "./shared/pluginy.h"
#include "./shared/konfigurace.h"
#include <boost/algorithm/string.hpp>

/**
 * Hlavni trida, ktera se stara o beh programu a volani jednotlivych soucasti a funkci.
 * K jejimu vykonani je treba ji zkonstruovat a zavolat funkci Run().
 */
class Engine
{
public:
    /**
     * Vsechny defaultni metody jsou zakazany.
     */
    Engine() = delete;
    Engine(const Engine& source) = delete;
    Engine& operator=(const Engine& source) = delete;
    Engine(Engine&& source) = delete;
    Engine& operator=(Engine&& source) = delete;

    /**
     * Jedina moznost konstrukce objektu Engine, je nutne mu predat parametry, ktere byly zadany programu pri spusteni.
     * @param parametry std::vector<std::string> ve kterem jsou ulozeny rozparsovane parametry z prikazove radky.
     * Ukazka pouziti ve funkci main():
     * @code
     * std::vector<std::string> options(argv, argv + argc);
     * Engine engine(options);
     * @endcode
     */
    Engine(StringVector parametry);

    /**
     * Funkce zodpovedna za samotne provedeni vsech potrebnych veci pred a po spusteni jednotlivych pluginu.
     * Nacita soubor titulku, nacita pluginy, vsechny je spusti a ulozi vysledny soubor s popisem chyb.
     */
    void Run();

private:
    void Uvod();
    void Konec();
    void Help();
    int NactiSoubor();
    int NactiPluginy();
    void Error(const string_t& msg);
    int Analyzuj();
    int UlozSoubor();
    void ZpracujParametry();

    /**
     * Pripravi na nacteni noveho souboru a provedeni nove analyzy se stavajicimi pluginy.
     */
    void Reset();
    /**
     * Pripravi a zaroven vse potrebne nastavi na nove spusteni se stavajicimi pluginy.
     * @param input_soubor urcuje vstupni soubor, pokud bude prazdne, pak se bude cist ze standardniho vstupu
     * @param output_soubor vystupni soubor, opet pokud bude prazdny, pak se vypise na standardni vystup
     * @param format_vystup 0 == XML, 1 == JSON
     * @param input_charset nazev kodovani ve kterem je vstup
     */
    void NewInit(string_t input_soubor = "", string_t output_soubor = "", int format_vystup = 0, string_t input_charset = "");

    void OrezKonecRadky(string_t& radka);
    void OrezUTFBom(string_t& radka);


    StringVector Parametry_;
    string_t NazevProgramu_;
    string_t InputCharset_;
    string_t InputSoubor_;
    string_t OutputSoubor_;
    std::set<string_t> IncludePluginy_;
    std::set<string_t> ExcludePluginy_;

    /*
      FormatVystup_:
       0 => XML
       1 => JSON
    */
    int FormatVystup_;
    bool OutputFormat_; // true pokud byl pouzit parametr --output-format

    Titulky Titulky_;
    Pluginy Pluginy_;
    Chyby Chyby_;
};

#endif // ENGINE_H
