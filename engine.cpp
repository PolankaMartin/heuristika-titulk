#include "engine.h"

Engine::Engine(StringVector parametry)
    : Parametry_(parametry.begin()+1,parametry.end()), NazevProgramu_(parametry.at(0)), InputCharset_(""), InputSoubor_(""),
      OutputSoubor_(""), IncludePluginy_(), ExcludePluginy_(), FormatVystup_(0), OutputFormat_(false)
{
    ZpracujParametry();
}

void Engine::Run()
{
    Uvod();

    NactiSoubor();

    NactiPluginy();

    Analyzuj();

    UlozSoubor();

    Konec();
}

int Engine::Analyzuj()
{
    LogSystem::VratLog().Zapis("engine", "Zahajuji analyzu...");

    Pluginy_.Run(Titulky_, Chyby_);

    LogSystem::VratLog().Zapis("engine", "Analyza dokoncena.");

    return 0;
}

void Engine::Uvod()
{
    LogSystem::VratLog().Zapis("engine", "Vitejte");
}

void Engine::Konec()
{
    LogSystem::VratLog().Zapis("engine", "Program bude nyni ukoncen.");
}

int Engine::NactiSoubor()
{
    LogSystem::VratLog().Zapis("engine", "Nacitam soubor...");

    std::istream* input;
    std::ifstream input_soubor;
    size_t cislo_radky = 0;
    size_t id_titulku = 0;

    if(InputSoubor_ == "")
    {
        input = &std::cin;
    }
    else
    {
        input_soubor.open(InputSoubor_);
        input = &input_soubor;
    }

    string_t radka;

    if(!getline(*input, radka))
    {
        Error("Prazdny soubor!");
    }
    cislo_radky++;

    OrezUTFBom(radka);
    OrezKonecRadky(radka);
    while(radka == "")
    {
        if(!getline(*input, radka)){Error("Prazdny soubor!");}
        cislo_radky++;
        OrezUTFBom(radka);
        OrezKonecRadky(radka);
    }

    while(true)
    {
        string_t zac;
        string_t kon;
        size_t uid = 0;
        long zacatek = 0;
        long konec = 0;
        StringVector text;

        // ENCODING z jineho kodovani do UTF8
        if(InputCharset_ != "" && Text::NaMala(InputCharset_) != "utf8")
        {
            try{
                radka = boost::locale::conv::to_utf<char>(radka, InputCharset_, boost::locale::conv::method_type::stop); }
            catch(boost::locale::conv::invalid_charset_error){
                Error("Neplatne jmeno vstupniho kodovani: " + InputCharset_); }
            catch(boost::locale::conv::conversion_error){
                Error("Chyba pri konverzi textu do UTF8 na radce: " + std::to_string(cislo_radky)); }
        }
		else // kodovani by melo byt UTF8, radsi zkontrolujeme
		{
			if (!utf8::is_valid(radka.begin(), radka.end()))
			{
				Error("Radka cislo " + std::to_string(cislo_radky) + " je nevalidni s UTF8");
			}
		}

        try
        { // na radce kde melo byt cislo titulku nebylo cislo
            uid = std::stoi(radka);
        }
        catch(...)
        {
            Error("Na radce: " + std::to_string(cislo_radky) + " bylo ocekavano cislo titulku!");
        }

        if(uid != ++id_titulku) // kontrola spravnosti ID titulku
        {
            Chyba temp("engine", "Spatne ID titulku na radce: " + std::to_string(cislo_radky), uid, 0);
            temp.PridejOpravu(std::to_string(id_titulku));
            Chyby_.PushBack(temp);

			LogSystem::VratLog().Zapis("engine", "Zaznamenana chyba v titulku: " + std::to_string(uid));
        }

        if(!getline(*input, radka))
        {
            Error("Soubor v nespravnem formatu! Chyba na radce: " + std::to_string(cislo_radky));
        }
        cislo_radky++;
        OrezKonecRadky(radka);

        // ENCODING z jineho kodovani do UTF8
        if(InputCharset_ != "" && Text::NaMala(InputCharset_) != "utf8")
        {
            try{
                radka = boost::locale::conv::to_utf<char>(radka, InputCharset_, boost::locale::conv::method_type::stop); }
            catch(boost::locale::conv::invalid_charset_error){
                Error("Neplatne jmeno vstupniho kodovani: " + InputCharset_); }
            catch(boost::locale::conv::conversion_error){
                Error("Chyba pri konverzi textu do UTF8 na radce: " + std::to_string(cislo_radky)); }
        }
		else // kodovani by melo byt UTF8, radsi zkontrolujeme
		{
			if (!utf8::is_valid(radka.begin(), radka.end()))
			{
				Error("Radka cislo " + std::to_string(cislo_radky) + " je nevalidni s UTF8");
			}
		}

        size_t delim = radka.find("-->");
        if(delim != string_t::npos)
        {
            zac = radka.substr(0,delim);
            kon = radka.substr(delim + 3,radka.length());
        }
        else
        { // radek kde mel byt zacatek a konec titulku ve formatu XX:XX:XX,XXXX --> XX:XX:XX,XXXX nebylo nalezeno "-->"
            Error("Na radce: " + std::to_string(cislo_radky) + " byl ocekavan cas zacatku a konce titulku ve spravnem formatu!");
        }

        while(zac[zac.length() - 1] == ' '){ zac = zac.substr(0,zac.length() - 1); } // odstrani mezery z konce stringu zac
        while(kon[0] == ' '){ kon = kon.substr(1, kon.length() - 1); } // odstrani mezery ze zacatku stringu kon

        try
        { // v pripade chyby v parsovani casu zacatku a konce titulku
            zacatek = Titulek::StringNaLong(zac);
            konec = Titulek::StringNaLong(kon);
        }
        catch(...)
        {
            Error("Na radce: " + std::to_string(cislo_radky) + " byl ocekavan cas zacatku a konce titulku ve spravnem formatu!");
        }

        while(getline(*input, radka))
        {
            cislo_radky++;
            OrezKonecRadky(radka);
            if(radka == "") break;

            // ENCODING z jineho kodovani do UTF8
            if(InputCharset_ != "" && Text::NaMala(InputCharset_) != "utf8")
            {
                try{
                    radka = boost::locale::conv::to_utf<char>(radka, InputCharset_, boost::locale::conv::method_type::stop); }
                catch(boost::locale::conv::invalid_charset_error){
                    Error("Neplatne jmeno vstupniho kodovani: " + InputCharset_); }
                catch(boost::locale::conv::conversion_error){
                    Error("Chyba pri konverzi textu do UTF8 na radce: " + std::to_string(cislo_radky)); }
            }
			else // kodovani by melo byt UTF8, radsi zkontrolujeme
			{
				if (!utf8::is_valid(radka.begin(), radka.end()))
				{
					Error("Radka cislo " + std::to_string(cislo_radky) + " je nevalidni s UTF8");
				}
			}

            if(radka.find("-->") != string_t::npos) // navrh opravy pokud se dva titulky neoddeli prazdnou radkou
            {
                Chyba temp("engine", "Mozna chyba v neoddeleni dvou titulku prazdnym radkem (radek "
                           + std::to_string(cislo_radky) + ")", uid, 0);
                Chyby_.PushBack(temp);

				LogSystem::VratLog().Zapis("engine", "Zaznamenana chyba v titulku: " + std::to_string(uid));
            }

            text.push_back(radka);
        }
        Titulky_.PushBack(Titulek(uid,zacatek,konec,text));

        radka = ""; // pro pripad konce souboru, aby se nepreskocil nasledujici while cyklus

        bool skonci = false;
        while(radka == "")
        {
            if(getline(*input,radka))
            {
                cislo_radky++;
                OrezKonecRadky(radka);
            }
            else{skonci = true; break;}
        }
        if(skonci == false){ OrezKonecRadky(radka); continue; }
        else{break;}
    }

    LogSystem::VratLog().Zapis("engine", "Soubor \"" + InputSoubor_ + "\" (" + std::to_string(cislo_radky) + " radku) byl nacten...");

    return 0;
}

int Engine::NactiPluginy()
{
    LogSystem::VratLog().Zapis("engine", "Nacitam pluginy...");

    Pluginy_.NactiPluginy(IncludePluginy_, ExcludePluginy_);

    LogSystem::VratLog().Zapis("engine", "Pluginy nacteny.");

    return 0;
}

void Engine::Error(const string_t& msg)
{
    string_t error_msg = "Objevila se fatalni chyba: ";
    error_msg += msg;

    LogSystem::VratLog().Zapis("error_system", error_msg);

    std::cerr << error_msg << std::endl;

    exit(1);
    return;
}

void Engine::ZpracujParametry()
{

    /********** BOOST PROGRAM_OPTIONS **********/

    using namespace boost::program_options;

    options_description desc("Parametry programu heuristika titulku");
    desc.add_options()
            ("help,h","Zobrazi tuto napovedu")
            ("log,l",value<string_t>(),"Zapne logovani programu do zadaneho souboru")
            ("input-file,i",value<string_t>(),"Vstupni soubor ve formatu *.srt")
            ("output-file,o",value<string_t>(),"Vystupni soubor ve formatu XML nebo JSON")
            ("output-format,t",value<string_t>(),"Specifikuje format vystupu (moznosti jsou XML a JSON)")
            ("config-file,f",value<string_t>(),"Cesta k souboru s alternativni konfiguraci ve formatu XML nebo JSON")
            ("config,c",value<string_t>(),"Alternativni konfigurace ve formatu XML nebo JSON")
            ("include,p",value<string_t>(),"Pluginy, ktere maji byt nacteny oddelene carkou a bez mezer")
            ("exclude,e",value<string_t>(),"Pluginy, ktere maji byt vynechany, oddelene carkou a bez mezer")
            ("input-charset,a", value<string_t>(), "Specifikuje, jake kodovani ma vstupni soubor, implicitne se predpoklada UTF8");

    positional_options_description p;
    p.add("input-file",-1);
    variables_map vm;

    try
    {
        store(command_line_parser(Parametry_).options(desc).style(
                  command_line_style::default_style |
                  command_line_style::allow_slash_for_short).positional(p).run(),vm);
        notify(vm);
    }
    catch(std::exception& e)
    {
        Error("Chyba v nacitani parametru programu (" + string_t(e.what()) + ")");
    }


    /*** VSE V PORADKU, JEDNOTLIVE PARAMETRY SE MOHOU NACIST A VYHODNOTIT V RAMCI APLIKACE ***/

    if (vm.count("help"))
    {
        //std::cout << desc << std::endl;
        Help();
        exit(1);
    }

    if(vm.count("log"))
    {
       LogSystem::VratLog().Otevrit(vm["log"].as<string_t>());
    }

    if(vm.count("input-file"))
    {
        InputSoubor_ = vm["input-file"].as<string_t>();

        // kontrola koncovky, jestli je spravne nastavena na *.srt
        if(InputSoubor_.length() > 4)
        {
            string_t koncovka = InputSoubor_.substr(InputSoubor_.length()-4);

            koncovka = Text::NaMala(koncovka);

            if(koncovka != ".srt"){ Error("Nepodporovany vstupni format (" + koncovka + ") souboru!"); }
        }
        else{ Error("Nepodporovany nazev souboru (" + InputSoubor_ + ")!"); }
    }

    if(vm.count("output-file"))
    {
        OutputSoubor_ = vm["output-file"].as<string_t>();

        // kontrola koncovky, jestli je *.xml nebo *.json
        size_t output_delka = OutputSoubor_.length();
        string_t koncovka3 = "";
        string_t koncovka4 = "";
        if(output_delka > 4){ koncovka3 = OutputSoubor_.substr(output_delka-4); }
        if(output_delka > 5){ koncovka4 = OutputSoubor_.substr(output_delka-5); }

        koncovka3 = Text::NaMala(koncovka3);
        koncovka4 = Text::NaMala(koncovka4);

        if(koncovka3 == ".xml")
        {
            if(OutputFormat_ == true)
            {
                LogSystem::VratLog().Zapis("engine","Parametr --output-format bude ignorovan");
            }
            FormatVystup_ = 0;
        }
        else if(koncovka4 == ".json")
        {
            if(OutputFormat_ == true)
            {
                LogSystem::VratLog().Zapis("engine","Parametr --output-format bude ignorovan");
            }
            FormatVystup_ = 1;
        }
        else{ Error("Nepodporovany vystupni format souboru (" + OutputSoubor_ + ")!"); }
    }

    if(vm.count("output-format"))
    {
        string_t format = vm["output-format"].as<string_t>();
        format = Text::NaMala(format);

        if(OutputSoubor_ == "")
        {
            if(format == "xml")
            {
                FormatVystup_ = 0;
                OutputFormat_ = true;
            }
            else if(format == "json")
            {
                FormatVystup_ = 1;
                OutputFormat_ = true;
            }
            else
            {
                string_t error_msg = "Neznamy format " + format +
                        " ve volbe --output-format";
                Error(error_msg);
            }
        }
        else
        {
            LogSystem::VratLog().Zapis("engine","Parametr --output-format bude ignorovan");
        }
    }

    if(vm.count("config-file"))
    {
        string_t nazev = vm["config-file"].as<string_t>();

        if(KonfSystem::VratKonfiguraci().VratAltSoubor() == "")
        {
            KonfSystem::VratKonfiguraci().AltKonfSoubor_ = nazev;
        }
        else{Error("Spatny format parametru: Dvojity alternativni konfiguracni soubor!");}
    }

    if(vm.count("config"))
    {
        string_t konf = vm["config"].as<string_t>();

        if(KonfSystem::VratKonfiguraci().VratAltKonfiguraci() == "")
        {
            KonfSystem::VratKonfiguraci().AltKonfigurace_ = konf;
        }
        else{Error("Spatny format parametru: Dvojita alternativni konfigurace!");}
    }

    if(vm.count("include"))
    {
        string_t incl = vm["include"].as<string_t>();

        boost::split(IncludePluginy_, incl, boost::is_any_of(","), boost::token_compress_on);
    }

    if(vm.count("exclude"))
    {
        string_t excl = vm["exclude"].as<string_t>();

        boost::split(ExcludePluginy_, excl, boost::is_any_of(","), boost::token_compress_on);
    }

    if(vm.count("input-charset"))
    {
        InputCharset_ = vm["input-charset"].as<string_t>();
    }

    /********** KONEC BOOST PROGRAM_OPTIONS **********/

    return;
}

int Engine::UlozSoubor()
{
    LogSystem::VratLog().Zapis("engine","Ukladam soubor do zvoleneho formatu.");

    std::ostream* output;
    std::ofstream output_soubor;

    if(OutputSoubor_ == "")
    {
        output = &std::cout;
    }
    else
    {
        output_soubor.open(OutputSoubor_);
        output = &output_soubor;
    }

    boost::property_tree::ptree strom;

    size_t pocet = Chyby_.Pocet();
    for(size_t i = 0; i < pocet; ++i)
    {
        boost::property_tree::ptree& vrchol = strom.add("seznam_chyb.chyba","");

        Chyba zapis = Chyby_.at(i);

        vrchol.add("nazev_pluginu",zapis.VratNazevPluginu());
        vrchol.add("nazev_chyby",zapis.VratNazevChyby());
        vrchol.add("cislo_titulku",zapis.VratCisloTitulku());
        vrchol.add("pozice_v_titulku",zapis.VratPozici());

        const StringVector& opravy = zapis.VratOpravy();
        for(auto& i : opravy)
        {
            vrchol.add("seznam_oprav.oprava",i);
        }
    }

    if(FormatVystup_ == 0)
    {
/* 
 * problemy s pretty printingem na ruznych prekladacich, ci ruznych verzich boostu
#ifdef _WIN32
			boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
#else
			boost::property_tree::xml_writer_settings<char> settings('\t', 1);
#endif
			boost::property_tree::xml_parser::write_xml(*output, strom, settings);
			*/

        boost::property_tree::xml_parser::write_xml(*output, strom);
    }
    else
    {
        boost::property_tree::json_parser::write_json(*output, strom);
    }

    return 0;
}

void Engine::OrezKonecRadky(string_t& radka)
{
    if(radka.length() == 0){ return; }

    while(radka[radka.length() - 1] == '\r' || radka[radka.length() - 1] == '\n')
    {
        radka = radka.substr(0,radka.length() - 1);
        if(radka.length() == 0){ return; }
    }
    return;
}

void Engine::OrezUTFBom(string_t& radka)
{
    if(radka.length() < 3){ return; }

    if (radka[0] == '\xEF' && radka[1] == '\xBB' && radka[2] == '\xBF')
    {
        if(radka.length() == 3){ radka = ""; return; }

        radka = radka.substr(3,radka.length());
    }
    return;
}

void Engine::Reset()
{
    InputSoubor_.clear();
    OutputSoubor_.clear();
    FormatVystup_ = 0;
    OutputFormat_ = false;
    InputCharset_.clear();

    Titulky_.Reset();
    Chyby_.Reset();
}

void Engine::NewInit(string_t input_soubor, string_t output_soubor, int format_vystup, string_t input_charset)
{
    InputSoubor_ = input_soubor;
    OutputSoubor_ = output_soubor;
    FormatVystup_ = format_vystup;
    if(format_vystup != 0){ OutputFormat_= true; }
    InputCharset_ = input_charset;

    Titulky_.Reset();
    Chyby_.Reset();
}

void Engine::Help()
{
    std::cout << "Vitejte v napovede programu Heuristika titulku" << std::endl << std::endl;


    std::cout << "Popis:" << std::endl << std::endl;

    std::cout << "\theuristika_titulku [INPUT_FILE]" << std::endl;
    std::cout << "\t\tProgram heuristika_titulku slouzi k analyze souboru s multimedialnimi titulky\n\t\tPokud je zadan INPUT_FILE bude nacten a zanalyzovan soubor s timto jmenem." << std::endl << std::endl;


    std::cout << "Parametry:" << std::endl << std::endl;

    std::cout << "\t-h [ --help ]" << std::endl;
    std::cout << "\t\tZobrazi tuto napovedu" << std::endl << std::endl;

    std::cout << "\t-l [ --log ] LOGFILE" << std::endl;
    std::cout << "\t\tZapina logovani programu, ktere se uklada do souboru LOGFILE" << std::endl << std::endl;

    std::cout << "\t-i [ --input-file ] FILE" << std::endl;
    std::cout << "\t\tVstupni soubor se jmenem FILE ve formatu SubRip s koncovkou *.srt" << std::endl << std::endl;

    std::cout << "\t-a [ --input-charset ] CHARSET" << std::endl;
    std::cout << "\t\tSpecifikuje jake kodovani ma vstupni soubor, implicitne se predpoklada UTF8" << std::endl << std::endl;

    std::cout << "\t-o [ --output-file ] FILE" << std::endl;
    std::cout << "\t\tSpecifikuje vystupni soubor se jmenem FILE ve formatu xml nebo json" << std::endl << std::endl;

    std::cout << "\t-t [ --output-format ] FORMAT" << std::endl;
    std::cout << "\t\tNastavuje vystupni format (FORMAT == xml || FORMAT == json),\n\t\t pokud byl specifikovan vystupni soubor, bude tato volba ignorovana" << std::endl << std::endl;

    std::cout << "\t-f [ --config-file ] FILE" << std::endl;
    std::cout << "\t\tDiky teto volbe bude nacten alternativni konfiguracni soubor s nazvem FILE" << std::endl << std::endl;

    std::cout << "\t-c [ --config ] CONFIG" << std::endl;
    std::cout << "\t\tNacita alternativni serializovanou konfiguraci CONFIG ve formatu xml nebo json" << std::endl << std::endl;

    std::cout << "\t-p [ --include ] LIBS" << std::endl;
    std::cout << "\t\tPluginy, ktere maji byt nacteny oddelene carkou a bez mezer" << std::endl << std::endl;

    std::cout << "\t-e [ --exclude ] LIBS" << std::endl;
    std::cout << "\t\tPluginy, ktere maji byt vynechany, oddelene carkou a bez mezer" << std::endl << std::endl;

    exit(1);
    return;
}
