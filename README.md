# README #

Repozitář programu heuristika_titulku, který slouží k analýze titulků ve formátu SubRip.

### Úvod ###

* Program heuristika_titulku vzniká jako Ročníkový projekt a navazující bakalářská práce na MFF v Praze pod vedením pana [Děckého](http://www.decky.cz/).
* Původní nápad na napsání aplikace vzešel od [Khanovy školy](https://khanovaskola.cz/), která se zabývá překladem videí z webu [Khan Academy](https://www.khanacademy.org/).
* Tento softwarový projekt se tedy zabývá analýzou a hledáním chyb v titulcích.
* Vývoj probíhá na operačním systému založeném na linuxovém jadře a dochází k používání některých systémových funkcí, aplikace se tak proto stává nepřenositelnou.

### Získání aplikace a překlad ###

* Aplikaci je možné stáhnout pouze s nainstalovaným verzovacím systémem **git** a pouze z tohoto repozitáře.
* Překlad se pak provádí standardně pomocí programu **make**. Pro samotný překlad je pak nutný soubor knihoven **boost**.
* Pro bližší seznámení s aplikací je nutno přečíst minimálně uživatelskou dokumentaci, která je umístěna ve složce "./dokumentace/".