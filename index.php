<?php

set_time_limit(0);

/* 
 * http://stackoverflow.com/questions/2320608/php-stderr-after-exec
 * Answered and written by Mark; ID: 65385
 *
 * Modified by Martin Polanka
 */
function my_shell_exec($cmd, $input_given, $stdin, &$stdout=null, &$stderr=null) {
    $proc = proc_open($cmd,[
    	  0 => ['pipe','r'],
        1 => ['pipe','w'],
        2 => ['pipe','w']
    ],$pipes);
    
    if($input_given == 1)
    {
    	fwrite($pipes[0], $stdin);
    	fclose($pipes[0]);
    }
    
    $stdout = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $stderr = stream_get_contents($pipes[2]);
    fclose($pipes[2]);
    return proc_close($proc);
}


function html_header()
{
	echo '<!DOCTYPE html>', PHP_EOL; 
	echo '<html>', PHP_EOL;
  
	echo '<head>', PHP_EOL;
	echo '  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">', PHP_EOL;
	echo '  <meta http-equiv="Content-language" content="cs">', PHP_EOL;
	echo '  <meta name="author" content="Martin" >', PHP_EOL;
	echo '  <title>Heuristika titulků - Web interface</title>', PHP_EOL;
	echo '</head>', PHP_EOL;

	echo '<body>', PHP_EOL;
	
	echo '<script>
function loading()
{
  var load = document.getElementById(\'loading\');

  load.style.display = \'inline-flex\';
  load.innerHTML = \'<div>Loading...</div>\';
}
</script>', PHP_EOL;
echo '<style>
#loading {
	display: none;
	position: fixed;
	left: 0px;
	top: 0px;
   z-index: 100;
   width: 100%;
   height: 100%;
	vertical-align: middle;
	text-align: center;
   background-color: rgba(192, 192, 192, 0.5);
   background-repeat: no-repeat;
   background-position: center;
}
#loading div {
	width: 200px;
	height: 100px;
	margin: auto;
	line-height: 100px;
	font-weight: bold;
	background-color: #f5f5f5;
	border: 2px solid #808080;
}
</style>', PHP_EOL;

	echo '<div id="loading"></div>', PHP_EOL;

	echo '<h1>Heuristika titulků - Webové rozhranní</h1>', PHP_EOL;
}

function html_footer()
{
	echo '</body>', PHP_EOL;
	echo '</html>', PHP_EOL;
}



if(isset($_POST["exec_form"]))
{
	$stdin = "";
	$stderr = "";
	$stdout = "";
	$ret_val = 0;
  
  if(PHP_OS == "Windows" || PHP_OS == "WINNT" || PHP_OS == "WIN32")
  {
    $exec_prg = substr($_SERVER["SCRIPT_FILENAME"], 0, strrpos($_SERVER["SCRIPT_FILENAME"], "/")) . '/heuristika_titulku.exe'; 
  }
  else
  {
    $exec_prg = "./heuristika_titulku";
  }
  
  
  
  
  /* nacteni parametru z formulare */

	$input = "";
	$input_given = 0;
	if($_POST["input"] == "test")
	{
		$input = " -i test.srt";
	}
	elseif($_POST["input"] == "text")
	{
		$input_given = 1;
		$stdin = $_POST["input_text"];
	}
	elseif($_POST["input"] == "file")
	{
		// Check file size
		if ($_FILES["input_file"]["size"] > 500000) {
    		die("Prilis velky vstupni soubor!");
		}
		move_uploaded_file($_FILES["input_file"]["tmp_name"], $_FILES["input_file"]["tmp_name"] . '.srt');
		$input = ' -i "' . $_FILES["input_file"]["tmp_name"] . '.srt"';
	}
  
	$konf = "";
	if($_POST["konf"] == "text")
	{
		$konf = ' -c "' . $_POST["konf_text"] . '"';
	}
	elseif($_POST["konf"] == "file")
	{
		// Check file size
		if ($_FILES["konf_file"]["size"] > 500000) {
    		die("Prilis velky soubor s alternativni konfiguraci!");
		}
		$konf = ' -f "' . $_FILES["konf_file"]["tmp_name"] . '"';
	}
	
	$out_format = "";
	if($_POST["output_format"] == "json")
	{
		$out_format = ' -t json';
	}
	
	$help = "";
	if(isset($_POST["help"]) && $_POST["help"] == "1")
	{
		$help = ' -h';
	}
	
	$log = "";
	if(isset($_POST["log"]) && $_POST["log"] == "1")
	{
		$tempname = tempnam('/tmp', "log");
		unlink($tempname);
		$log = ' -l ' . './logs/' . basename($tempname) . '.log';
	}
	
	$include = "";
	if(isset($_POST["include"]))
	{
		$include = " -p \"";
		foreach($_POST["include"] as $incl_entry)
		{
			$include = $include . "," . $incl_entry;
		}
		$include = $include . "\"";
	}
	
	$exclude = "";
   if(isset($_POST["exclude"]))
	{
		$exclude = " -e \"";
		foreach($_POST["exclude"] as $excl_entry)
		{
			$exclude = $exclude . "," . $excl_entry;
		}
		$exclude = $exclude . "\"";
	}
	
	$charset = "";
	if($_POST["charset"] == "cp1250")
	{
		$charset = " -a cp1250";
	}
	elseif($_POST["charset"] == "other")
	{
		$charset = ' -a "' . $_POST["charset_other"] . '"';
	}
	
  
  
  /* spusteni programu a ziskani vystupu */
  
  if(!file_exists($exec_prg)){ die('Program heuristika_titulku na serveru neexistuje!'); }
  else
  {
    my_shell_exec('"'. $exec_prg . '"' . $help . $input . $konf . $out_format . $log . $include . $exclude . $charset, $input_given, $stdin, $stdout, $stderr);
    if($_POST["input"] == "file"){ unlink($_FILES["input_file"]["tmp_name"] . '.srt'); }
  }
  
  
	if(true)
	{
		html_header();		
		
		echo '<a href="' . $_SERVER["PHP_SELF"] . '">Vrat se zpatky na zadavaci formular.</a><br><br>', PHP_EOL;
  
  		if($help != "")
  		{
  			echo nl2br($stdout);
  		}
  		else
  		{
  			if($stderr != ""){ echo '<b>stderr output: </b>' . $stderr; }
  			
  			if($stdout != "" && $out_format == "")
			{ 
				if($log != "")
				{
					echo '<a href="./logs/' . basename($tempname) . '.log">Log File</a><br><br>', PHP_EOL;
				}			
			
				$dom = new DOMDocument;
				$dom->loadXML($stdout);
    
				$seznam_chyb = $dom->getElementsByTagName("seznam_chyb");
				$seznam_chyb = $seznam_chyb->item(0);
    
				echo '<table border="1">', PHP_EOL;
				echo '<tr><td>Nazev pluginu</td><td>Nazev chyby</td><td>Cislo titulku</td><td>Pozice v titulku</td><td>Seznam Oprav</td></tr>', PHP_EOL;
				foreach($seznam_chyb->childNodes as $chyba)
				{
					if($chyba->nodeType != XML_ELEMENT_NODE){ continue; }
				
					$children = $chyba->childNodes;
				
					echo '<tr>', PHP_EOL;
					echo '<td>' . $children->item(0)->nodeValue . '</td>', PHP_EOL;
					echo '<td>' . $children->item(1)->nodeValue . '</td>', PHP_EOL;
					echo '<td>' . $children->item(2)->nodeValue . '</td>', PHP_EOL;
					echo '<td>' . $children->item(3)->nodeValue . '</td>', PHP_EOL;
					echo '<td>', PHP_EOL;
				
					if($children->length > 4)
					{
						echo '<table width="100%" border="1">', PHP_EOL;
						foreach($children->item(4)->childNodes as $oprava)
						{
							if($oprava->nodeType != XML_ELEMENT_NODE){ continue; }
						
							echo '<tr><td>', PHP_EOL;
							echo nl2br($oprava->nodeValue);
							echo '</td></tr>', PHP_EOL;		
						}
					
						echo '</table>', PHP_EOL;	
					}
					echo '</td>', PHP_EOL;
					echo '</tr>', PHP_EOL;
				}
				echo '</table>', PHP_EOL;
			}
		
			if($stdout != "" && $out_format != "")
			{
				if($log != "")
				{
					echo '<a href="./logs/' . basename($tempname) . '.log">Log File</a><br><br>', PHP_EOL;
				}	
				
				echo nl2br($stdout);
			}
  		}
		
		html_footer();
	}  
}
else
{
	html_header();
	echo '<style>.tr_border_top {} .tr_border_top td {border-top:1px solid black;}
		.tr_border_bot {} .tr_border_bot td {border-bottom:1px solid black;}
		.td_border_bot {border-bottom:1px solid black;}
		</style>', PHP_EOL;
	echo '<form method="post" action="' . $_SERVER["PHP_SELF"] . '"  enctype="multipart/form-data">', PHP_EOL;
	echo '<table>', PHP_EOL;
	
	echo '<tr class="tr_border_top">', PHP_EOL;
	echo '<td><b>Zobraz nápovědu: </b></td><td colspan="2"><input type="checkbox" name="help" value="1"></td>', PHP_EOL;
	echo '</tr>', PHP_EOL;
	
	echo '<tr class="tr_border_top">', PHP_EOL;
	echo '<td rowspan="2" class="td_border_bot"><b>Formát výstupu: </b></td>', PHP_EOL;
	echo '<td><input id="output_format1" type="radio" name="output_format" value="xml" checked></td><td><label for="output_format1">XML (bude zpracováno a zobrazena tabulka)</label></td>', PHP_EOL;
	echo '</tr>', PHP_EOL;
	echo '<tr class="tr_border_bot">', PHP_EOL;
	echo '<td><input id="output_format2" type="radio" name="output_format" value="json"></td><td><label for="output_format2">JSON</label></td>', PHP_EOL;
	echo '</tr>', PHP_EOL;	
	
	echo '<tr>', PHP_EOL;
	echo '<td><b>Log: </b></td><td colspan="2"><input type="checkbox" name="log" value="1"></td>', PHP_EOL;
	echo '</tr>', PHP_EOL;
	
	echo '<tr class="tr_border_top"><td rowspan="3" class="td_border_bot"><b>Input: </b></td>', PHP_EOL;
	echo '<td><input id="input1" type="radio" name="input" value="test" checked></td>', PHP_EOL;
	echo '<td><label for="input1"><a target="_blank" href="./test.srt">test.srt</a></label></td></tr>', PHP_EOL;
	echo '<tr><td><input id="input2" type="radio" name="input" value="file"></td>', PHP_EOL;
	echo '<td><label for="input2"><input type="file" name="input_file"></label></td></tr>', PHP_EOL;	
	echo '<tr class="tr_border_bot"><td><input id="input3" type="radio" name="input" value="text"></td>', PHP_EOL;
	echo '<td><textarea name="input_text" cols="150" rows="10">1
00:00:01,000 --> 00:00:02,000
First subtitle text

2
00:00:03,000 --> 00:00:04,000
Second subtitle text</textarea></td></tr>', PHP_EOL;
	
	echo '<tr><td rowspan="3" class="td_border_bot"><b>Vstupní kódování: </b></td>', PHP_EOL;
	echo '<td><input id="charset1" type="radio" name="charset" value="utf8" checked></td>', PHP_EOL;
	echo '<td><label for="charset1">UTF8</label></td>', PHP_EOL;
	echo '</tr>', PHP_EOL;
	echo '<tr><td><input id="charset2" type="radio" name="charset" value="cp1250"></td>', PHP_EOL;
	echo '<td><label for="charset2">CP1250</label></td>', PHP_EOL;
	echo '</tr>', PHP_EOL;
	echo '<tr class="tr_border_bot"><td><input id="charset3" type="radio" name="charset" value="other"></td>', PHP_EOL;
	echo '<td><input name="charset_other"></td>', PHP_EOL;
	echo '</tr>', PHP_EOL;

	echo '<tr><td rowspan="3" class="td_border_bot"><b>Konfigurace: </b></td>', PHP_EOL;
	echo '<td><input id="konf1" type="radio" name="konf" value="default" checked></td><td><label for="konf1">default</label></td></tr>', PHP_EOL;
	echo '<tr><td><input id="konf2" type="radio" name="konf" value="file"></td>', PHP_EOL;
	echo '<td><label for="konf2"><input type="file" name="konf_file"></label></td></tr>', PHP_EOL;
	echo '<tr class="tr_border_bot"><td><input id="konf3" type="radio" name="konf" value="text"></td>', PHP_EOL;
	echo '<td><textarea name="konf_text" cols="150" rows="10"><heuristika_titulku>
...
</heuristika_titulku></textarea></td></tr>', PHP_EOL;

	$plugins_folder = scandir("./pluginy");
	for($i = 0; $i < count($plugins_folder); $i++)
	{
		if($plugins_folder[$i] == '.' || $plugins_folder[$i] == '..'){ array_splice($plugins_folder, $i, 1); $i--; continue; }
		
		$lib_pos = strpos($plugins_folder[$i], "lib");
		if($lib_pos === FALSE || $lib_pos != 0){ array_splice($plugins_folder, $i, 1); $i--; continue; }
		else{ $plugins_folder[$i] = substr($plugins_folder[$i], 3); }
		
		$dll_ext = "";
		if(PHP_OS == "Windows" || PHP_OS == "WINNT" || PHP_OS == "WIN32"){ $dll_ext = ".dll"; }
  		else{ $dll_ext = ".so"; }
  		
		$ext_pos = strpos($plugins_folder[$i], $dll_ext);
		if($ext_pos === FALSE || $ext_pos != strlen($plugins_folder[$i]) - strlen($ext_pos) - 1){ array_splice($plugins_folder, $i, 1); $i--; continue; }
		else{ $plugins_folder[$i] = substr($plugins_folder[$i], 0, strlen($plugins_folder[$i]) - strlen($ext_pos) - 1); }
	}
	
	echo '<tr>', PHP_EOL;
	echo '<td rowspan="' . count($plugins_folder) . '" class="td_border_bot"><b>Použité pluginy: </b></td>', PHP_EOL;
	for($i = 0; $i < count($plugins_folder); $i++)
	{
		$temp_soubor = $plugins_folder[$i];		
		
		if($i == 0){ echo '', PHP_EOL; }
		elseif($i < (count($plugins_folder) - 1)){ echo '<tr>', PHP_EOL; }
		else{ echo '<tr class="tr_border_bot">', PHP_EOL; }
		
		echo '<td><input id="include' . $i . '" type="checkbox" name="include[]" value="' . $temp_soubor . '" checked></td>', PHP_EOL;
		echo '<td><label for="include' . $i . '">' . $temp_soubor . '</label></td>', PHP_EOL;
		
		if($i != (count($plugins_folder) - 1)){ echo '</tr>', PHP_EOL; }
	}
	echo '</tr>', PHP_EOL;
	
	echo '<tr>', PHP_EOL;
	echo '<td rowspan="' . count($plugins_folder) . '" class="td_border_bot"><b>Vynechané pluginy: </b></td>', PHP_EOL;
	for($i = 0; $i < count($plugins_folder); $i++)
	{
		$temp_soubor = $plugins_folder[$i];		
		
		if($i == 0){ echo '', PHP_EOL; }
		elseif($i < (count($plugins_folder) - 1)){ echo '<tr>', PHP_EOL; }
		else{ echo '<tr class="tr_border_bot">', PHP_EOL; }
		
		echo '<td><input id="exclude' . $i . '" type="checkbox" name="exclude[]" value="' . $temp_soubor . '"></td>', PHP_EOL;
		echo '<td><label for="exclude' . $i . '">' . $temp_soubor . '</label></td>', PHP_EOL;
		
		if($i != (count($plugins_folder) - 1)){ echo '</tr>', PHP_EOL; }
	}
	echo '</tr>', PHP_EOL;
	
	echo '<tr><td colspan="3"><input type="submit" name="exec_form" value="Execute" onClick="loading()"></td></tr>', PHP_EOL;
	echo '</table>', PHP_EOL;
	echo '</form>', PHP_EOL;
	html_footer();
}


?>
