#ifndef ANGLICKA_NOTACE_CISEL_H
#define ANGLICKA_NOTACE_CISEL_H

#ifdef _WIN32
#define EXPORT_ANGLICKA_NOTACE_CISEL __declspec(dllexport)
#else
#define EXPORT_ANGLICKA_NOTACE_CISEL
#endif

#include "../../shared/plugin.h"

class Anglicka_notace_cisel : public Plugin
{
public:
    Anglicka_notace_cisel();
    virtual ~Anglicka_notace_cisel(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    string_t EnNaCz(const string_t &cislo);
};

#endif // ANGLICKA_NOTACE_CISEL_H
