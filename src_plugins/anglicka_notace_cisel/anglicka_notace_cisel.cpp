#include "anglicka_notace_cisel.h"

Anglicka_notace_cisel::Anglicka_notace_cisel() : Plugin("Anglicka notace cisla","anglicka_notace_cisel")
{
}

int Anglicka_notace_cisel::Run(Titulek &titulek, Chyby &chyby)
{
    int vysledek = 0;

    size_t pocet = titulek.PocetSlov();

    for(size_t i = 0; i < pocet; ++i)
    {
        const string_t& slovo = titulek.VratSlovo(i).VratText();
        size_t pozice_slova = titulek.VratSlovo(i).VratPozici();

        size_t pocet_tecek = 0;
        size_t pocet_cisel = 0;
        size_t delka_slova = slovo.length();
        if(delka_slova == 0){ continue; }
        bool anglicka_notace = false;

        size_t zacatek = 0;
        if(slovo.at(0) == '-'){ ++zacatek; } // zaporna cisla

        for(size_t j = zacatek; j < delka_slova; ++j)
        {
            char pismeno = slovo.at(j);

            if((pismeno >= '0' && pismeno <= '9') || pismeno == '.')
            {
                if(pismeno == '.')
                {
                    ++pocet_tecek;
                    anglicka_notace = true;
                }
                else
                {
                    ++pocet_cisel;
                }
            }
            else
            { // byl nalezen znak krome cisel a tecek
                anglicka_notace = false;
                break;
            }
        }

        if(pocet_cisel == 0){ anglicka_notace = false; } // aby se nedekovali "..." apod.
        if(pocet_tecek == 1 && slovo.back() == '.'){ anglicka_notace = false; } // pokud je cislovka na konci slova, nebo je to radovka (pr: "1. unora")

        if(anglicka_notace == true)
        {
            if(pocet_tecek == 1)
            {
                Chyba pridej(NazevPluginu(),NazevChyby(),titulek.VratUID(),pozice_slova);
                pridej.PridejOpravu(EnNaCz(slovo));

                chyby.PushBack(pridej);

                Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                vysledek = 1;
            }
            else
            { // pokud bylo v cisle vice tecek
                Chyba pridej(NazevPluginu(),"Nejaky divny ciselny format",titulek.VratUID(),pozice_slova);
                pridej.PridejOpravu(EnNaCz(slovo));

                chyby.PushBack(pridej);

                Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                vysledek = 1;
            }
        }
    }

    return vysledek;
}

void Anglicka_notace_cisel::Reset()
{
    return;
}

string_t Anglicka_notace_cisel::EnNaCz(const string_t& cislo)
{
    string_t vysledek;

    size_t zacatek = 0;
    if(cislo.at(0) == '-')
    {
        ++zacatek;
        vysledek.push_back('-');
    }

    if(cislo.at(zacatek) == '.')
    {
        vysledek.push_back('0');
    }

    size_t delka = cislo.length();

    for(size_t i = zacatek; i < delka; ++i)
    {
        if(cislo.at(i) == '.')
        {
            vysledek.push_back(',');
        }
        else
        {
            vysledek.push_back(cislo.at(i));
        }
    }

    return vysledek;
}

extern "C" Plugin EXPORT_ANGLICKA_NOTACE_CISEL * vytvor()
{
    return new Anglicka_notace_cisel();
}

extern "C" void EXPORT_ANGLICKA_NOTACE_CISEL znic(Plugin* p)
{
    delete p;
}
