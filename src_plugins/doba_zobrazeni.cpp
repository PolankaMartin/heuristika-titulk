#include "doba_zobrazeni.h"

Zaznam::Zaznam(string_t symbol, size_t doba) : Symbol_(symbol), DobaZobrazeni_(doba)
{
}

string_t &Zaznam::VratSymbol()
{
    return Symbol_;
}

size_t Zaznam::VratDobu()
{
    return DobaZobrazeni_;
}

bool Zaznam::operator<(const Zaznam& right) const
{
    if(Symbol_.length() < right.Symbol_.length()){return true;}
    else{return false;}
}

bool Zaznam::operator>(const Zaznam& right) const
{
    return right.operator<(*this);
}

size_t Doba_zobrazeni::Run(size_t default_zobrazeni, ZaznamVector &zaznamy, Titulek &titulek)
{
    std::vector<std::vector<bool>> zpracovano;
    size_t soucet_doby = 0;
    size_t zapocitanych_znaku = 0;
    size_t ii = 0;

    size_t pocet_radku = titulek.PocetRadku();
    for(size_t i = 0; i < pocet_radku; ++i)
    {
        string_t radka = titulek.VratRadek(i).VratZmensenyText();

        zpracovano.push_back(std::vector<bool>(radka.length(),false));

        for(auto& j : zaznamy)
        {
            string_t symbol = j.VratSymbol();
            size_t doba = j.VratDobu();
            size_t delka_symbolu = symbol.length();

            size_t iter = 0;
            size_t pozice_symbolu = radka.find(symbol,iter);

            while(pozice_symbolu != string_t::npos)
            {
                bool zapocitano_temp = false;
                for(size_t a = 0; a < delka_symbolu; ++a)
                {
                    if(zpracovano.at(ii).at(pozice_symbolu + a) == true)
                    {
                        zapocitano_temp = true;
                    }
                }

                if(zapocitano_temp == false)
                {
                    soucet_doby += doba;
                    zapocitanych_znaku += delka_symbolu;

                    for(size_t a = 0; a < delka_symbolu; ++a)
                    {
                        zpracovano.at(ii).at(pozice_symbolu + a) = true;
                    }

                    iter = pozice_symbolu + delka_symbolu;
                    pozice_symbolu = radka.find(symbol,iter);
                }
                else
                {
                    iter = pozice_symbolu + 1;
                    pozice_symbolu = radka.find(symbol,iter);
                }
            }
        }

        ++ii;
    }

    size_t ostatni_symboly = titulek.PocetZnaku() - zapocitanych_znaku;
    soucet_doby += (default_zobrazeni * ostatni_symboly);

    return soucet_doby;
}

int Doba_zobrazeni::NactiDobyZobrazeni(Plugin& plugin, ZaznamVector &zaznamy, size_t &default_zobr)
{
    bool vysledek = true;

    boost::property_tree::ptree strom = plugin.NactiStromNastaveni("kratke_zobrazeni");
    if(strom.empty() == true){ vysledek = false; }

    try
    {
        default_zobr = strom.get<size_t>("default");

        strom = strom.get_child("seznam_zaznamu");

        for(auto& i : strom)
        {
            string_t symbol;
            size_t doba;

            symbol = i.second.get<string_t>("symbol");
            symbol = Text::NaMala(symbol);

            doba = i.second.get<size_t>("doba_zobrazeni");

            zaznamy.push_back(Zaznam(symbol,doba));
        }
    }
    catch(...)
    {
        vysledek = false;
    }

    if(vysledek == false)
    {
        plugin.ErrorNastaveni();
        return 1;
    }

    // vysortit, aby se nejdrive zpracovaly delsi symboly...
    std::sort(zaznamy.begin(),zaznamy.end(),std::greater<Zaznam>());

    return 0;
}
