#ifndef MALE_PISMENO_H
#define MALE_PISMENO_H

#ifdef _WIN32
#define EXPORT_MALE_PISMENO __declspec(dllexport)
#else
#define EXPORT_MALE_PISMENO
#endif

#include "../../shared/plugin.h"

class Male_pismeno : public Plugin
{
public:
    Male_pismeno();
    virtual ~Male_pismeno(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    int NactiNastaveni();

    Slovnik<string_t> Slovnik_;
    bool ZacatekVety_;
};

#endif // MALE_PISMENO_H
