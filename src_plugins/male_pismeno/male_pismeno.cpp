#include "male_pismeno.h"

Male_pismeno::Male_pismeno() : Plugin("Male pismeno na zacatku vety","male_pismeno"), ZacatekVety_(true)
{
    NactiNastaveni();
}

void Male_pismeno::Reset()
{
    ZacatekVety_ = true;
}

int Male_pismeno::Run(Titulek &titulek, Chyby &chyby)
{
    int vysledek = 0;
    size_t pocet_slov = titulek.PocetSlov();

    for(size_t i = 0; i < pocet_slov; ++i)
    {
        const string_t& slovo = titulek.VratSlovo(i).VratText();
        size_t pozice = titulek.VratSlovo(i).VratPozici();

        if(slovo.length() == 0){ continue; }

        if(ZacatekVety_ == true)
        {
            string_t temp_slovo = Text::OdstranZacatekBloku(slovo);
            if(temp_slovo.length() == 0){ continue; }

            if(Text::JeMale(temp_slovo, 0) == true && Text::JeVelke(temp_slovo, 0) == false)
            {
                Chyba pridej(NazevPluginu(),NazevChyby(),titulek.VratUID(),pozice);
                string_t temp = Text::NaVelka(temp_slovo,0,1);
                pridej.PridejOpravu(temp);
                chyby.PushBack(pridej);

                Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                vysledek = 1;
            }

            ZacatekVety_ = false;
        }

        if(slovo.length() >= 2)
        {
            string_t dve = slovo.substr(slovo.length()-2);
            if(dve == ".\"" || dve == "!\"" || dve == "?\"" || dve == ".\'" || dve == "!\'" || dve == "?\'")
            {
                ZacatekVety_ = true;
            }
            else if(dve == ".:")
            {
                ZacatekVety_ = false;
            }
        }
        if(slovo.length() > 0 && (slovo.back() == '.' || slovo.back() == '!' || slovo.back() == '?'))
        {
            bool radovka = false;
            try
            {
                if(slovo.back() == '.')
                {
                    stoi(slovo.substr(0,slovo.length()-1));
                    radovka = true;
                }
            }
            catch(...){}

            string_t temp = titulek.VratSlovo(i).VratZmensenyText();

            if(radovka == true || Slovnik_.Hledej(temp)){ ZacatekVety_ = false; } // bud byla nalezena radovka, nebo vyjimka...
            else{ ZacatekVety_ = true; }
        }
    }

    return vysledek;
}

int Male_pismeno::NactiNastaveni()
{
    bool vysledek = true;

    boost::property_tree::ptree strom = NactiStromNastaveni();
    if(strom.empty() == true){ vysledek = false; }

    for(auto& i : strom)
    {
        string_t slovo;

        try
        {
            slovo = i.second.get_value<string_t>();
            slovo = Text::NaMala(slovo);
        }
        catch(...)
        {
            vysledek = false;
            break;
        }

        Slovnik_.PushBack(slovo);
    }

    if(vysledek == false)
    {
        ErrorNastaveni();
        return 1;
    }

    return 0;
}

extern "C" Plugin EXPORT_MALE_PISMENO * vytvor()
{
    return new Male_pismeno();
}

extern "C" void EXPORT_MALE_PISMENO znic(Plugin* p)
{
    delete p;
}
