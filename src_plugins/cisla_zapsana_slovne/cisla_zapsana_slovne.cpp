#include "cisla_zapsana_slovne.h"

Cisla_zapsana_slovne::Cisla_zapsana_slovne() : Plugin("Cislo zapsane slovne, ne cislovkou","cisla_zapsana_slovne")
{
    NactiNastaveni();
}

void Cisla_zapsana_slovne::ZapisChybu(Chyby &chyby, Titulek &titulek, size_t zacatek_cisla, size_t cislo, bool negace, bool je_radovka)
{
    Chyba pridej(NazevPluginu(),NazevChyby(),titulek.VratUID(),zacatek_cisla);

    string_t temp_oprava = std::to_string(cislo);
    if(negace == true){ temp_oprava = NegaceOprava_ + temp_oprava; }
    if(je_radovka == true){ temp_oprava += "."; }
    pridej.PridejOpravu(temp_oprava);

    chyby.PushBack(pridej);

    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));

    return;
}

void Cisla_zapsana_slovne::Reset()
{
    return;
}

int Cisla_zapsana_slovne::Run(Titulek &titulek, Chyby &chyby)
{
    /*
     * Seznam bugu:
     * - konfiguracni soubor neobsahuje radove cislovky a ma cisla jen do tisice.
     * - plugin nedetekuje spatne sklonovani cisla, tzn. kazde jednotlive slovo ve slovne napsane cislovce muze mit jiny pad, presto je opraveno na cislo. (nemusi byt bug, zalezi na uhlu pohledu)
     * - pokud je v sledu cisel zapsanych slovne jedno cislo radove, pak je cele cislo oznaceno za radovku a zpusobem opravy je cislo s teckou na konci
     * - desitkove cislovky s obracenym slovosledem (dvaadvacet) detekovany a opravovany nejsou
     */

    int vysledek = 0;

    size_t pocet_slov = titulek.PocetSlov();

    size_t predchozi_cislo = 0; // predchozi neradove cislo (tj. promenna je_rad == false)
    size_t predchozi_volny_rad = size_t() - 1; // volny rad u predchoziho neradoveho cisla
    size_t volny_rad = size_t() - 1; // volny rad u aktualniho cisla v promenne cislo
    bool predchozi_je_rad = true; // pokud bylo predchozi cislo radove (je_rad == true), pak je nastaveno na true
    bool negace = false; // na zacatku se objevilo minus
    bool nalezeno_cislo = false; // pokud je true, pak se prave zpracovava cislo
    bool je_radovka = false; // radova cislovka (tj. prvni, druhy, treti, apod)
    bool predchozi_je_radovka = false;
    size_t cislo = 0;
    size_t zacatek_cisla = 0; // pozice na ktere cislo zacalo

    for(size_t i = 0; i < pocet_slov; ++i)
    {
        string_t slovo = titulek.VratSlovo(i).VratZmensenyText();

        bool konec = Text::JeKonecBloku(slovo);
        if(konec == true){ slovo = Text::OdstranKonecBloku(slovo); } // v pripade ze se narazilo na konec vety, tak se tecky, carky apod. uriznou
        bool zacatek = Text::JeZacatekBloku(slovo);
        if(zacatek == true)
        {
            slovo = Text::OdstranZacatekBloku(slovo);
            if(nalezeno_cislo == false){ zacatek = false; }
        }

        size_t pozice = titulek.VratSlovo(i).VratPoziciZmenseny();

        if(std::binary_search(NegaceZapis_.begin(),NegaceZapis_.end(),slovo)) // naslo se minus
        {
            if(nalezeno_cislo == false)
            {
                negace = true;
                nalezeno_cislo = true;
                zacatek_cisla = pozice;
                continue;
            }
        }

        std::shared_ptr<Cislo_data> oprava = SlovnikZapisu_.Hledej(slovo);
        if(zacatek == true) // dojde se sem pouze pokud je treba ukoncit predesle cislo (tzn. napriklad chybel oddelovac na konci minuleho slova a zacina novy blok oddeleny uvozovkami nebo pomlckou)
        {
            ZapisChybu(chyby,titulek,zacatek_cisla,cislo+predchozi_cislo,negace,je_radovka);

            vysledek = 1;

            /* VSE VYNULUJME */
            nalezeno_cislo = false;
            je_radovka = false;
            predchozi_je_rad = true;
            predchozi_cislo = 0;
            negace = false;
            cislo = 0;
            zacatek_cisla = 0;
            volny_rad = size_t() - 1;
            predchozi_volny_rad = volny_rad;
        }

        if(oprava != nullptr) // naslo se cislo zapsane slovne
        {
            if(nalezeno_cislo == false) // zacina se nove cislo
            {
                nalezeno_cislo = true;
                zacatek_cisla = pozice;
            }

            bool je_rad = oprava->JeRad();
            if(oprava->JeRadovka() == true){ je_radovka = true; }
            size_t temp_cislo = oprava->VratCislo();

            if(je_rad == true) // definice radu (stovky, tisice, atd. => desitky se berou jako cisla, ne rad)
            {
                if(predchozi_je_rad == true)
                {
                    if(cislo > temp_cislo || cislo == 0) // pokud mame napriklad: tri sta tisic, pak aby se tri sta priradilo k tisicum (k tomu slouzi else vetev)
                    {
                        if(volny_rad < Cisla::RadCisla(temp_cislo))
                        {
                            ZapisChybu(chyby,titulek,zacatek_cisla,cislo,negace,predchozi_je_radovka);
                            vysledek = 1;

                            /* NASTAVIT NA NOVE CISLO */
                            nalezeno_cislo = true;
                            predchozi_je_rad = true;
                            negace = false;
                            cislo = temp_cislo;
                            predchozi_cislo = 0;
                            zacatek_cisla = pozice;
                            volny_rad = Cisla::VolnyRadCisla(cislo);
                            predchozi_volny_rad = volny_rad;
                        }
                        else
                        {
                            cislo += temp_cislo;
                        }
                    }
                    else
                    {
                        cislo *= temp_cislo;
                    }
                    volny_rad = Cisla::VolnyRadCisla(cislo);
                    predchozi_volny_rad = volny_rad;
                }
                else
                {
                    if(cislo > temp_cislo || cislo == 0)
                    {
                        if(volny_rad < Cisla::RadCisla(predchozi_cislo * temp_cislo))
                        {
                            ZapisChybu(chyby,titulek,zacatek_cisla,cislo,negace,je_radovka);
                            vysledek = 1;

                            /* NASTAVIT NA NOVE CISLO */
                            nalezeno_cislo = true;
                            predchozi_je_rad = true;
                            negace = false;
                            cislo = predchozi_cislo * temp_cislo;
                            predchozi_cislo = 0;
                            zacatek_cisla = pozice;
                            volny_rad = Cisla::VolnyRadCisla(cislo);
                            predchozi_volny_rad = volny_rad;
                        }
                        else
                        {
                            cislo += (predchozi_cislo * temp_cislo);
                        }
                    }
                    else
                    {
                        cislo += predchozi_cislo;
                        cislo *= temp_cislo;
                    }
                    volny_rad = Cisla::VolnyRadCisla(cislo);
                    predchozi_volny_rad = volny_rad;
                }

                predchozi_cislo = 0;
                predchozi_je_rad = true;
            }
            else // cislovky ktere nejsou rad (tedy nejsou stovky, tisice apod.)
            {
                if(predchozi_je_rad == true)
                {
                    if(predchozi_volny_rad < Cisla::RadCisla(temp_cislo))
                    {
                        ZapisChybu(chyby,titulek,zacatek_cisla,cislo + predchozi_cislo,negace,je_radovka);
                        vysledek = 1;

                        /* NASTAVIT NA NOVE CISLO */
                        nalezeno_cislo = true;
                        predchozi_je_rad = false;
                        negace = false;
                        cislo = 0;
                        predchozi_cislo = temp_cislo;
                        zacatek_cisla = pozice;
                        volny_rad = size_t() - 1;
                        predchozi_volny_rad = Cisla::VolnyRadCisla(temp_cislo);
                    }
                    else
                    {
                        predchozi_cislo = temp_cislo;
                        predchozi_je_rad = false;
                        predchozi_volny_rad = Cisla::VolnyRadCisla(temp_cislo);
                    }
                }
                else
                {
                    if(predchozi_volny_rad < Cisla::RadCisla(temp_cislo))
                    {
                        ZapisChybu(chyby,titulek,zacatek_cisla,cislo + predchozi_cislo,negace,je_radovka);
                        vysledek = 1;

                        /* NASTAVIT NA NOVE CISLO */
                        nalezeno_cislo = true;
                        predchozi_je_rad = false;
                        negace = false;
                        cislo = 0;
                        predchozi_cislo = temp_cislo;
                        zacatek_cisla = pozice;
                        volny_rad = size_t() - 1;
                        predchozi_volny_rad = Cisla::VolnyRadCisla(temp_cislo);
                    }
                    else
                    {
                        predchozi_cislo += temp_cislo;
                        predchozi_volny_rad = Cisla::VolnyRadCisla(predchozi_cislo);
                    }
                }
            }
        }

        if(oprava == nullptr || konec == true) // nebylo nalezeno cislo, nebo se doslo na konec vety
        {
            if(nalezeno_cislo == true) // ukoncuje se cislo a pridava oprava
            {
                ZapisChybu(chyby,titulek,zacatek_cisla,cislo+predchozi_cislo,negace,je_radovka);
                vysledek = 1;

                /* VSE VYNULUJME */
                nalezeno_cislo = false;
                je_radovka = false;
                predchozi_je_rad = true;
                predchozi_cislo = 0;
                negace = false;
                cislo = 0;
                zacatek_cisla = 0;
                volny_rad = size_t() - 1;
                predchozi_volny_rad = volny_rad;
            }
        }

        predchozi_je_radovka = je_radovka;
    }

    if(nalezeno_cislo == true) // kdyby nahodou bylo cislo na konci slova
    {
        ZapisChybu(chyby,titulek,zacatek_cisla,cislo+predchozi_cislo,negace,je_radovka);
        vysledek = 1;
    }

    return vysledek;
}

int Cisla_zapsana_slovne::NactiNastaveni()
{
    bool vysledek = true;

    boost::property_tree::ptree strom = NactiStromNastaveni();
    if(strom.empty() == true){ vysledek = false; }

    try
    {
        /*    CISLOVKY    */

        boost::property_tree::ptree seznam_cislovek = strom.get_child("seznam_cislovek");

        for(auto& i : seznam_cislovek)
        {
            auto pridej = std::make_shared<Cislo_data>(i.second.get<size_t>("cislo"),i.second.get<bool>("je_radovka"),i.second.get<bool>("je_rad"));
            //std::cout << i.second.get<size_t>("cislo") << " " << i.second.get<bool>("je_radovka") << " " << i.second.get<bool>("je_rad") << std::endl;
            OpravaData_.push_back(pridej);

            boost::property_tree::ptree moznosti_zapisu = i.second.get_child("seznam_moznosti_zapisu");

            for(auto& j : moznosti_zapisu)
            {
                SlovnikZapisu_.PushBack(Text::NaMala(j.second.get_value<string_t>()),pridej);
            }
        }

        /*    NEGACE    */

        boost::property_tree::ptree negace = strom.get_child("negace");
        boost::property_tree::ptree moznosti_zapisu = negace.get_child("seznam_moznosti_zapisu");

        for(auto& i : moznosti_zapisu)
        {
            NegaceZapis_.push_back(Text::NaMala(i.second.get_value<string_t>()));
        }

        NegaceOprava_ = negace.get<string_t>("cislo");

        std::sort(NegaceZapis_.begin(),NegaceZapis_.end());
    }
    catch(...)
    {
        vysledek = false;
    }

    if(vysledek == false)
    {
        ErrorNastaveni();
        return 1;
    }

    return 0;
}

extern "C" Plugin EXPORT_CISLA_ZAPSANA_SLOVNE * vytvor()
{
    return new Cisla_zapsana_slovne();
}

extern "C" void EXPORT_CISLA_ZAPSANA_SLOVNE znic(Plugin* p)
{
    delete p;
}
