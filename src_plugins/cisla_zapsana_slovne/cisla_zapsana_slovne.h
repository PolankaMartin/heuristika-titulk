#ifndef CISLA_ZAPSANA_SLOVNE_H
#define CISLA_ZAPSANA_SLOVNE_H

#ifdef _WIN32
#define EXPORT_CISLA_ZAPSANA_SLOVNE __declspec(dllexport)
#else
#define EXPORT_CISLA_ZAPSANA_SLOVNE
#endif

#include "cislovky.h"

class Cisla_zapsana_slovne : public Plugin
{
public:
    Cisla_zapsana_slovne();
    virtual ~Cisla_zapsana_slovne(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    int NactiNastaveni();
    void ZapisChybu(Chyby& chyby,Titulek& titulek,size_t zacatek_cisla,size_t cislo,bool negace,bool je_radovka);

    std::vector<std::shared_ptr<Cislo_data>> OpravaData_;
    Slovnik_cislovky SlovnikZapisu_;

    StringVector NegaceZapis_;
    string_t NegaceOprava_;
};

#endif // CISLA_ZAPSANA_SLOVNE_H
