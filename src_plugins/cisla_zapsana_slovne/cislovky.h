#ifndef CISLOVKY_H
#define CISLOVKY_H

#include "../../shared/plugin.h"

class Cisla
{
public:
    Cisla() = delete;
    Cisla(const Cisla& source) = delete;
    Cisla& operator=(const Cisla& source) = delete;

    static size_t RadCisla(size_t cislo);
    static size_t VolnyRadCisla(size_t cislo);
};

class Cislo_data
{
public:
    Cislo_data() = delete;
    Cislo_data(size_t cislo, bool je_radovka, bool je_rad);

    size_t VratCislo(){ return Cislo_; }
    bool JeRadovka(){ return JeRadovka_; }
    bool JeRad(){ return JeRad_; }
private:
    size_t Cislo_;
    bool JeRadovka_;
    bool JeRad_;
};

class Slovnik_cislovky
{
public:
    Slovnik_cislovky();
    Slovnik_cislovky(const Slovnik_cislovky& source) = delete;
    Slovnik_cislovky& operator=(const Slovnik_cislovky& source) = delete;

    void PushBack(string_t text, std::shared_ptr<Cislo_data> id);
    std::shared_ptr<Cislo_data> Hledej(string_t text); // pokud dane slovo ve slovniku neni, pak se vraci nullptr
private:
    std::map<string_t,std::shared_ptr<Cislo_data>>Data_;
};

#endif // CISLOVKY_H
