#include "cislovky.h"

size_t Cisla::RadCisla(size_t cislo)
{
    size_t vysledek = 0;

    do
    {
        vysledek++;
    }
    while((cislo /= 10) > 0);

    return vysledek;
}

size_t Cisla::VolnyRadCisla(size_t cislo)
{
    size_t vysledek = 0;

    while((cislo % 10) == 0)
    {
        vysledek++;
        cislo /= 10;
    }

    return vysledek;
}

Cislo_data::Cislo_data(size_t cislo, bool je_radovka, bool je_rad) : Cislo_(cislo), JeRadovka_(je_radovka), JeRad_(je_rad)
{}

Slovnik_cislovky::Slovnik_cislovky()
{
}

void Slovnik_cislovky::PushBack(string_t text, std::shared_ptr<Cislo_data> id)
{
    Data_.insert(std::make_pair(text,id));
    return;
}

std::shared_ptr<Cislo_data> Slovnik_cislovky::Hledej(string_t text)
{
    std::map<string_t,std::shared_ptr<Cislo_data>>::iterator nalezeno = Data_.find(text);
    if(nalezeno == Data_.end()){ return nullptr; }
    else{ return nalezeno->second; }
}
