#include "spatna_pomlcka.h"

Spatna_pomlcka::Spatna_pomlcka() : Plugin("Nalezena pomlcka uprostred slova","spatna_pomlcka")
{
    NactiNastaveni();
}

void Spatna_pomlcka::Reset()
{
    return;
}

int Spatna_pomlcka::Run(Titulek &titulek, Chyby &chyby)
{
    int vysledek = 0;
    size_t pocet_slov = titulek.PocetSlov();

    for(size_t i = 0; i < pocet_slov; ++i)
    {
        string_t slovo = titulek.VratSlovo(i).VratText();
        size_t pozice_slova = titulek.VratSlovo(i).VratPozici();

		if (slovo.length() == 0){ continue; }
		slovo = Text::OdstranTriTecky(slovo);
		slovo = Text::OdstranKonecBloku(slovo);
		slovo = Text::OdstranKonecVety(slovo);
		slovo = Text::OdstranZacatekBloku(slovo);
		if (slovo.length() == 0){ continue; }

        for(auto& pomlcka : Pomlcky_) // projdeme pres vsechny pomlcky zadane v config filu
        {
            size_t found = slovo.find(pomlcka);
            while(found != string_t::npos)
            {
                if(found != 0 && found != (slovo.length() - 1)) // pokud je pomlcka na konci ci zacatku, pak neopravujeme
                {
                    if((slovo.at(found - 1) >= '0' && slovo.at(found -1) <= '9')
                            && (slovo.at(found + 1) >= '0' && slovo.at(found + 1) < '9'))
                        // zkontrolujeme jestli jsme nahodou nenarazili na cislo ve tvaru 20-30
                    {
                        size_t pocet_vlevo = 0;
                        size_t pocet_vpravo = 0;
                        int vlevo = found - 1;
                        size_t vpravo = found + 1;

                        while(vlevo >= 0) // kontrolujeme znaky vlevo od pomlcky
                        {
                            if(slovo.at(vlevo) >= '0' && slovo.at(vlevo) <= '9')
                            {
                                pocet_vlevo++;
                            }
                            vlevo--;
                        }

                        while(vpravo < slovo.length()) // kontrolujeme znaky vpravo od pomlcky
                        {
                            if(slovo.at(vpravo) >= '0' && slovo.at(vpravo) <= '9')
                            {
                                pocet_vpravo++;
                            }
                            vpravo++;
                        }

                        if((pocet_vlevo + pocet_vpravo + pomlcka.length()) == slovo.length())
                            // pocet cisel ve slove s delkou pomlcky dava dohromady delku slova, tudiz nehlasime chybu
                        {
                            found = slovo.find(pomlcka, found + pomlcka.length());
                            continue;
                        }
                    }

                    Chyba temp(NazevPluginu(), NazevChyby(), titulek.VratUID(), pozice_slova + found);
                    string_t oprava = slovo.substr(0, found) + Spojovnik_;
                    if(slovo.length() > (found + pomlcka.length())){ oprava += slovo.substr(found + pomlcka.length()); }
                    temp.PridejOpravu(oprava);
                    chyby.PushBack(temp);

                    vysledek = 1;
                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                }

                found = slovo.find(pomlcka, found + pomlcka.length());
            }
        }
    }

    return vysledek;
}

int Spatna_pomlcka::NactiNastaveni()
{
    bool vysledek = true;

    boost::property_tree::ptree strom = NactiStromNastaveni();
    if(strom.empty() == true){ vysledek = false; }

    try
    {
        Spojovnik_ = strom.get<string_t>("spojovnik");

        boost::property_tree::ptree seznam = strom.get_child("seznam_spatnych_pomlcek");

        for(auto& i : seznam)
        {
            Pomlcky_.push_back(i.second.get_value<string_t>());
        }
    }
    catch(...)
    {
        vysledek = false;
    }

    if(vysledek == false)
    {
        ErrorNastaveni();
        return 1;
    }

    return 0;
}

extern "C" Plugin EXPORT_SPATNA_POMLCKA * vytvor()
{
    return new Spatna_pomlcka();
}

extern "C" void EXPORT_SPATNA_POMLCKA znic(Plugin* p)
{
    delete p;
}
