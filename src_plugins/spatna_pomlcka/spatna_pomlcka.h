#ifndef SPATNA_POMLCKA_H
#define SPATNA_POMLCKA_H

#ifdef _WIN32
#define EXPORT_SPATNA_POMLCKA __declspec(dllexport)
#else
#define EXPORT_SPATNA_POMLCKA
#endif

#include "../../shared/plugin.h"

class Spatna_pomlcka : public Plugin
{
public:
    Spatna_pomlcka();
    virtual ~Spatna_pomlcka(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    int NactiNastaveni();

    string_t Spojovnik_;
    std::vector<string_t> Pomlcky_;
};

#endif // SPATNA_POMLCKA_H
