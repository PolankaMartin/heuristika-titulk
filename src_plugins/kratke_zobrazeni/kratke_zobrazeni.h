#ifndef KRATKE_ZOBRAZENI_H
#define KRATKE_ZOBRAZENI_H

#ifdef _WIN32
#define EXPORT_KRATKE_ZOBRAZENI __declspec(dllexport)
#else
#define EXPORT_KRATKE_ZOBRAZENI
#endif

#include "../../shared/plugin.h"
#include "../doba_zobrazeni.h"

class Kratke_zobrazeni : public Plugin
{
public:
    Kratke_zobrazeni();
    virtual ~Kratke_zobrazeni(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    ZaznamVector Data_;
    size_t DefaultZobrazeni_; // Pokud symbol nebude ve vektoru Zaznamu, pak se pouzije tato doba k zobrazeni
};

#endif // KRATKE_ZOBRAZENI_H
