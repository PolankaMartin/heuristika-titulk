#include "kratke_zobrazeni.h"


Kratke_zobrazeni::Kratke_zobrazeni()
    : Plugin("Prilis kratke zobrazeni titulku","kratke_zobrazeni")
{
    Doba_zobrazeni::NactiDobyZobrazeni(*this, Data_,DefaultZobrazeni_);
}

void Kratke_zobrazeni::Reset()
{
    return;
}

int Kratke_zobrazeni::Run(Titulek &titulek, Chyby &chyby)
{
    size_t soucet_doby = Doba_zobrazeni::Run(DefaultZobrazeni_,Data_,titulek);

    size_t doba_zobrazeni_titulku = titulek.VratKonec() - titulek.VratZacatek();

    if(soucet_doby > doba_zobrazeni_titulku)
    {
        Chyba pridej(NazevPluginu(),NazevChyby(),titulek.VratUID(),0);
        pridej.PridejOpravu(Titulek::CasNaString(titulek.VratZacatek(),soucet_doby));
        chyby.PushBack(pridej);

        Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));

        return 1;
    }

    return 0;
}

extern "C" Plugin EXPORT_KRATKE_ZOBRAZENI * vytvor()
{
    return new Kratke_zobrazeni();
}

extern "C" void EXPORT_KRATKE_ZOBRAZENI znic(Plugin* p)
{
    delete p;
}
