#ifndef SPATNY_ZAPIS_CISEL_H
#define SPATNY_ZAPIS_CISEL_H

#ifdef _WIN32
#define EXPORT_SPATNY_ZAPIS_CISEL __declspec(dllexport)
#else
#define EXPORT_SPATNY_ZAPIS_CISEL
#endif

#include "../../shared/plugin.h"

class Spatny_zapis_cisel : public Plugin
{
public:
    Spatny_zapis_cisel();
    virtual ~Spatny_zapis_cisel(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
};

#endif // SPATNY_ZAPIS_CISEL_H
