#include "spatny_zapis_cisel.h"

Spatny_zapis_cisel::Spatny_zapis_cisel() : Plugin("Spatny zapis cisla","spatny_zapis_cisel")
{
}

void Spatny_zapis_cisel::Reset()
{
    return;
}

int Spatny_zapis_cisel::Run(Titulek &titulek, Chyby &chyby)
{
    /*
     * Plugin, ktery pokud narazi ve slove na cislovku, tak zkontroluje jestli je zapsana spravne
     * Postup kontroly je jednoduchy a obecny v zasade pokud slovo obsahuje cislovku
     * a pritom obsahuje i nejake jine znaky (krome tecky, carky a pomlcky),
     * tak se zahlasi jako chybne... Z duvodu obecnosti kontroly a detekce se nenavrhuje oprava.
     * Navrhuje se pouze v pripade ze mame ve slove cislo a za nim nejake znaky (napr: 45-ti),
     * pak se navrhne jako oprava jen samotne cislo.
     */

    int vysledek = 0;

    size_t pocet_slov = titulek.PocetSlov();
    for(size_t i = 0; i < pocet_slov; ++i)
    {
        string_t slovo = titulek.VratSlovo(i).VratText();
        size_t pozice = titulek.VratSlovo(i).VratPozici();

        if(slovo.length() == 0){ continue; }

        // odstranime ze slova vsechny nepotrebne zbytecnosti
        slovo = Text::OdstranKonecBloku(slovo);
        slovo = Text::OdstranKonecVety(slovo);
        slovo = Text::OdstranTriTecky(slovo);
        slovo = Text::OdstranZacatekBloku(slovo);

        size_t pocet_cisel = 0; // pocet ciselnych znaku ve slove
        size_t pocet_carek_tecek = 0; // ktere jsou jenom uprostred slova! (tecka i na zacatku)
        size_t pocet_minusu = 0; // muze byt i uprostred slova jako vyjadreni rozsahu
        size_t ostatni_znaky = 0;

        auto slovo_end = slovo.end();
        auto slovo_begin = slovo.begin();
        size_t znaky_na_konci = 0; // slouzi pro pripadne opravy a k overeni, ze znaky mimo tecku, carku, minus a cisla jsou jenom na konci
        size_t posledni_cislo = 0; // pozice posledniho cisla ve slove
        size_t pozice_posledni_znak = 0; // pozice posledniho znaku ve slove
        string_t::const_iterator posledni_znak; // posledni znak ve slove
        for(auto znak_it = slovo_begin; znak_it < slovo_end;)
        {
            size_t znak = utf8::next(znak_it, slovo_end);

            if(znak >= '0' && znak <= '9')
            {
                pocet_cisel++;
                znaky_na_konci = 0; // prekazime znakum serii pokud byli uz pred cislem nebo mezi cisly
                posledni_cislo = znak_it - slovo_begin;
            }
            else if(znak == ',' && znak_it != slovo_begin)
            {
                pocet_carek_tecek++;
                znaky_na_konci = 0;
            }
            else if(znak == '.')
            {
                pocet_carek_tecek++;
                znaky_na_konci = 0;
            }
            else if(znak == '-' && znak_it != (slovo_end - 1)){ pocet_minusu++; }
            else
            {
                ostatni_znaky++;
                znaky_na_konci++;
                pozice_posledni_znak = znak_it - slovo_begin;
                posledni_znak = znak_it;
            }
        }

        if(pocet_cisel > 0) // ve slove bylo nalezeno cislo, zkontrolujeme jestli v sobe neobsahuje jine znaky
        {
            // slovo obsahujici cislo obsahuje i jine znaky... hlasime chybu
            if(pocet_carek_tecek > 1 || pocet_minusu > 2 || ostatni_znaky != 0)
            {
                Chyba pridej(NazevPluginu(), NazevChyby() + ": " + slovo, titulek.VratUID(), pozice);
                if(znaky_na_konci == ostatni_znaky && ostatni_znaky != 0) // pokud byli znaky jen na konci, pak navrhneme opravu
                {
                    pridej.PridejOpravu(slovo.substr(0, posledni_cislo + 1));
                }
                else if(ostatni_znaky == 1 && pocet_carek_tecek < 2 && pocet_minusu < 3)
                {
                    size_t znak_delka = utf8::internal::sequence_length(posledni_znak);

                    pridej.PridejOpravu(slovo.substr(0, pozice_posledni_znak) + slovo.substr(pozice_posledni_znak + znak_delka));
                }
                chyby.PushBack(pridej);

                Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                vysledek = 1;
            }
        }
    }

    return vysledek;
}

extern "C" Plugin EXPORT_SPATNY_ZAPIS_CISEL * vytvor()
{
    return new Spatny_zapis_cisel();
}

extern "C" void EXPORT_SPATNY_ZAPIS_CISEL znic(Plugin* p)
{
    delete p;
}
