#ifndef DOBA_ZOBRAZENI_H
#define DOBA_ZOBRAZENI_H

#include "../shared/titulek.h"
#include "../shared/plugin.h"

class Zaznam
{
public:
    Zaznam() = delete;
    Zaznam(const Zaznam& source) = default;
    Zaznam& operator=(const Zaznam& source) = default;
#ifdef _WIN32
	/* Defaultni move metody neumi Visual Studio 2013 aktualne prelozit a bohuzel ani generovat... */
#else
	Zaznam(Zaznam&& source) = default;
	Zaznam& operator=(Zaznam&& source) = default;
#endif

    Zaznam(string_t symbol, size_t doba);

    bool operator<(const Zaznam& right) const;
    bool operator>(const Zaznam& right) const;

    string_t& VratSymbol();
    size_t VratDobu();
private:
    string_t Symbol_;
    size_t DobaZobrazeni_; // v milisekundach
};

typedef std::vector<Zaznam> ZaznamVector;

class Doba_zobrazeni
{
public:
    Doba_zobrazeni() = delete;
    Doba_zobrazeni(const Doba_zobrazeni& source) = delete;
    Doba_zobrazeni& operator=(const Doba_zobrazeni& source) = delete;

    static size_t Run(size_t default_zobrazeni, ZaznamVector& zaznamy, Titulek& titulek);
    static int NactiDobyZobrazeni(Plugin& plugin ,ZaznamVector& zaznamy, size_t& default_zobr);
};

#endif // DOBA_ZOBRAZENI_H
