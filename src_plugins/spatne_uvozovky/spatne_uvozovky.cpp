#include "spatne_uvozovky.h"

Spatne_uvozovky::Spatne_uvozovky() : Plugin("Spatne uvozovky","spatne_uvozovky"), RozpracovanaVeta_(false)
{
}

void Spatne_uvozovky::Reset()
{
    RozpracovanaVeta_ = false;
}

int Spatne_uvozovky::Run(Titulek &titulek, Chyby &chyby)
{
    /*
     * Nebude se zajistovat hledani apostrofu, ci uvozovek uprostred slova. Na tento problem pak bude alespon nejakym zpusobem upozornovat plugin detekujici neznama slova.
     *
     * Jsou 2 typy obsahu v uvozovkach:
     *  1) Prima rec a doslovne citaty -> oddelovace se na konci pisi pred ukoncujici uvozovkou (obvykle je to cela veta a zacina velkym pismenem)
     *  2) Slova, ktera se chteji zduraznit; ironie apod -> oddelovace se na konci pisi za ukoncujici uvozovkou (obvykle jednoslovne nebo kratke vyrazy, zacinajici malym pismenem)
     */

    int vysledek = 0;

    size_t pocet_slov = titulek.PocetSlov();
    SizeVector otevrenych_uvozovek;
    SizeVector pocet_slov_v_uvozovkach;
    SizeVector otevrenych_apostrofu;
    SizeVector pocet_slov_v_apostrofech;

    for(size_t i = 0; i < pocet_slov; ++i)
    {
        string_t slovo = titulek.VratSlovo(i).VratText();
        size_t pozice = titulek.VratSlovo(i).VratPozici();
        size_t velikost_slova = slovo.length();

		if (velikost_slova == 0){ continue; }

        string_t zacatek;
        string_t konec_jedna;
        string_t konec_dva;
        if(velikost_slova > 0){ zacatek = slovo.substr(0,1); }

		if (slovo == "\"\"" || slovo == "\'\'")
		{
			if (zacatek == "\"")
			{
				Chyba pridej(NazevPluginu(), "Prazdne uvozovky", titulek.VratUID(), pozice);
				chyby.PushBack(pridej);
			}
			else
			{
				Chyba pridej(NazevPluginu(), "Prazdne apostrofy", titulek.VratUID(), pozice);
				chyby.PushBack(pridej);
			}

			Log_.Zapis(NazevPluginu(), "Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
			vysledek = 1;
			continue;
		}


        /* Do vsech otevrenych uzovovek a apostrofu pripocteme jedno slovo */
        for(size_t k = 0; k < pocet_slov_v_uvozovkach.size(); ++k)
        { pocet_slov_v_uvozovkach[k]++; }
        for(size_t k = 0; k < pocet_slov_v_apostrofech.size(); ++k)
        { pocet_slov_v_apostrofech[k]++; }


        if(zacatek == "\"" || zacatek == "\'")
        {
            slovo = Text::OdstranZacatekBloku(slovo);
            velikost_slova = slovo.length();

            if(zacatek == "\"")
            {
                otevrenych_uvozovek.push_back(pozice);
                pocet_slov_v_uvozovkach.push_back(1);
            }
            else
            {
                otevrenych_apostrofu.push_back(pozice);
                pocet_slov_v_apostrofech.push_back(1);
            }

            if(RozpracovanaVeta_ == false && Text::JeVelke(slovo,0))
            {
                RozpracovanaVeta_ = true;
            }
        }

        if(velikost_slova > 0){ konec_jedna = slovo.substr(velikost_slova-1); }
        if(velikost_slova > 1){ konec_dva = slovo.substr(velikost_slova-2); }

        if(konec_dva == ".\"" || konec_dva == ".\'" || konec_dva == ",\"" || konec_dva == ",\'" ||
                konec_dva == "!\"" || konec_dva == "!\'" || konec_dva == "?\"" || konec_dva == "?\'") // narazilo se na konec prvniho typu uvozovek
        {
            if(konec_dva.back() == '\"')
            {
                if(otevrenych_uvozovek.empty() == true) // chyba, nejsou zadne prave otevrene uvozovky
                {
                    Chyba pridej(NazevPluginu(),"Spatne koncove uvozovky, nebyli nalezeny oteviraci uvozovky",titulek.VratUID(),pozice);
                    chyby.PushBack(pridej);

                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                    vysledek = 1;
                    continue;
                }
                else // zbavime se otevrenych uvozovek
                {
                    // pokud mame vyraz v uvozovkach delsi nez 2 slova, pak nastavime rozpracovanou vetu na true
                    size_t tmp = pocet_slov_v_uvozovkach.back();
                    if(tmp > 2){ RozpracovanaVeta_ = true; }

                    otevrenych_uvozovek.pop_back();
                    pocet_slov_v_uvozovkach.pop_back();
                }
            }
            else
            {
                if(otevrenych_apostrofu.empty() == true) // chyba, nejsou zadne prave otevrene apostrofy
                {
                    Chyba pridej(NazevPluginu(),"Spatne koncove apostrofy, nebyli nalezeny hodici se oteviraci",titulek.VratUID(),pozice);
                    chyby.PushBack(pridej);

                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                    vysledek = 1;
                    continue;
                }
                else // zbavime se otevrenych apostrofu
                {
                    // pokud mame vyraz v apostrofech delsi nez 2 slova, pak nastavime rozpracovanou vetu na true
                    size_t tmp = pocet_slov_v_apostrofech.back();
                    if(tmp > 2){ RozpracovanaVeta_ = true; }

                    otevrenych_apostrofu.pop_back();
                    pocet_slov_v_apostrofech.pop_back();
                }
            }

            if(RozpracovanaVeta_ == false) // napsan konec typu 1, ale vyzadovan konec typu dva => chyba
            {
                string_t popis_uvozovky;

                if(konec_dva.back() == '\"')
                {
                    popis_uvozovky = "uvozovky";
                }
                else
                {
                    popis_uvozovky = "apostrofu";
                }

                Chyba pridej(NazevPluginu(),"Spatne poradi " + popis_uvozovky + " a oddelovace",titulek.VratUID(),pozice);

                string_t oprava = titulek.VratSlovo(i).VratText();
                oprava = oprava.substr(0,oprava.length()-2);
                pridej.PridejOpravu(oprava + string_t(konec_dva.at(1),1) + string_t(konec_dva.at(0),1));

                chyby.PushBack(pridej);

                Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                vysledek = 1;
            }

            if(konec_dva == ".\"" || konec_dva == ".\'" || konec_dva == "!\"" ||
                    konec_dva == "!\'" || konec_dva == "?\"" || konec_dva == "?\'") // konec rozpracovane vety
            {
                RozpracovanaVeta_ = false;
            }
        }
        else if(konec_dva == "\"." || konec_dva == "\'." || konec_dva == "\"," || konec_dva == "\'," ||
                konec_dva == "\"!" || konec_dva == "\'!" || konec_dva == "\"?" || konec_dva == "\'?") // narazilo se na konec druheho typu uvozovek
        {
            size_t pocet_slov_v_bloku = 1; // promenna ktera rika kolik slov bylo v aktualne ukoncenem bloku uvozovek

            if(konec_dva.at(0) == '\"')
            {
                if(otevrenych_uvozovek.empty() == true) // chyba, nejsou zadne prave otevrene uvozovky
                {
                    Chyba pridej(NazevPluginu(),"Spatne koncove uvozovky, nebyli nalezeny oteviraci uvozovky",titulek.VratUID(),pozice);
                    chyby.PushBack(pridej);

                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                    vysledek = 1;
                    continue;
                }
                else // zbavime se otevrenych uvozovek
                {
                    pocet_slov_v_bloku = pocet_slov_v_uvozovkach.back();

                    otevrenych_uvozovek.pop_back();
                    pocet_slov_v_uvozovkach.pop_back();
                }
            }
            else
            {
                if(otevrenych_apostrofu.empty() == true) // chyba, nejsou zadne prave otevrene apostrofy
                {
                    Chyba pridej(NazevPluginu(),"Spatne koncove apostrofy, nebyli nalezeny hodici se oteviraci",titulek.VratUID(),pozice);
                    chyby.PushBack(pridej);

                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                    vysledek = 1;
                    continue;
                }
                else
                {
                    pocet_slov_v_bloku = pocet_slov_v_apostrofech.back();

                    otevrenych_apostrofu.pop_back();
                    pocet_slov_v_apostrofech.pop_back();
                }
            }

            if(RozpracovanaVeta_ == true && pocet_slov_v_bloku > 2) // napsan konec typu 2, ale vyzadovan konec typu jedna => chyba
            {
                string_t popis_uvozovky;

                if(konec_dva.back() == '\"')
                {
                    popis_uvozovky = "uvozovky";
                }
                else
                {
                    popis_uvozovky = "apostrofu";
                }

                Chyba pridej(NazevPluginu(),"Spatne poradi " + popis_uvozovky + " a oddelovace",titulek.VratUID(),pozice);

                string_t oprava = titulek.VratSlovo(i).VratText();
                oprava = oprava.substr(0,oprava.length()-2);
                pridej.PridejOpravu(oprava + konec_dva.substr(1) + konec_dva.substr(0,1));

                chyby.PushBack(pridej);

                Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                vysledek = 1;

                if(konec_dva == "\"." || konec_dva == "\'." || konec_dva == "\"!" ||
                        konec_dva == "\'!" || konec_dva == "\"?" || konec_dva == "\'?") // kdyby nahodou, tak se ukonci rozpracovana veta
                {
                    RozpracovanaVeta_ = false;
                }
            }
        }
        else if(konec_jedna == "\"" || konec_jedna == "\'") // pokud by na konci byli jen uvozovky
        {
            if(konec_jedna.back() == '\"')
            {
                if(otevrenych_uvozovek.empty() == true) // chyba, nejsou zadne prave otevrene uvozovky
                {
                    Chyba pridej(NazevPluginu(),"Spatne koncove uvozovky, nebyli nalezeny oteviraci uvozovky",titulek.VratUID(),pozice);
                    chyby.PushBack(pridej);

                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                    vysledek = 1;
                    continue;
                }
                else{ otevrenych_uvozovek.pop_back(); pocet_slov_v_uvozovkach.pop_back(); }
            }
            else
            {
                if(otevrenych_apostrofu.empty() == true) // chyba, nejsou zadne prave otevrene apostrofy
                {
                    Chyba pridej(NazevPluginu(),"Spatne koncove apostrofy, nebyli nalezeny hodici se oteviraci",titulek.VratUID(),pozice);
                    chyby.PushBack(pridej);

                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                    vysledek = 1;
                    continue;
                }
                else{ otevrenych_apostrofu.pop_back(); pocet_slov_v_apostrofech.pop_back(); }
            }
        }
    }

    for(auto& i : otevrenych_uvozovek)
    {
        Chyba pridej(NazevPluginu(),"Otevrene, ale uz neuzavrene uvozovky",titulek.VratUID(),i);
        chyby.PushBack(pridej);

        Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
        vysledek = 1;
    }

    for(auto& i : otevrenych_apostrofu)
    {
        Chyba pridej(NazevPluginu(),"Otevrene, ale uz neuzavrene apostrofy",titulek.VratUID(),i);
        chyby.PushBack(pridej);

        Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
        vysledek = 1;
    }

    return vysledek;
}

extern "C" Plugin EXPORT_SPATNE_UVOZOVKY * vytvor()
{
    return new Spatne_uvozovky();
}

extern "C" void EXPORT_SPATNE_UVOZOVKY znic(Plugin* p)
{
    delete p;
}
