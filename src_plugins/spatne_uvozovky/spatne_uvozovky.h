#ifndef SPATNE_UVOZOVKY_H
#define SPATNE_UVOZOVKY_H

#ifdef _WIN32
#define EXPORT_SPATNE_UVOZOVKY __declspec(dllexport)
#else
#define EXPORT_SPATNE_UVOZOVKY
#endif

#include "../../shared/plugin.h"

class Spatne_uvozovky : public Plugin
{
public:
    Spatne_uvozovky();
    virtual ~Spatne_uvozovky(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    bool RozpracovanaVeta_; // true, pokud se nasli uvozovky 1) typu, tedy prima rec, nebo doslovne citaty, je true, dokud neni nalezena tecka
};

#endif // SPATNE_UVOZOVKY_H
