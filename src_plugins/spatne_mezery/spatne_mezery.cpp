#include "spatne_mezery.h"

Spatne_mezery::Spatne_mezery() : Plugin("Spatne umistena mezera","spatne_mezery")
{
}

void Spatne_mezery::Reset()
{
    return;
}

int Spatne_mezery::Run(Titulek &titulek, Chyby &chyby)
{
    int vysledek = 0;

    //size_t konec_predchoziho_slova = 0;
    size_t konec_predchoziho_slova_orig = 0;
    //string_t predchozi_slovo = "";
    string_t predchozi_slovo_orig = "";

    size_t pocet_slov = titulek.PocetSlov();

    for(size_t i = 0; i < pocet_slov; ++i)
    {
        string_t slovo = titulek.VratSlovo(i).VratText();
        string_t slovo_orig = titulek.VratSlovo(i).VratOrigText();
        //size_t velikost_slova = slovo.length();
        size_t velikost_slova_orig = slovo_orig.length();
        size_t pozice_slova = titulek.VratSlovo(i).VratPozici();
        size_t pozice_slova_orig = titulek.VratSlovo(i).VratPoziciOrig();

        slovo = Text::OdstranTriTecky(slovo);
        size_t velikost_slova = slovo.length();

        if(slovo == "")
        {
            continue;
        }

        if((pozice_slova_orig - konec_predchoziho_slova_orig) > 1 || (i == 0 && pozice_slova_orig > 0)) // detekuje vicenasobnou mezeru mezi dvema slovy
        {
            Chyba pridej(NazevPluginu(),"Vicenasobna mezera",titulek.VratUID(),konec_predchoziho_slova_orig);
            pridej.PridejOpravu(predchozi_slovo_orig + slovo_orig);
            chyby.PushBack(pridej);

            Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
            vysledek = 1;
        }

        std::vector<std::tuple<char,string_t,string_t>> oddelovace;
        oddelovace.push_back(std::tuple<char,string_t,string_t>('.',"teckou","Tecka"));
        oddelovace.push_back(std::tuple<char,string_t,string_t>(',',"carkou","Carka"));
        oddelovace.push_back(std::tuple<char,string_t,string_t>('!',"vykricnikem","Vykricnik"));
        oddelovace.push_back(std::tuple<char,string_t,string_t>('?',"otaznikem","Otaznik"));

        for(auto& tup : oddelovace)
        {
            char oddelovac = std::get<0>(tup);
            string_t chyba_1(std::get<1>(tup));
            string_t chyba_2(std::get<2>(tup));

            size_t oddelovac_pozice = slovo.find(oddelovac);

            while(oddelovac_pozice != string_t::npos)
            {
                string_t dve;
                if(velikost_slova > 1){ dve = slovo.substr(velikost_slova - 2); }

                if(oddelovac == ',') // testuje se jestli slovo neni cislo (pouze v ceske notaci)
                {
                    size_t pocet_cisel = 0;
                    size_t pocet_oddel = 0;
                    size_t zac = 0;
                    if(slovo.at(0) == '-'){ zac++; pocet_cisel++; }

                    for(size_t k = zac; k < velikost_slova; ++k)
                    {
                        char pism = slovo.at(k);
                        if((pism < '0' || pism > '9') && pism != ','){ break; }
                        else
                        {
                            if(pism == ','){ pocet_oddel++; }
                            pocet_cisel++;
                        }
                    }

                    if(pocet_cisel == velikost_slova && pocet_oddel < 2){ break; }
                }
                if(oddelovac_pozice == (velikost_slova - 1) && velikost_slova != 1){ break; } // oddelovac spravne na konci slova, koncime...
                if(oddelovac_pozice == (velikost_slova - 2) && velikost_slova != 2 && (dve == (std::string(1,oddelovac) + "\"") 
					|| dve == (std::string(1,oddelovac) + "\'"))){ break; } // oddelovac spravne pred uvozovkami (na konci), koncime...
                if(oddelovac_pozice == 0) // oddelovac na zacatku => spatna mezera pred nim a chybejici mezera za nim
                {
                    Chyba pridej(NazevPluginu(),"Spatne umistena mezera pred " + chyba_1,titulek.VratUID(),konec_predchoziho_slova_orig);
                    pridej.PridejOpravu(predchozi_slovo_orig.substr(0,velikost_slova-1) + std::string(1,oddelovac) + " " + slovo_orig.substr(0,pozice_slova-pozice_slova_orig) + slovo_orig.substr(pozice_slova-pozice_slova_orig+1));
                    chyby.PushBack(pridej);

                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                    vysledek = 1;
                }
                else // oddelovac uprostred slova
                {
                    Chyba pridej(NazevPluginu(),chyba_2 + " uprostred slova",titulek.VratUID(),oddelovac_pozice);
                    pridej.PridejOpravu(slovo_orig.substr(0,(pozice_slova-pozice_slova_orig)+oddelovac_pozice+1) + " " + slovo_orig.substr((pozice_slova-pozice_slova_orig)+oddelovac_pozice+1));
                    chyby.PushBack(pridej);

                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                    vysledek = 1;
                }

                oddelovac_pozice = slovo.find(oddelovac,oddelovac_pozice+1);
            }
        }

        //konec_predchoziho_slova = pozice_slova + velikost_slova;
        konec_predchoziho_slova_orig = pozice_slova_orig + velikost_slova_orig;
        //predchozi_slovo = slovo + " ";
        predchozi_slovo_orig = slovo_orig + " ";
    }

    return vysledek;
}

extern "C" Plugin EXPORT_SPATNE_MEZERY * vytvor()
{
    return new Spatne_mezery();
}

extern "C" void EXPORT_SPATNE_MEZERY znic(Plugin* p)
{
    delete p;
}
