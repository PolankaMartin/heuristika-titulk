#ifndef SPATNE_MEZERY_H
#define SPATNE_MEZERY_H

#ifdef _WIN32
#define EXPORT_SPATNE_MEZERY __declspec(dllexport)
#else
#define EXPORT_SPATNE_MEZERY
#endif

#include "../../shared/plugin.h"

class Spatne_mezery : public Plugin
{
public:
    Spatne_mezery();
    virtual ~Spatne_mezery(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
};

#endif // SPATNE_MEZERY_H
