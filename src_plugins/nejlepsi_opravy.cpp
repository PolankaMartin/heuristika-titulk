#include "nejlepsi_opravy.h"


NejlepsiOpravyKontejner::NejlepsiOpravyKontejner(string_t puv_slovo, size_t max_oprav, size_t max_vzd)
    : PuvodniSlovo_(puv_slovo), MaxPocetOprav_(max_oprav), MaxVzdalenost_(max_vzd), MaxVKontejneru_(string_t::npos)
{}

int NejlepsiOpravyKontejner::PridejOpravu(size_t levenshtein_vzd, string_t oprava)
{
    // levenshteinova vzdalenost ktera prisla je vetsi nez maximalni povolena
    if(MaxVzdalenost_ != 0 && levenshtein_vzd > MaxVzdalenost_){ return 1; }

    // pokud je levenshteinova vzdalenost mensi nez maximalni vzdalenost, ktera je prave v kontejneru
    // nebo pokud kontejner oprav neni plne naplnen, tak se do nej prida oprava
    if(levenshtein_vzd <= MaxVKontejneru_ || NejlepsiOpravy_.size() < MaxPocetOprav_)
    {
        NejlepsiOpravy_.insert(std::make_pair(levenshtein_vzd, oprava));

        // pokud jsme pridanim opravy do kontejneru zvetsili jeho velikost pres maximalni hodnotu,
        // pak vymazeme posledni prvek
        if(MaxPocetOprav_ != 0 && NejlepsiOpravy_.size() > MaxPocetOprav_)
        {
            auto konec = NejlepsiOpravy_.end();
            NejlepsiOpravy_.erase(--konec);
        }

        // ziskame nejvetsi hodnotu levenshteinovy vzdalenosti v kontejneru
        auto konec = NejlepsiOpravy_.end();
        --konec;
        MaxVKontejneru_ = konec->first;

        return 0;
    }
    else{ return 1; }
}

std::multimap<size_t, string_t>& NejlepsiOpravyKontejner::VratNejlepsiOpravy()
{
    return NejlepsiOpravy_;
}

const string_t& NejlepsiOpravyKontejner::VratPuvodniSlovo()
{
    return PuvodniSlovo_;
}
