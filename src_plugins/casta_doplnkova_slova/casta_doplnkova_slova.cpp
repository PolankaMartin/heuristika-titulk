#include "casta_doplnkova_slova.h"

Casta_doplnkova_slova::Casta_doplnkova_slova() : Plugin("Casta doplnkova slova","casta_doplnkova_slova"),
    MaxRuznychVeVete_(0), MaxStejnychVeVete_(0), MaxVTitulku_(0), RuznychVeVete_(0), PoziceZacatkuVety_(0),
    UIDZacatkuVety_(1), PredchoziBylKonecVety_(true)
{
    NactiNastaveni();
}

void Casta_doplnkova_slova::Reset()
{
    RuznychVeVete_ = 0;
    PoziceZacatkuVety_ = 0;
    UIDZacatkuVety_ = 1;
    PredchoziBylKonecVety_ = true;
    StejnychVeVete_.clear();
}

int Casta_doplnkova_slova::Run(Titulek &titulek, Chyby &chyby)
{
    int vysledek = 0;

    size_t pocet_slov = titulek.PocetSlov();

    size_t v_titulku = 0;

    for(size_t i = 0; i < pocet_slov; ++i)
    {
        string_t slovo = titulek.VratSlovo(i).VratZmensenyText();
        size_t pozice = titulek.VratSlovo(i).VratPoziciZmenseny();

        bool konec_vety;

        slovo = Text::OdstranZacatekBloku(slovo);
        konec_vety = Text::JeKonecVety(slovo);
        slovo = Text::OdstranKonecBloku(slovo);

        if(PredchoziBylKonecVety_ == true)
        {
            PoziceZacatkuVety_ = pozice;
            UIDZacatkuVety_ = titulek.VratUID();

            PredchoziBylKonecVety_ = false;
        }

        size_t id = Slovnik_.HledejId(slovo);
        size_t maxsize = size_t() -1;
        if(id != maxsize) // nalezeno doplnkove slovo
        {
            v_titulku++;
            RuznychVeVete_++;

            auto it = StejnychVeVete_.find(id);
            if(it != StejnychVeVete_.end())
            {
                it->second += 1;
            }
            else
            {
                StejnychVeVete_.insert(std::make_pair(id,1));
            }
        }

        if(konec_vety == true) // jsme na konci vety, vyhodnocujeme predchozi vetu a nulujeme citace
        {
            if(RuznychVeVete_ > MaxRuznychVeVete_)
            {
                Chyba pridej(NazevPluginu(),"Casta doplnkova slova ve vete",UIDZacatkuVety_,PoziceZacatkuVety_);
                chyby.PushBack(pridej);

                Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                vysledek = 1;
            }
            for(auto& j : StejnychVeVete_)
            {
                if(j.second > MaxStejnychVeVete_)
                {
                    Chyba pridej(NazevPluginu(),"Caste doplnkove slovo \"" + Slovnik_.at(j.first) + "\" ve vete",UIDZacatkuVety_,PoziceZacatkuVety_);
                    chyby.PushBack(pridej);

                    Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                    vysledek = 1;
                }
            }

            RuznychVeVete_ = 0;
            StejnychVeVete_.clear();
            PredchoziBylKonecVety_ = true;
        }
    }

    if(v_titulku > MaxVTitulku_)
    {
        Chyba pridej(NazevPluginu(),"Casta doplnkova slova v titulku",titulek.VratUID(),0);
        chyby.PushBack(pridej);

        Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
        vysledek = 1;
    }

    return vysledek;
}

int Casta_doplnkova_slova::NactiNastaveni()
{
    bool vysledek = true;

    boost::property_tree::ptree strom = NactiStromNastaveni();
    if(strom.empty() == true){ vysledek = false; }

    try
    {
        MaxRuznychVeVete_ = strom.get<size_t>("max_ruznych_ve_vete");
        MaxStejnychVeVete_ = strom.get<size_t>("max_stejnych_ve_vete");
        MaxVTitulku_ = strom.get<size_t>("max_v_titulku");

        boost::property_tree::ptree seznam = strom.get_child("seznam_doplnkovych_slov");

        for(auto& i : seznam)
        {
            Slovnik_.PushBack(Text::NaMala(i.second.get_value<string_t>()));
        }
    }
    catch(...)
    {
        vysledek = false;
    }

    if(vysledek == false)
    {
        ErrorNastaveni();
        return 1;
    }

    return 0;
}

extern "C" Plugin EXPORT_DOPLNKOVA_SLOVA * vytvor()
{
    return new Casta_doplnkova_slova();
}

extern "C" void EXPORT_DOPLNKOVA_SLOVA znic(Plugin* p)
{
    delete p;
}
