#ifndef CASTA_DOPLNKOVA_SLOVA_H
#define CASTA_DOPLNKOVA_SLOVA_H

#ifdef _WIN32
#define EXPORT_DOPLNKOVA_SLOVA __declspec(dllexport)
#else
#define EXPORT_DOPLNKOVA_SLOVA
#endif

#include "../../shared/plugin.h"

class Casta_doplnkova_slova : public Plugin
{
public:
    Casta_doplnkova_slova();
    virtual ~Casta_doplnkova_slova(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    int NactiNastaveni();

    Slovnik<string_t> Slovnik_;
    size_t MaxRuznychVeVete_;
    size_t MaxStejnychVeVete_;
    size_t MaxVTitulku_;

    size_t RuznychVeVete_;
    std::map<size_t,size_t> StejnychVeVete_;

    size_t PoziceZacatkuVety_;
    size_t UIDZacatkuVety_;
    bool PredchoziBylKonecVety_;
};

#endif // CASTA_DOPLNKOVA_SLOVA_H
