#ifndef DOBA_ZOBRAZENI_H
#define DOBA_ZOBRAZENI_H

#include "../shared/plugin.h"

class NejlepsiOpravyKontejner
{
public:
    NejlepsiOpravyKontejner(string_t puv_slovo, size_t max_oprav, size_t max_vzd);
    ~NejlepsiOpravyKontejner(){}

    int PridejOpravu(size_t levenshtein_vzd, string_t oprava);
    std::multimap<size_t, string_t>& VratNejlepsiOpravy();
    const string_t& VratPuvodniSlovo();

private:
    std::multimap<size_t, string_t> NejlepsiOpravy_;
    string_t PuvodniSlovo_;
    size_t MaxPocetOprav_;
    size_t MaxVzdalenost_;
    size_t MaxVKontejneru_;
};

#endif // DOBA_ZOBRAZENI_H
