#include "kontrola_preklepu_kombinace.h"


Kontrola_preklepu_kombinace::Kontrola_preklepu_kombinace()
    : Plugin("Detekovan preklep","kontrola_preklepu_kombinace"), Opravovat_(false)
{
    NactiNastaveni();
}

void Kontrola_preklepu_kombinace::Reset()
{
    return;
}

int Kontrola_preklepu_kombinace::Run(Titulek &titulek, Chyby &chyby)
{
    int vysledek = 0;
    size_t pocet_slov = titulek.PocetSlov();

    for(size_t i = 0; i < pocet_slov; ++i)
    {
        string_t slovo = titulek.VratSlovo(i).VratZmensenyText();
        size_t pozice = titulek.VratSlovo(i).VratPoziciZmenseny();

        // odstranime ze slova vsechny nepotrebne veci
        if(slovo.length() == 0){ continue; }
        if(slovo.find_first_of("0123456789") != string_t::npos){ continue; }
		slovo = Text::OdstranTriTecky(slovo);
        slovo = Text::OdstranKonecBloku(slovo);
        slovo = Text::OdstranKonecVety(slovo);
        slovo = Text::OdstranZacatekBloku(slovo);
        if(slovo.length() == 0){ continue; }

        auto slovo_end = slovo.end();

        for(auto it = slovo.begin(); it != slovo_end;)
        {
            string_t trigram;
            bool nalezeno = false;
            NejlepsiOpravyKontejner kont(slovo, MaxPocetOprav_, 1);

            size_t all_three = 1;
            size_t prvni_delka = utf8::internal::sequence_length(it); // delka prvni utf8 znaku
            size_t char1 = utf8::next(it, slovo_end);
            size_t char2;
            size_t char3;
            auto it_ = it;
            if(it != slovo_end)
            {
                char2 = utf8::next(it_, slovo_end);
                all_three++;

                if(it_ != slovo_end)
                {
                    char3 = utf8::next(it_, slovo_end);
                    all_three++;
                }
            }

            if(all_three == 3)
            {
                utf8::unchecked::append(char1, std::back_inserter(trigram));
                utf8::unchecked::append(char2, std::back_inserter(trigram));
                utf8::unchecked::append(char3, std::back_inserter(trigram));
            }
            else{ break; }

            // nejdrive zjistime jestli nahodou neni trigram ve slovniku pomoci bin. vyhledavani
            if(std::binary_search(Wordlist_.begin(), Wordlist_.end(), trigram)){ continue; }

            if(Opravovat_ == false)
            {
                Chyba pridej(NazevPluginu(), NazevChyby() + " ve slove \"" + slovo + "\"", titulek.VratUID(), pozice);
                chyby.PushBack(pridej);

                Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                vysledek = 1;
                continue;
            }

            // trigram neni ve slovniku a proto potrebujeme najit k nemu nejblizsi moznosti
            for(auto& wordlist_entry : Wordlist_)
            {
                size_t lev = Text::LevenshteinDistance(wordlist_entry, trigram, 1);
                if(lev == 0){ nalezeno = true; break; }
                else
                {
                    kont.PridejOpravu(lev, wordlist_entry);
                }
            }

            // slovo ve slovniku nebylo nalezeno a vsechny polozky ve wordlistu byly projity,
            // ted uz se jen vypisi ta nejblizsi slova
            if(nalezeno == false && kont.VratNejlepsiOpravy().size() != 0)
            {
                Chyba pridej(NazevPluginu(), NazevChyby() + " ve slove \"" + slovo + "\"", titulek.VratUID(), pozice);
                for(auto& i : kont.VratNejlepsiOpravy())
                {
                    pridej.PridejOpravu(slovo.substr(0, it - slovo.begin() - prvni_delka) + i.second
                                        + slovo.substr(it_ - slovo.begin()));
                }
                chyby.PushBack(pridej);

                Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
                vysledek = 1;
            }
        }
    }

    return vysledek;
}

int Kontrola_preklepu_kombinace::NactiNastaveni()
{
    bool vysledek = true;

    boost::property_tree::ptree strom = NactiStromNastaveni();
    if(strom.empty() == true){ vysledek = false; }

    Opravovat_ = strom.get("navrh_oprav.zapnout", false);
    MaxPocetOprav_ = strom.get("navrh_oprav.max_pocet_oprav", 0);
    Kapacita_ = strom.get("kapacita", 0);
    string_t wordlist_file = strom.get("wordlist_file", "");

    if(MaxPocetOprav_ == 0){ vysledek = false; }

    if(vysledek == false)
    {
        ErrorNastaveni();
        return 1;
    }
    else{ NactiWordlist(wordlist_file); }

    return 0;
}

int Kontrola_preklepu_kombinace::NactiWordlist(const string_t &wordlist_file)
{
    /*
     * predpoklada se, ze 3-gramy v souboru budou uz jen s malymi pismeny, serazene
     *  a ze soubor bude ve spravnem formatu, tedy jeden trigram na radek
     */
    std::ifstream soubor(wordlist_file);

    Wordlist_.reserve(Kapacita_);

    string_t radka;
    while(getline(soubor, radka))
    {
        Wordlist_.push_back(radka);
    }

    return 0;
}

extern "C" Plugin EXPORT_KONTROLA_PREKLEPU_KOMBINACE * vytvor()
{
    return new Kontrola_preklepu_kombinace();
}

extern "C" void EXPORT_KONTROLA_PREKLEPU_KOMBINACE znic(Plugin* p)
{
    delete p;
}
