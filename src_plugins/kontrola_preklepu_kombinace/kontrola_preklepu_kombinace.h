#ifndef KONTROLA_PREKLEPU_KOMBINACE_H
#define KONTROLA_PREKLEPU_KOMBINACE_H

#ifdef _WIN32
#define EXPORT_KONTROLA_PREKLEPU_KOMBINACE __declspec(dllexport)
#else
#define EXPORT_KONTROLA_PREKLEPU_KOMBINACE
#endif

#include "../../shared/plugin.h"
#include "../nejlepsi_opravy.h"

class Kontrola_preklepu_kombinace : public Plugin
{
public:
    Kontrola_preklepu_kombinace();
    virtual ~Kontrola_preklepu_kombinace(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    int NactiNastaveni();
    int NactiWordlist(const string_t& wordlist_file);

    bool Opravovat_;
    size_t MaxPocetOprav_;
    size_t Kapacita_;
    StringVector Wordlist_;
};

#endif // KONTROLA_PREKLEPU_KOMBINACE_H
