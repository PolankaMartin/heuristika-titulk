#ifndef PREKRYVAJICI_TITULKY_H
#define PREKRYVAJICI_TITULKY_H

#ifdef _WIN32
#define EXPORT_PREKRYVAJICI_TITULKY __declspec(dllexport)
#else
#define EXPORT_PREKRYVAJICI_TITULKY
#endif

#include "../../shared/plugin.h"
#include "../doba_zobrazeni.h"

class Prekryvajici_titulky : public Plugin
{
public:
    Prekryvajici_titulky();
    virtual ~Prekryvajici_titulky(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    long PredchoziKonec_;

    ZaznamVector Zaznamy_;
    size_t DefaultZobrazeni_;
};

#endif // PREKRYVAJICI_TITULKY_H
