#include "prekryvajici_titulky.h"


Prekryvajici_titulky::Prekryvajici_titulky()
    : Plugin("Prekryvajici se titulky","prekryvajici_titulky"), PredchoziKonec_(0)
{
    Doba_zobrazeni::NactiDobyZobrazeni(*this,Zaznamy_,DefaultZobrazeni_);
}

void Prekryvajici_titulky::Reset()
{
    PredchoziKonec_ = 0;
}

int Prekryvajici_titulky::Run(Titulek &titulek, Chyby &chyby)
{
    long konec = titulek.VratKonec();
    size_t uid = titulek.VratUID();

    long predch_konec = PredchoziKonec_;
    PredchoziKonec_ = konec;

    if(predch_konec > titulek.VratZacatek())
    {
        Chyba pridej(NazevPluginu(),NazevChyby(),uid,0);

        size_t doba_zobrazeni = Doba_zobrazeni::Run(DefaultZobrazeni_,Zaznamy_,titulek);
        pridej.PridejOpravu(Titulek::CasNaString(predch_konec,doba_zobrazeni));
        chyby.PushBack(pridej);

        Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));

        return 1;
    }

    return 0;
}

extern "C" Plugin EXPORT_PREKRYVAJICI_TITULKY * vytvor()
{
    return new Prekryvajici_titulky();
}

extern "C" void EXPORT_PREKRYVAJICI_TITULKY znic(Plugin* p)
{
    delete p;
}
