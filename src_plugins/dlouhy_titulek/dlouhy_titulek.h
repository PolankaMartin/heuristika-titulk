#ifndef DLOUHY_TITULEK_H
#define DLOUHY_TITULEK_H

#ifdef _WIN32
#define EXPORT_DLOUHY_TITULEK __declspec(dllexport)
#else
#define EXPORT_DLOUHY_TITULEK
#endif

#include "../../shared/plugin.h"

class Dlouhy_titulek : public Plugin
{
public:
    Dlouhy_titulek();
    virtual ~Dlouhy_titulek(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    int NactiNastaveni();

    size_t MaxZnakuNaRadku_;
    size_t MaxRadku_;
    size_t MaxZnaku_;
    Slovnik<string_t> Spojky_;
};

#endif // DLOUHY_TITULEK_H
