#include "dlouhy_titulek.h"

Dlouhy_titulek::Dlouhy_titulek()
    : Plugin("Prilis dlouhy titulek","dlouhy_titulek")
{
    NactiNastaveni();
}

void Dlouhy_titulek::Reset()
{
    return;
}

int Dlouhy_titulek::Run(Titulek &titulek, Chyby &chyby)
{
    bool vysl = true;
    size_t radku = titulek.PocetRadku();
    size_t cely_titulek = titulek.PocetUTF8Znaku();

    if(radku > MaxRadku_)
    {
        Chyba pridej(NazevPluginu(),"Prilis mnoho radku v titulku",titulek.VratUID(),0);
        chyby.PushBack(pridej);
        vysl = false;
        Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
    }
    if(cely_titulek > MaxZnaku_)
    {
        Chyba pridej(NazevPluginu(), "Pocet znaku v celem titulku je prilis dlouhy", titulek.VratUID(), 0);
        chyby.PushBack(pridej);
        vysl = false;
        Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
    }

    for(size_t i = 0; i < radku; ++i)
    {
        const string_t& radek = titulek.VratRadek(i).VratZmensenyText();
        size_t pozice = titulek.VratRadek(i).VratPoziciZmenseny();

        size_t delka = utf8::distance(radek.begin(), radek.end());

        if(delka > MaxZnakuNaRadku_)
        {
            Chyba pridej(NazevPluginu(), "Prilis dlouha radka", titulek.VratUID(), pozice);

            if(radku < MaxRadku_ && cely_titulek < MaxZnaku_) // pokud mozno tak najdeme idealni rozdelovaci bod a navrhneme opravu
            {
                std::vector<size_t> split_points;
                StringVector splitted; // neni nejlepsi zpusob... chtelo by to vylepsit
                boost::split(splitted, radek, boost::is_any_of(" "));
                size_t pozice = 0;

                for(auto& slovo : splitted)
                {
                    pozice += slovo.length() + 1;
                    if(slovo.length() == 0){ continue; }

                    if(Text::JeKonecBloku(slovo)){ split_points.push_back(pozice); } // nasli jsme ukonceni bloku, tudiz po nem muzeme zalomit
                    else
                    {
                        slovo = Text::OdstranZacatekBloku(slovo);
                        slovo = Text::OdstranTriTecky(slovo);
                        slovo = Text::OdstranKonecBloku(slovo);

                        if(Spojky_.Hledej(slovo)) // nasli jsme spoujku a muzeme ji zaradit jako split point
                        {
                            split_points.push_back(pozice - slovo.length() - 1);
                        }
                    }
                }

                // vybrat nejlepsi split point
                size_t nejlepsi = 0;
                size_t prostredek = radek.length() / 2;
                // opravu navrhujeme pouze v pripade ze bude mozne radku rozdelit na dve
                //   bez poruseni maximalniho poctu znaku na radek
                if(prostredek < MaxZnakuNaRadku_)
                {
                    for(auto& split_point : split_points)
                    {
                        if(std::abs((long)prostredek - (long)split_point) < std::abs((long)prostredek - (long)nejlepsi))
                        {
                            nejlepsi = split_point;
                        }
                    }

                    // samotny navrh opravy
                    if(nejlepsi > 0 && nejlepsi < (radek.length() - 1))
                    {
                        size_t delka_prvni = 0;
                        size_t delka_druhy = 0;

                        string_t vysledek = titulek.VratRadek(i).VratText().substr(0, nejlepsi - 1) + "\n";
                        delka_prvni = vysledek.length() - 1;

                        vysledek += titulek.VratRadek(i).VratText().substr(nejlepsi);
                        delka_druhy = vysledek.length() - delka_prvni;

                        if(delka_prvni <= MaxZnakuNaRadku_ && delka_druhy <= MaxZnakuNaRadku_)
                        {
                            pridej.PridejOpravu(vysledek);
                        }
                    }
                }
            }

            chyby.PushBack(pridej);
            vysl = false;
            Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
        }
    }
    if(vysl == false){return 1;}
    else{return 0;}
}

int Dlouhy_titulek::NactiNastaveni()
{
    bool vysledek = true;

    boost::property_tree::ptree strom = NactiStromNastaveni();
    if(strom.empty() == true){ vysledek = false; }

    MaxZnakuNaRadku_ = strom.get("max_znaku_na_radku",0);
    MaxRadku_ = strom.get("max_radku",0);
    MaxZnaku_ = strom.get("max_znaku_titulek",0);

    if(MaxZnakuNaRadku_ == 0 || MaxRadku_ == 0 || MaxZnaku_ == 0){ vysledek = false; }
    else
    {
        try
        {
            boost::property_tree::ptree seznam = strom.get_child("seznam_spojek");
            for(auto& i : seznam)
            {
                Spojky_.PushBack(Text::NaMala(i.second.get_value<string_t>()));
            }
        }
        catch(...)
        {
            vysledek = false;
        }
    }

    if(vysledek == false)
    {
        ErrorNastaveni();
        return 1;
    }

    return 0;
}

extern "C" Plugin EXPORT_DLOUHY_TITULEK * vytvor()
{
    return new Dlouhy_titulek();
}

extern "C" void EXPORT_DLOUHY_TITULEK znic(Plugin* p)
{
    delete p;
}
