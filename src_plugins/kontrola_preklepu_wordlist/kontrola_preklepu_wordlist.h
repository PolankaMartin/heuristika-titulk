#ifndef KONTROLA_PREKLEPU_WORDLIST_H
#define KONTROLA_PREKLEPU_WORDLIST_H

#ifdef _WIN32
#define EXPORT_KONTROLA_PREKLEPU_WORDLIST __declspec(dllexport)
#else
#define EXPORT_KONTROLA_PREKLEPU_WORDLIST
#endif

#include "../../shared/plugin.h"
#include "../nejlepsi_opravy.h"

class Kontrola_preklepu_wordlist : public Plugin
{
public:
    Kontrola_preklepu_wordlist();
    virtual ~Kontrola_preklepu_wordlist(){}

    virtual int Run(Titulek& titulek, Chyby& chyby);
    virtual void Reset();
private:
    int NactiNastaveni();
    int NactiWordlist(const string_t& wordlist_file);

    bool Opravovat_;
    size_t MaxPocetOprav_;
    size_t MaxVzdalenost_;
    size_t Kapacita_;
    StringVector Wordlist_;
};

#endif // KONTROLA_PREKLEPU_WORDLIST_H
