#include "kontrola_preklepu_wordlist.h"


Kontrola_preklepu_wordlist::Kontrola_preklepu_wordlist()
    : Plugin("Detekovan preklep","kontrola_preklepu_wordlist"), Opravovat_(false)
{
    NactiNastaveni();
}

void Kontrola_preklepu_wordlist::Reset()
{
    return;
}

int Kontrola_preklepu_wordlist::Run(Titulek &titulek, Chyby &chyby)
{
    int vysledek = 0;
    size_t pocet_slov = titulek.PocetSlov();

    for(size_t i = 0; i < pocet_slov; ++i)
    {
        bool nalezeno = false;
        string_t slovo = titulek.VratSlovo(i).VratZmensenyText();
        size_t pozice = titulek.VratSlovo(i).VratPoziciZmenseny();
        NejlepsiOpravyKontejner kont(slovo, MaxPocetOprav_, MaxVzdalenost_);

        // odstranime ze slova vsechny nepotrebne veci
        if(slovo.length() == 0){ continue; }
        if(slovo.find_first_of("0123456789") != string_t::npos){ continue; }
		slovo = Text::OdstranTriTecky(slovo);
		slovo = Text::OdstranKonecBloku(slovo);
        slovo = Text::OdstranKonecVety(slovo);
        slovo = Text::OdstranZacatekBloku(slovo);
        if(slovo.length() == 0){ continue; }

        // nejdrive zjistime jestli nahodou neni slovo ve slovniku pomoci bin. vyhledavani
        if(std::binary_search(Wordlist_.begin(), Wordlist_.end(), slovo)){ continue; }

        if(Opravovat_ == false)
        {
            Chyba pridej(NazevPluginu(), NazevChyby() + " ve slove \"" + slovo + "\"", titulek.VratUID(), pozice);
            chyby.PushBack(pridej);

            Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
            vysledek = 1;
            continue;
        }

        // slovo neni ve slovniku a proto potrebujeme najit k nemu nejblizsi slova
        for(auto& wordlist_entry : Wordlist_)
        {
            size_t lev = Text::LevenshteinDistance(wordlist_entry, slovo, MaxVzdalenost_);
            if(lev == 0){ nalezeno = true; break; }
            else
            {
                kont.PridejOpravu(lev, wordlist_entry);
            }
        }

        // slovo ve slovniku nebylo nalezeno a vsechny polozky ve wordlistu byly projity,
        // ted uz se jen vypisi ta nejblizsi slova
        if(nalezeno == false && kont.VratNejlepsiOpravy().size() != 0)
        {
            Chyba pridej(NazevPluginu(), NazevChyby() + " ve slove \"" + slovo + "\"", titulek.VratUID(), pozice);
            for(auto& i : kont.VratNejlepsiOpravy())
            {
                pridej.PridejOpravu(i.second);
            }
            chyby.PushBack(pridej);

            Log_.Zapis(NazevPluginu(),"Zaznamenana chyba v titulku: " + std::to_string(titulek.VratUID()));
            vysledek = 1;
        }
    }

    return vysledek;
}

int Kontrola_preklepu_wordlist::NactiNastaveni()
{
    bool vysledek = true;

    boost::property_tree::ptree strom = NactiStromNastaveni();
    if(strom.empty() == true){ vysledek = false; }

    Opravovat_ = strom.get("navrh_oprav.zapnout", false);
    MaxPocetOprav_ = strom.get("navrh_oprav.max_pocet_oprav", 0);
    MaxVzdalenost_ = strom.get("navrh_oprav.max_vzdalenost", 0);
    Kapacita_ = strom.get("kapacita", 0);
    string_t wordlist_file = strom.get("wordlist_file", "");

    if(MaxPocetOprav_ == 0){ vysledek = false; }

    if(vysledek == false)
    {
        ErrorNastaveni();
        return 1;
    }
    else{ NactiWordlist(wordlist_file); }

    return 0;
}

int Kontrola_preklepu_wordlist::NactiWordlist(const string_t &wordlist_file)
{
    /*
     * predpoklada se, ze slova v souboru budou uz jen s malymi pismeny, serazena
     *  a ze soubor bude ve spravnem formatu, tedy jedno slovo na radek
     */
    std::ifstream soubor(wordlist_file);

    Wordlist_.reserve(Kapacita_);

    string_t radka;
    while(getline(soubor, radka))
    {
        Wordlist_.push_back(radka);
    }

    return 0;
}

extern "C" Plugin EXPORT_KONTROLA_PREKLEPU_WORDLIST * vytvor()
{
    return new Kontrola_preklepu_wordlist();
}

extern "C" void EXPORT_KONTROLA_PREKLEPU_WORDLIST znic(Plugin* p)
{
    delete p;
}
