#include<iostream>
#include<string>

/* g++ levenshtein.cpp -std=c++11 ../shared/text.cpp -lboost_system -lboost_locale */

#include "../shared/text.h"

int main()
{
    std::string s = "kitteš";
    std::string t = "kitter";

    std::cout << Text::LevenshteinDistance(s,t) << std::endl;

    return 0;
}
