#include<iostream>
#include<string>

/* g++ text_check.cpp -std=c++11 ../shared/text.cpp -lboost_system -lboost_locale */

#include "../shared/text.h"

int main()
{
    std::string r = "ěščřžýáíéúů";
    std::string s = "kitteš";
    std::string t = "kitter";

    std::cout << Text::NaVelka(r, 1) << std::endl;
    std::cout << Text::NaVelka(s, 2, utf8::distance(s.begin(), s.end()) - 3) << std::endl;
    //std::cout << Text::NaVelka(t, 0, utf8::distance(t.begin(), t.end() + 1)) << std::endl;
	std::cout << Text::NaVelka(r, 0, 1) << std::endl;

    r = "ĚšČŘŽÝÁÍÉÚŮ";
    s = "KITTEŠ";
    t = "KITTER";

    //std::cout << Text::NaMala(r, 1) << std::endl;
    //std::cout << Text::NaMala(s, 2, utf8::distance(s.begin(), s.end()) - 3) << std::endl;
    //std::cout << Text::NaMala(t, 0, utf8::distance(t.begin(), t.end() + 1)) << std::endl;

    std::cout << std::endl;

    //std::cout << Text::JeVelke(r, 0) << std::endl;
    //std::cout << Text::JeVelke(r, 1) << std::endl;

    std::cout << std::endl;

    //std::cout << Text::JeMale(r, 0) << std::endl;
    //std::cout << Text::JeMale(r, 1) << std::endl;

    std::cout << std::endl;

    r = "";
    s = "1";
    //std::cout << Text::JeMale(r, 0) << std::endl;
    //std::cout << Text::JeMale(s, 1) << std::endl;
    //std::cout << Text::JeMale(s, 0) << std::endl;
    //std::cout << Text::JeVelke(s, 0) << std::endl;
    //std::cout << Text::NaMala(r, 0) << std::endl;
    //std::cout << Text::NaMala(s, 1) << std::endl;

    std::cout << std::endl;
    std::cout << "KMP FIND FUNKCE NA UTF8:" << std::endl;

    std::string jehla = "šepotat";
    std::string text = "šepotat v šepotátku a šepotujeme... šepotat.";

    std::vector<std::string::const_iterator> res = Text::UTF8HledejVsechny(text, jehla);
    for(auto& i : res)
    {
        std::cout << "Nalez na: " << (i - text.begin()) << std::endl;
    }

    return 0;
}
