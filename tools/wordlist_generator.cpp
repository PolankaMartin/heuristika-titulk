#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <set>
#include "boost/algorithm/string.hpp"
#include "../utf8_v2_3_4/source/utf8.h"
#include "../shared/text.h"

/* g++ wordlist_generator.cpp ../shared/text.cpp -std=c++11 -lboost_system */

using namespace std;

void OrezKonecRadky(string& radka)
{
    while(radka[radka.length() - 1] == '\r' || radka[radka.length() - 1] == '\n')
    {
        radka = radka.substr(0,radka.length() - 1);
    }
    return;
}

int main()
{
    ifstream soubor("subtitles_cs.csv");
    size_t cislo_radky = 0;

    set<string> Slova;
    set<string> Vynechano;

    string radka;
    getline(soubor, radka);
    cislo_radky++;

    while(true)
    {
        size_t uid = 0;

        try
        { // na radce kde melo byt cislo titulku nebylo cislo
            uid = std::stoi(radka);
        }
        catch(...)
        {
            std::cerr << "Spatny format, ID ocekavano. Radka: " + std::to_string(cislo_radky) << std::endl;
            exit(1);
        }

        if(!getline(soubor, radka))
        {
            std::cerr << "Soubor v nespravnem formatu! Chyba na radce: " + std::to_string(cislo_radky) << std::endl;
        }
        cislo_radky++;

        size_t delim = radka.find("-->");
        if(delim != string::npos)
        { // ok
        }
        else
        { // radek kde mel byt zacatek a konec titulku ve formatu XX:XX:XX,XXXX --> XX:XX:XX,XXXX nebylo nalezeno "-->"
            std::cerr << "****** Na radce: " + std::to_string(cislo_radky)
                         + " byl ocekavan cas zacatku a konce titulku ve spravnem formatu! ******" << std::endl;
        }

        while(getline(soubor, radka))
        {
            vector<string> temp_slova;
            cislo_radky++;
            OrezKonecRadky(radka);
            if(radka == "") break;


            if(cislo_radky % 1000 == 0){ cout << "." << flush; }


            boost::split(temp_slova, radka, boost::is_any_of(" \t"));

            // zaradit slova do vectoru slov, tak aby bylo unikatni
            for(auto& i : temp_slova)
            {
                string temp = i;

                if(temp == ""){ continue; }
                if(temp.length() == 1){ Vynechano.insert(i); continue; }


                // hledame stringy jako html tagy apod., ktere ve slovniku nechceme
                vector<string> symboly = { "<br>", "</br>", "<b>", "<i>", "</i>", "<u>", "</u>",
                                         "&gt;", "&nbsp;", "&phi;", "��", "„", "“",
                                         "…", "‘", "’", "–", "√", "→", "&quot;", "♪", "", "•", "‚",
                                         "≤", "≥", "´", "`" };
                for(auto& symbol : symboly)
                {
                    int found = -1;
                    if(temp.length() == 1){ break; }
                    while((found = temp.find(symbol, found + 1)) != string::npos)
                    {
                        string temp_1 = temp.substr(0, found);
                        if((found + symbol.length()) < temp.length()){ temp_1 += temp.substr(found + symbol.length()); }
                        temp = temp_1;
                        if(temp.length() == 0 || temp.length() == 1){ break; }
                    }
                    if(temp.length() == 0 || temp.length() == 1){ break; }
                }

                if(temp.length() == 1 || temp.length() == 0){ Vynechano.insert(i); continue; }


                // hledame znaky jako tecky carky zavorky apod. na zacatku a na konci slova
                while(temp.at(0) == '.' || temp.at(0) == ',' || temp.at(0) == '?' || temp.at(0) == '!' ||
                      temp.at(0) == '-' || temp.at(0) == '"' || temp.at(0) == '\'' || temp.at(0) == '(' ||
                      temp.at(0) == ')' || temp.at(0) == '{' || temp.at(0) == '}' || temp.at(0) == '[' ||
                      temp.at(0) == ']' || temp.at(0) == '*' || temp.at(0) == '+' || temp.at(0) == '/' ||
                      temp.at(0) == ':' || temp.at(0) == '=' || temp.at(0) == '<' || temp.at(0) == '>' ||
                      temp.at(0) == '_' || temp.at(0) == ';')
                {
                    temp = temp.substr(1);
                    if(temp.length() == 1){ break; }
                }

                if(temp.length() == 1){ Vynechano.insert(i); continue; }
                while(temp.at(temp.length() - 1) == '.' || temp.at(temp.length() - 1) == ',' ||
                      temp.at(temp.length() - 1) == '?' || temp.at(temp.length() - 1) == '!' ||
                      temp.at(temp.length() - 1) == '-' || temp.at(temp.length() - 1) == '"' ||
                      temp.at(temp.length() - 1) == '\'' || temp.at(temp.length() - 1) == '(' ||
                      temp.at(temp.length() - 1) == ')' || temp.at(temp.length() - 1) == '{' ||
                      temp.at(temp.length() - 1) == '}' || temp.at(temp.length() - 1) == '[' ||
                      temp.at(temp.length() - 1) == ']' || temp.at(temp.length() - 1) == '*' ||
                      temp.at(temp.length() - 1) == '+' || temp.at(temp.length() - 1) == '/' ||
                      temp.at(temp.length() - 1) == ':' || temp.at(temp.length() - 1) == '=' ||
                      temp.at(temp.length() - 1) == '<' || temp.at(temp.length() - 1) == '>' ||
                      temp.at(temp.length() - 1) == '_' || temp.at(temp.length() - 1) == ';')
                {
                    temp = temp.substr(0, temp.length() - 1);
                    if(temp.length() == 1){ break; }
                }


                if(temp.find_first_of("0123456789") != string::npos){ Vynechano.insert(i); continue; }
                if(temp.length() == 1 || temp.length() == 0){ Vynechano.insert(i); continue; }

                if(utf8::is_valid(temp.begin(), temp.end())){ Slova.insert(Text::NaMala(temp)); }
                else{ Vynechano.insert(i); }
            }
        }

        radka = ""; // pro pripad konce souboru, aby se nepreskocil nasledujici while cyklus

        bool skonci = false;
        while(radka == "")
        {
            if(getline(soubor,radka))
            {
                cislo_radky++;
                OrezKonecRadky(radka);
            }
            else{skonci = true; break;}
        }
        if(skonci == false){ OrezKonecRadky(radka); continue; }
        else{break;}
    }


    // zapsat slova a vynechana slova do souboru
    ofstream slova_soubor("slovnik_slova.txt");
    ofstream vynechana_soubor("slovnik_vynechana.txt");

    for(auto& i : Slova)
    {
        slova_soubor << i << std::endl;
    }

    for(auto& i : Vynechano)
    {
        vynechana_soubor << i << std::endl;
    }

    return 0;
}
