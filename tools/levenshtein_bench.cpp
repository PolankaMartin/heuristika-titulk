#include<iostream>
#include<string>
#include <fstream>

/* g++ -O2 levenshtein_bench.cpp -std=c++11 ../shared/text.cpp -lboost_system -lboost_locale */

#include "../shared/text.h"
#include "../shared/timer.h"

// wikibooks implementation
unsigned int levenshtein_distance(const std::string& s1, const std::string& s2) 
{
	const std::size_t len1 = s1.size(), len2 = s2.size();
	std::vector<unsigned int> col(len2+1), prevCol(len2+1);
 
	for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;
		for (unsigned int j = 0; j < len2; j++)
                        // note that std::min({arg1, arg2, arg3}) works only in C++11,
                        // for C++98 use std::min(std::min(arg1, arg2), arg3)
			col[j+1] = std::min({ prevCol[1 + j] + 1, col[j] + 1, prevCol[j] + (s1[i]==s2[j] ? 0 : 1) });
		col.swap(prevCol);
	}
	return prevCol[len2];
}

int main()
{
	std::vector<string_t> wordlist;
    std::ifstream input("./wordlist_aspell.txt");
    size_t words_count = 0;

	string_t line;
	while(getline(input, line))
	{
		wordlist.push_back(line);
        words_count++;
	}

	Timer<> mine;
	mine.Start();
    for(auto& i : wordlist)
	{
		Text::LevenshteinDistance("jjfkjsdklajfldasjljlruweioqp", i);
	}
    std::cout << "My implementation of Levenshtein on " << words_count << " words: " << (mine.Time() / 1000) << "ms" << std::endl;

	Timer<> refer;
	refer.Start();
    for(auto& i : wordlist)
	{
		levenshtein_distance("jjfkjsdklajfldasjljlruweioqp", i);
	}
    std::cout << "Reference implementation of Levenshtein on " << words_count << " words: " << (refer.Time() / 1000) << "ms" << std::endl;

    return 0;
}
