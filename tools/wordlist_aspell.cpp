#include<iostream>
#include<string>
#include<fstream>
#include<set>

/* g++ wordlist_aspell.cpp -std=c++11 ../shared/text.cpp -lboost_system -lboost_locale */

#include "../shared/text.h"

using namespace std;

int main()
{
    std::locale loc = boost::locale::generator().generate("cs_CZ.utf8");
    set<string> Slova;
    ifstream input("./wordlist_aspell.txt");
    ofstream output("./wordlist_aspell_transf.txt");

    string line;

    size_t i = 0;
    while(getline(input, line))
    {
        if(i % 100000 == 0){ cout << i << endl; }
        Slova.insert(Text::NaMala(line));
        i++;
    }

    for(auto& i : Slova)
    {
        output << i << endl;
    }

    return 0;
}
