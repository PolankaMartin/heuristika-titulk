#include<iostream>
#include<string>
#include<fstream>
#include<set>

/* g++ char_three_grams_generator.cpp -std=c++11 ../shared/text.cpp -lboost_system -lboost_locale */

#include "../shared/text.h"

using namespace std;

int main()
{
    set<string> Trigramy;
    ifstream input("./wordlist_aspell.txt");
    ofstream output("./char_three_grams.txt");

    string line;
    size_t i = 0;
    while(getline(input, line))
    {
        if(i % 100000 == 0){ std::cout << i << std::endl; }

        for(auto it = line.begin(); it != line.end();)
        {
            size_t all_three = 1;
            size_t char1 = utf8::next(it, line.end());
            size_t char2;
            size_t char3;

            auto it_ = it;
            if(it != line.end())
            {
                char2 = utf8::next(it_, line.end());
                all_three++;

                if(it_ != line.end())
                {
                    char3 = utf8::next(it_, line.end());
                    all_three++;
                }
            }

            if(all_three == 3)
            {
                string_t result;
                utf8::append(char1, std::back_inserter(result));
                utf8::append(char2, std::back_inserter(result));
                utf8::append(char3, std::back_inserter(result));

                Trigramy.insert(Text::NaMala(result));
            }
        }
        i++;
    }

    for(auto& slovo : Trigramy)
    {
        output << slovo << endl;
    }

    return 0;
}
