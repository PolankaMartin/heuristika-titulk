#include "engine.h"

int main(int argc, char * * argv)
{
    StringVector parametry(argv, argv + argc);

    Engine e(parametry);
    e.Run();
    return 0;
}
