\chapter{Kontext práce}
\label{ch:kontextprace}

Podobné programy \textbf{heuristice titulků} jsou pravděpodobně jenom různé nástroje na editaci a tvorbu titulků. Z~hlediska implementovaných funkčností je zajímavým tématem kontrola překlepů, která je dnes velmi hojně využívána v~různých editorech textu a dalších programech pracujících s~textem.

\section{Nástroje na titulky}

V~zásadě podobný program \textbf{heuristice titulků} pravděpodobně v~českém prostředí vůbec neexistuje. Nejblíže k~tomu mají programy na editaci a tvorbu titulků, které jsou často opatřeny kontrolou překlepů a nebo chyb spojených s~titulky, jako například překrývající se titulky a podobně.

\begin{description}[style=nextline]
\item[SubtitleToolCZ \cite{subtitletoolcz}] Český software vytvořený už v~roce 2002 a v~tomto roce také naposledy aktualizovaný, přesto je i na nových systémech stále funkční. Má jednoduché GUI a zvládá veškerou základní práci s~titulky. Nevýhodou je podpora malého množství titulkových formátů a kódování (\textbf{UTF8} není podporováno). Program nabízí také jednoduchou kontrolu titulků, v~rámci které detekuje překrývající titulky, prázdný titulek a zobrazuje i malou statistiku daného titulkového souboru. V~té je možné se dozvědět jaký je nejširší titulek, který má nejvíce řádků, nejmenší vzdálenost mezi titulky a nejkratší doba zobrazení titulku.
\item[Aegisub \cite{aegisub}] Pokročilý nástroj na vytváření a editaci titulkových souborů, podporuje mnoho různých formátů a konverze mezi nimi. Velkou předností je zvládnutí moderního formátu \textbf{Advanced SubStation Alpha} \cite{ass}, který umožňuje libovolné umisťování titulku a i mnohé grafické podoby. Program zvládá jak základní práci s~titulky, tak pokročilé věci, jako umisťování titulku, editace na spuštěném videu, nebo umisťování podle zvukové křivky. Pěknou vlastností je také multiplatformnost, díky které je zajištěn běh aplikace na platformách Windows, Linux, FreeBSD, Mac OS X.

Z~našeho pohledu je ale zajímavější kontrola překlepů, respektive pravopisu, kterou \textbf{Aegisub} zvládá. A~nebo zde lze nalézt také možnost odstranění překrývajících se titulků.
\item[Subtitle Workshop \cite{subtitleworkshop}] Jeden z~nejoblíbenějších a nejpoužívanějších nástrojů pro tvorbu a editaci titulků. A~také program, který se analýzou stavu titulků alespoň částečně blíží k~\textbf{heuristice titulků}. Program nabízí podporu pro většinu titulkových formátů a opět nabízí veškerou základní práci s~titulky a mnoho pokročilých funkcí (lze např. provádět změny v~titulcích při přehrávání videa). Nevýhodou je opět nepodpora \textbf{UTF8} i v~nových verzích.

Aplikace obsahuje kontrolu překlepů (pomocí pluginu MS Wordu) a také spoustu možných informací o~titulku, kam patří například statistika o~titulku (nejdelší zobrazení, nejkratší zobrazení, nejdelší titulek apod.) a kontrola chyb v~titulku. \textbf{Subtitle Workshop} je schopen zkontrolovat to, jestli je titulek prázdný, jestli se sousední titulky nepřekrývají, krátké pauzy mezi titulky, dlouhé zobrazení titulku, nebo věci jako velký počet stejných znaků za sebou, OCR chyby a nepotřebné mezery.
\end{description}

\section{Kontrola překlepů}
Ve výsledné aplikaci by měla být naprogramována i součást, která kontroluje překlepy ve slovech. To je obecně celkem oblíbené a rozšířené téma u~lingvistů a programátorů aplikací nějakým způsobem pracující s~textem, které si díky tomu zaslouží vlastní zmínku. V~rámci spell-checkingu (kontroly překlepů) vzniká mnoho nástrojů a softwaru, který ať už lépe, či hůře dokáže detekovat překlepy a opravovat chyby.

\begin{description}[style=nextline]
\item[Lingea \cite{lingea}] \textbf{Lingea} je česká společnost s~bohatou tradicí vývoje jazykových aplikací, se kterým začala už v~roce 1997. Jejím pravděpodobně nejznámějším produktem je elektronický slovník pro jazyky angličtina, ruština, francouzština atd. Mezi její oblasti zájmu se ale obecně dá zařadit vše okolo jazyka a knih.

Méně známým faktem je to, že \textbf{Lingea} poskytuje společnostem jako \textbf{Microsoft}, \textbf{Adobe} apod. korektor pravopisu. Tudíž mnoho lidí bez svého vědomí používá kontrolu překlepů v~\textbf{Microsoft Office} \cite{word2010spellcheck}, či lemmatizátor (při fulltextovém vyhledávání) na různých webových stránkách, který navrhla a napsala právě společnost \textbf{Lingea}.

\item[Korektor \cite{korektor}] \textbf{Korektor} je jeden z~projektů, který vzniká na fakultě \textbf{ÚFAL} \cite{ufal} a vychází z~diplomové práce Michala Richtera (\textbf{Advanced Czech Spellchecker} \cite{advancedczechspellchecker}) z~roku 2012. Používá statistické metody pro kontrolu překlepů a dokáže detekovat i některé pravopisné chyby. K~dispozici je i online demo \cite{korektordemo}, kde je možné si program nezávazně vyzkoušet.
\item[Norvig's Spell Corrector \cite{norvigspellcorrect}] Peter Norvig, ředitel výzkumu u~společnosti \textbf{Google} \cite{norvigbio}, uvedl na svém webu jednoduchou a rychlou implementaci kontroly překlepů. Ta vychází z~naprosto základního způsobu kontroly překlepů wordlistem a hledáním opravy pomocí nejmenší \textbf{Levenshteinovy vzdálenosti}. Jeho algoritmus ale v~případě hledání chyby postupuje jiným způsobem než klasickým počítáním vzdáleností od všech slov.

Dramatického vylepšení se v~tomto případě dosáhlo prostým vygenerováním všech možných tvarů slova, které mají maximální vzdálenost 2 od slova hledaného a jejich srovnávání oproti slovníku. Pokud bylo vygenerované slovo ve slovníku nalezeno, je vráceno jako možná oprava. Z~všech možných oprav se pak vybere jedna nejpravděpodobnější, což je dosaženo pomocí dodatečných informací ve slovníku, kde se ukládá i něco na způsob počtu výskytů v~trénovacích datech.

Tímto postupem vzniká jednoduchý nástroj pro kontrolu překlepů, který je dostatečně rychlý při návrhu opravy a stručný při psaní kódu. Jedinou nevýhodou zmíněnou Norvigem je alespoň dle jeho názoru nedostatečná přesnost v~navrhovaných opravách.
\end{description}