#ifndef KONFIGURACE_H
#define KONFIGURACE_H

#include "titulek.h"

class Engine;

/**
 * Trida udrzujici jednotlive konfigurace (pripadne nazvy souboru/slozky ve kterych lze najit konfiguracni soubory) nactene ridicim programem.
 * Je dostupna pomoci singletonu, tedy tridy KonfSystem a metody VratKonfiguraci().
 */
class Konfigurace
{
public:
    /** Defaultni konstruktor je povolen a musi nastavit vychozi slozku. */
    Konfigurace();
    /** Copy konstruktor zakazan. */
    Konfigurace(const Konfigurace& source) = delete;
    /** Copy metoda zakazana. */
    Konfigurace& operator=(const Konfigurace& source) = delete;
    /** Move konstruktor zakazan. */
    Konfigurace(Konfigurace&& source);
    /** Move metoda zakazana. */
    Konfigurace& operator=(Konfigurace&& source) = delete;

    /**
     * Vraci nazev vychozi slozky, ve ktere jsou umisteny konfiguracni soubory k jednotlivym pluginum.
     */
    const string_t& VratDefaultSlozku();
    /**
     * Vraci nazev alternativniho konfiguracniho souboru, ten muze byt pouze jeden.
     */
    const string_t& VratAltSoubor();
    /**
     * Vrati alternativni serializovanou konfiguraci.
     */
    const string_t& VratAltKonfiguraci();
private:
    friend class Engine;

    /*
      Priorita pouziti konfiguraci (mensi cislo == vetsi priorita)
        1 -> Serializovana konfigurace (parametr --config=)
        2 -> Alternativni konfiguracni soubor (param. --config-file=)
        3 -> Defaultni konfiguracni soubor (urcen v dobe prekladu)
    */
    string_t DefaultNastaveni_; // cesta ke slozce s defaultnimi *.conf soubory
    string_t AltKonfSoubor_; // Alternativni konfiguracni soubor (format XML) = mozna by se mohl vracet rovnou ptree, aby se porad neoteviral jeden soubor
    string_t AltKonfigurace_; // Alternativni serializovana konfigurace (format XML)
};

#endif // KONFIGURACE_H
