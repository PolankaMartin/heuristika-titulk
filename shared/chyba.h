#ifndef CHYBA_H
#define CHYBA_H

#include "titulek.h"

/**
 * Objekt, ktery obsahuje jednotlive informace o chybe.
 * Veskera data krome moznosti oprav se mu musi predat uz pri konstrukci.
 */
class Chyba
{
public:
    /** Defaultni konstruktor je zakazany. */
    Chyba() = delete;
    Chyba(const Chyba& source) = default;
    Chyba& operator=(const Chyba& source) = default;
#ifdef _WIN32
	/* Defaultni move metody neumi Visual Studio 2013 aktualne prelozit a bohuzel ani generovat... */
#else
	Chyba(Chyba&& source) = default;
	Chyba& operator=(Chyba&& source) = default;
#endif

    /**
     * Nahrazuje defaultni konstruktor, je treba predat objektu Chyba 4 zakladni parametry, jinak ho nelze vytvorit.
     * @param nazev_pluginu urcuje nazev pluginu ve kterem byla objevena chyba
     * @param nazev_chyby presny popis nalezene chyby
     * @param titulek cislo titulku ve kterem je chyba
     * @param pozice rika na kolikatem znaku v titulku byla chyba nalezena
     */
    Chyba(string_t nazev_pluginu, string_t nazev_chyby, size_t titulek, size_t pozice);

    /**
     * Funkce, ktera prida do promenne Opravy_ zaznam.
     * @param oprava stringova hodnota, ktera bude zapsana do moznych oprav chyby
     */
    void PridejOpravu(const string_t& oprava);
    /**
     * Vraci presny popis chyby.
     */
    string_t VratNazevChyby();
    /**
     * Vraci nazev pluginu ve kterem byla chyba objevena
     */
    string_t VratNazevPluginu();
    /**
     * Vrati identifikacni cislo titulku
     * @return bezznamenkove cislo
     */
    size_t VratCisloTitulku();
    /**
     * Zpristupnuje pozici, na ktere byla nalezena chyba
     * @return bezznamenkove cislo, ukazujici na odpovidajici bajt, ne znak
     */
    size_t VratPozici();
    /**
     * Vrati vector moznych oprav chyby
     * @return konstantni reference na std::vector<std::string>
     */
    const StringVector& VratOpravy();
private:
    string_t NazevChyby_;
    string_t NazevPluginu_;
    size_t Titulek_;
    size_t Pozice_;
    StringVector Opravy_;
};

#endif // CHYBA_H
