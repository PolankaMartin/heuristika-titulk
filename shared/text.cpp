#include "text.h"

size_t Text::LevenshteinDistance(const string_t &word_1, const string_t &word_2, size_t max_distance)
{
	if (!utf8::is_valid(word_1.begin(), word_1.end()) || !utf8::is_valid(word_2.begin(), word_2.end()))
	{
		return string_t::npos;
	}

	size_t dist = 0;
	size_t length_1 = word_1.length();
	size_t length_2 = word_2.length();

	if (length_1 == 0){ return length_2; }
	if (length_2 == 0){ return length_1; }

	size_t **lev = new size_t*[2];
	lev[0] = new size_t[length_2 + 1];
	lev[1] = new size_t[length_2 + 1];
	size_t *temp;

    size_t first_min = 0; // minimalni cislo z prvniho radku
    size_t second_min = 0; // minimum z druheho radku

	auto word_1_it = word_1.begin(); // iterator pres prvni slovo
	auto word_2_begin = word_2.begin();
	auto word_1_end = word_1.end();
	auto word_2_end = word_2.end();

	for (size_t j = 0; j <= length_2; ++j)
	{
		lev[0][j] = j;
	}
	for (size_t i = 1; word_1_it != word_1_end; ++i)
	{
		auto word_2_it = word_2_begin; // iterator pres druhe slovo
		int word_1_char = 0; // utf8 kod znaku z prvniho slova
		word_1_char = utf8::unchecked::next(word_1_it);

		for (size_t j = 0; word_2_it != word_2_end; ++j)
		{
			int word_2_char = 0; // utf8 kod znaku z druheho slova

			if (j == 0) // prvni radek a sloupec matice naplnime odpovidajicimi default cisly
			{
				lev[1][j] = i;
                second_min = i;
			}
			else
			{
				word_2_char = utf8::unchecked::next(word_2_it);

				size_t min;
				if (word_1_char == word_2_char) // pismena na pozici vlevo nahore v matici jsou shodna
				{
					min = (std::min)({ lev[0][j] + 1, lev[1][j - 1] + 1, lev[0][j - 1] });
				}
				else{ min = (std::min)({ lev[0][j] + 1, lev[1][j - 1] + 1, lev[0][j - 1] + 1 }); }

				dist = lev[1][j] = min;
                second_min = (std::min)(min, second_min);
			}
		}

        // pokud minimum z prvniho a druheho radku preroste maximalni vzdalenost, pak prerusujeme beh a vracime "nekonecno"
        if(first_min > max_distance && second_min > max_distance){ dist = string_t::npos; break; }
        else{ first_min = second_min; }

		temp = lev[0];
		lev[0] = lev[1];
		lev[1] = temp;
	}

	delete[] lev[0];
	delete[] lev[1];
	delete[] lev;
	return dist;
}

std::vector<string_t::const_iterator> Text::UTF8HledejVsechny(const string_t &text, const string_t &jehla)
{
    std::vector<string_t::const_iterator> vysledek;

    if (!utf8::is_valid(text.begin(), text.end()) || !utf8::is_valid(jehla.begin(), jehla.end()))
    {
        return vysledek;
    }

    std::vector<size_t> utf8jehla;
    std::vector<int> tabulka_posunu;
    tabulka_posunu.push_back(-1);
    tabulka_posunu.push_back(0);

    /* Vytvorime jehlu v utf8 kodovani */
    for(auto i = jehla.begin(); i < jehla.end();)
    {
        size_t pismeno = utf8::unchecked::next(i);
        utf8jehla.push_back(pismeno);
    }

    /* NAPLNIT TABULKU POSUNU */
    for(size_t i = 2, posun = 0; i <= utf8jehla.size(); ++i)
    {
        while(posun > 0 && utf8jehla.at(posun) != utf8jehla.at(i - 1)){ posun = tabulka_posunu.at(posun); }
        if(utf8jehla.at(posun) == utf8jehla.at(i - 1)){ posun++; }
        tabulka_posunu.push_back(posun);
    }

    /* SAMOTNE PROHLEDAVANI VSTUPNIHO TEXTU */
    size_t jehla_it = 0;
    for(auto i = text.begin(); i < text.end();)
    {
        size_t pismeno = utf8::unchecked::next(i);

        while(jehla_it > 0 && utf8jehla.at(jehla_it) != pismeno){ jehla_it = tabulka_posunu.at(jehla_it); }
        if(utf8jehla.at(jehla_it) == pismeno){ jehla_it++; }
        if(jehla_it == utf8jehla.size())
        {
            size_t pismeno_delka = utf8::internal::sequence_length(i);

            vysledek.push_back(i - jehla.length() + pismeno_delka);
            jehla_it = tabulka_posunu.at(jehla_it);
        }
    }

    return vysledek;
}

string_t Text::TransformaceInternal(TransfInternalFlags flag, const string_t &slovo, size_t zacatek, size_t delka)
{
    if(slovo.length() == 0){ return slovo; }

    string_t vysledek;

    if (!utf8::is_valid(slovo.begin(), slovo.end())){ return vysledek; }

    vysledek.reserve(slovo.length());

    string_t::const_iterator konec_it;
    string_t::const_iterator zacatek_it = slovo.begin();

    // nastavime zacatek zkoumaneho bloku
    if(zacatek != 0)
    {
        try{ utf8::advance(zacatek_it, zacatek, slovo.end()); }
        catch(utf8::not_enough_room){ return slovo; }
    }

    // nastavime konec zkoumaneho bloku
    if(delka == string_t::npos)
    {
        konec_it = slovo.end();
    }
    else
    {
        konec_it = zacatek_it;
        try{ utf8::advance(konec_it, delka, slovo.end()); }
        catch(utf8::exception){ konec_it = slovo.end(); }
    }

    // hotpath
    if(zacatek == 0 && delka == string_t::npos)
    {
        if(flag == TransfInternalFlags::Lower){ vysledek = boost::locale::to_lower(slovo, TextData::VratUTF8CsLocale()); }
        else{ vysledek = boost::locale::to_upper(slovo, TextData::VratUTF8CsLocale()); }
    }
    else // coldpath
    {
        string_t transform_part;
        string_t after_transform;

        for(auto i = slovo.begin(); i < slovo.end();)
        {
            size_t pismeno_code = utf8::unchecked::next(i); // ziskame pismeno ze slova

            if(i <= zacatek_it) // cast slova pred casti kterou budeme zvetsovat
            {
                utf8::unchecked::append(pismeno_code, std::back_inserter(vysledek));
            }
            else if(i > zacatek_it && i <= konec_it) // pismena transformujeme na velka pouze v urcite oblasti
            {
                utf8::unchecked::append(pismeno_code, std::back_inserter(transform_part));
            }
            else // cast slova po transformovane casti
            {
                utf8::unchecked::append(pismeno_code, std::back_inserter(after_transform));
            }
        }

        // samotna transformace
        if(flag == TransfInternalFlags::Lower)
        {
            transform_part = boost::locale::to_lower(transform_part, TextData::VratUTF8CsLocale());
        }
        else{ transform_part = boost::locale::to_upper(transform_part, TextData::VratUTF8CsLocale()); }

        vysledek += transform_part + after_transform; // slozime vysledne slovo
    }

    return vysledek;
}

string_t Text::NaVelka(const string_t &slovo, size_t zacatek, size_t delka)
{
    return TransformaceInternal(TransfInternalFlags::Upper, slovo, zacatek, delka);
}

string_t Text::NaMala(const string_t &slovo, size_t zacatek, size_t delka)
{
    return TransformaceInternal(TransfInternalFlags::Lower, slovo, zacatek, delka);
}

bool Text::VelikostPismenaInternal(TransfInternalFlags flag, const string_t &slovo, size_t pozice)
{
    // jine znaky nez pismena jsou brana jako mala a velka pismena zaroven
    // pokud je pozice vetsi nez slovo, pak je vraceno false

    auto it = slovo.begin();
    int pismeno_code = 0;
    try
    {
        utf8::advance(it, pozice, slovo.end());
        pismeno_code = utf8::peek_next(it, slovo.end());
    }
    catch(utf8::exception){ return false; }

    string_t pismeno;
    string_t transformed;
    utf8::unchecked::append(pismeno_code, std::back_inserter(pismeno)); // ziskame pismeno, ktere chceme porovnavat

    if(flag == TransfInternalFlags::Lower)
    {
        // zkusime pismeno zmensit a vysledek ulozime jinam
        transformed = boost::locale::to_lower(pismeno, TextData::VratUTF8CsLocale());
    }
    else
    {
        // zkusime pismeno zvetsit a vysledek ulozime jinam
        transformed = boost::locale::to_upper(pismeno, TextData::VratUTF8CsLocale());
    }

    // pokud se pismeno a jeho transformace rovnaji, pak vracime true, jinak false
    if(transformed == pismeno){ return true; }
    else{ return false; }
}

bool Text::JeVelke(const string_t &slovo, size_t pozice)
{
    // jine znaky nez pismena jsou brana jako mala a velka pismena zaroven
    // pokud je pozice vetsi nez slovo, pak je vraceno false

    return Text::VelikostPismenaInternal(TransfInternalFlags::Upper, slovo, pozice);
}

bool Text::JeMale(const string_t &slovo, size_t pozice)
{
    // jine znaky nez pismena jsou brana jako mala a velka pismena zaroven
    // pokud je pozice vetsi nez slovo, pak je vraceno false

    return Text::VelikostPismenaInternal(TransfInternalFlags::Lower, slovo, pozice);
}

bool Text::JeKonecVety(const string_t &slovo)
{
    bool konec = false;
    size_t velikost_slova = slovo.length();

    string_t jedna = "";
    string_t dve = "";
    if(velikost_slova > 0){ jedna = slovo.substr(velikost_slova-1); }
    if(velikost_slova > 1){ dve = slovo.substr(velikost_slova-2); }

    if(jedna == "." || jedna == "!" || jedna == "?" || dve == "\"." || dve == "\"!" || dve == "\"?"
            || dve == "\'." || dve == "\'!" || dve == "\'?")
    { konec = true; }

    return konec;
}

string_t Text::OdstranKonecVety(const string_t &slovo)
{
    string_t vysledek;
    size_t velikost_slova = slovo.length();

    string_t jedna = "";
    string_t dve = "";
    if(velikost_slova > 0){ jedna = slovo.substr(velikost_slova-1); }
    if(velikost_slova > 1){ dve = slovo.substr(velikost_slova-2); }

    if(jedna == "." || jedna == "!" || jedna == "?")
    {
        vysledek = slovo.substr(0,velikost_slova-1);
    }
    else if(dve == "\"." || dve == "\"!" || dve == "\"?"
            || dve == "\'." || dve == "\'!" || dve == "\'?")
    {
        vysledek = slovo.substr(0,velikost_slova-2);
    }
    else
    {
        vysledek = slovo;
    }

    return vysledek;
}

bool Text::JeKonecBloku(const string_t &slovo)
{
    bool konec = false;
    size_t velikost_slova = slovo.length();

    string_t jedna = "";
    string_t dve = "";
    if(velikost_slova > 0){ jedna = slovo.substr(velikost_slova-1); }
    if(velikost_slova > 1){ dve = slovo.substr(velikost_slova-2); }

    if(jedna == "." || jedna == "," || jedna == "!" || jedna == "?" || jedna == ":" || jedna == ")"
            || jedna == "\"" || jedna == "\'"
            || dve == ".:" || dve == "\"." || dve == "\"," || dve == "\"!" || dve == "\"?"
            || dve == "\'." || dve == "\'," || dve == "\'!" || dve == "\'?")
    { konec = true; }

    return konec;
}

string_t Text::OdstranKonecBloku(const string_t &slovo)
{
    size_t velikost_slova = slovo.length();
    string_t vysledek;

    string_t jedna = "";
    string_t dve = "";
    if(velikost_slova > 0){ jedna = slovo.substr(velikost_slova-1); }
    if(velikost_slova > 1){ dve = slovo.substr(velikost_slova-2); }

    if(dve == "\"." || dve == "\"," || dve == "\"!" || dve == "\"?"
            || dve == "\'." || dve == "\'," || dve == "\'!" || dve == "\'?" || dve == ".:")
        {
            vysledek = slovo.substr(0,velikost_slova-2);
        }
    else if(jedna == "." || jedna == "," || jedna == "!" || jedna == "?" || jedna == ":"
            || jedna == ")" || jedna == "\"" || jedna =="\'")
    {
        vysledek = slovo.substr(0,velikost_slova-1);
    }
    else
    {
        vysledek = slovo;
    }

    return vysledek;
}

bool Text::JeZacatekBloku(const string_t &slovo)
{
    bool vysledek = false;
    size_t velikost_slova = slovo.length();

    string_t jedna = "";
    if(velikost_slova > 0){ jedna = slovo.substr(0,1); }

    if(jedna == "\"" || jedna == "\'" || jedna == "(")
    { vysledek = true; }

    return vysledek;
}

string_t Text::OdstranZacatekBloku(const string_t &slovo)
{
    string_t vysledek;
    size_t velikost_slova = slovo.length();

    string_t jedna = "";
    if(velikost_slova > 0){ jedna = slovo.substr(0,1); }

    if(jedna == "\"" || jedna == "\'" || jedna == "(")
    {
        vysledek = slovo.substr(1);
    }
    else
    {
        vysledek = slovo;
    }

    return vysledek;
}

bool Text::ObsahujeTriTecky(const string_t &slovo)
{
    if(slovo.find("...") == (slovo.length() - 3)
            || slovo.find("\u2026") == (slovo.length() - string_t("\u2026").length())){ return true; }
    else{ return false; }
}

string_t Text::OdstranTriTecky(const string_t &slovo)
{
    string_t vysledek;
    size_t velikost_slova = slovo.length();
    string_t konec;
    string_t konec_u;
    size_t unicode_l = string_t("\u2026").length();

    if(velikost_slova > 2){ konec = slovo.substr(velikost_slova-3); }
    if(velikost_slova >= unicode_l){ konec_u = slovo.substr(velikost_slova - unicode_l); }

    if(konec == "..."){ vysledek = slovo.substr(0,velikost_slova-3); }
    else if(konec_u == "\u2026"){ vysledek = slovo.substr(0, velikost_slova - unicode_l); }
    else{ vysledek = slovo; }

    return vysledek;
}
