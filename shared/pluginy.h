#ifndef PLUGINY_H
#define PLUGINY_H

#include "plugin.h"

typedef std::vector<Plugin*> PluginVector;

#ifdef _WIN32
typedef std::vector<HMODULE> LibVector;
typedef HMODULE LibHandle;
#else
typedef std::vector<void*> LibVector;
typedef void* LibHandle;
#endif

class Pluginy;

/**
 * Trida, ktera reprezentuje vrchol zavislostniho stromu pluginu. Tento strom je ulozen v objektu typu Pluginy.
 * Kazdy vrchol obsahuje weak_ptr na otce a shared_ptr na potomky. Dale je zde ukazatel na Plugin a identifikator otevrene dynamicke knihovny.
 * Pri kontrukci a destrukci se zajistuje nacteni/odnacteni pluginu pomoci dynamickeho loaderu.
 */
class PluginyNode
{
public:
    /** Vytvori vrchol, ktery nema otce a ani zadny prirazeny Plugin, proto se hodi na korenovy vrchol. */
    PluginyNode();
    /**
     * Doporuceny a jediny pouzitelny kontruktor na normalni vrchol, ktery neni korenovy.
     * @param rodic weak_ptr na rodice
     * @param jmeno jmeno pluginu, ktery ma byt nacten
     * @param cesta cesta k dynamicke knihovne, ktera obsahuje plugin s danym jmenem
     */
    PluginyNode(std::weak_ptr<PluginyNode> rodic, string_t jmeno, string_t cesta);
    PluginyNode(const PluginyNode& source) = delete;
    PluginyNode& operator=(const PluginyNode& source) = delete;
    PluginyNode(PluginyNode&& source) = delete;
    PluginyNode& operator=(PluginyNode&& source) = delete;
    /** Zajisti odnacteni a spravne uzavreni dynamicke knihovny, zaroven take destrukci objektu pluginu. */
    ~PluginyNode();

    /**
     * Na ulozenem pluginu spusti funkci Run() s odpovidajicimi parametry a pote pomoci jednoducheho systemu paralelizace spusti metody Run() na vsech svych potomcich.
     * Z teto funkce se vystoupi az v momente, kdy jsou ukonceni vsichni potomci.
     * @param titulky soubor titulkovych objektu na kterych se ma provest analyza
     * @param chyby odkaz na tridu Chyby, do ktere vsechny pluginy pridavaji nalezene chyby
     * @return vraci vzdy nulu
     */
    int Run(Titulky titulky, Chyby& chyby);
    /**
     * Na svem pluginu zavola fuknci Reset() a pote tuto metodu spusti i na svych potomcich.
     */
    void Reset();
    /**
     * Do sveho pole potomku prida noveho potomka, ale pouze v pripade ze se nejedna o nullptr.
     * @param potomek shared ukazatel na objekt typu PluginyNode
     */
    void PridejPotomka(std::shared_ptr<PluginyNode> potomek);
    /**
     * Vraci false, pokud se nepovedlo nacist knihovnu, nebo nebylo mozno nacist plugin, diky chybe v konfiguraci.
     */
    bool Provadet();
    /**
     * Vrati jmeno nacteneho pluginu.
     */
    string_t VratJmeno();
    /**
     * Debugovaci funkce, ktera na standardni vystup vypisuje vytvoreny zavislostni strom v jednoduche forme.
     * @param hloubka cislo, ktere udava hloubku zanoreni aktualniho pluginu
     */
    void Print(int hloubka)
    {
        for(int i = 0; i < hloubka; ++i){ std::cout << "\t"; }
        if(Plugin_ == nullptr){ std::cout << "*ROOT" << std::endl; }
        else{ std::cout << Jmeno_ << std::endl; }
        hloubka++;
        for(auto& i : Potomci_){ i->Print(hloubka); }
    }

private:
    string_t Jmeno_;
    std::vector<std::thread> ThreadPool_;
    std::vector<std::shared_ptr<PluginyNode>> Potomci_;
    std::weak_ptr<PluginyNode> Rodic_;
    Plugin* Plugin_;
    LibHandle LibHandle_;
    bool Provadet_;
};

/**
 * Kontejner ukladajici ukazatele na korenovy vrchol PluginyNode. Zajistuje samotne vytvoreni tohoto stromu, pomoci dat z konfigurace.
 * Metody Run() a Reset() predava dale do korenoveho vrcholu.
 */
class Pluginy
{
public:
    /** Defaultni konstruktor je povolen, ostatni zakazany. */
    Pluginy();
    Pluginy(const Pluginy& source) = delete;
    Pluginy& operator=(const Pluginy& source) = delete;
    Pluginy(Pluginy&& source) = delete;
    Pluginy& operator=(Pluginy&& source) = delete;
    /**
     * Destruktor se stara o smazani korenoveho vrcholu, coz ma dominovy efekt na vsechny vrcholy.
     */
    ~Pluginy();

    /**
     * Pomoci internich metod, nejprve nacte informace o hierarchii (z konfiguracniho souboru) a pote si zjisti jake pluginy jsou vubec dostupne ve slozce pluginu.
     * Potom nasleduje samotne vytvoreni zavislostniho stromu, kdy se nejprve nactou pluginy,
     *  ktere nebyli definovany v konfiguraci a pak se donactou vsechny ostatni, podle toho pod ktery vrchol patri.
     * @param include seznam pluginu, ktere se maji nacist
     * @param exclude seznam nazvu pluginu, ktere se urcite maji pri provadeni vynechat
     */
    void NactiPluginy(std::set<string_t> include, std::set<string_t> exclude);
    /**
     * Spusti funkci Run() s odpovidajicimi parametry na korenovem vrcholu.
     * @param titulky
     * @param chyby
     * @return vzdy nula
     */
    int Run(Titulky& titulky, Chyby& chyby);
    /**
     * Na korenovem vrcholu spusti funkci Reset().
     */
    void Reset();
    /**
     * Rekne, jaky je aktualni pocet nactenych pluginu, respektive vrcholu ve strome.
     */
    size_t PocetPluginu();

private:
	void NactiNastaveni();
    void NactiSlozkuPluginu();
    boost::property_tree::ptree NactiStromNastaveni();

    std::shared_ptr<PluginyNode> Root_;
    std::multimap<string_t, string_t> NactenaHierarchie_;
    std::set<string_t> NactenaHierarchieSeznam_;
    std::map<string_t, string_t> SlozkaPluginySOtcem_;
    std::map<string_t, string_t> SlozkaPluginyBezOtce_;
    size_t PocetPluginu_;
};

#endif // PLUGINY_H
