#include "chyba.h"

Chyba::Chyba(string_t nazev_pluginu, string_t nazev_chyby, size_t titulek, size_t pozice)
    : NazevChyby_(nazev_chyby), NazevPluginu_(nazev_pluginu), Titulek_(titulek), Pozice_(pozice)
{
}

void Chyba::PridejOpravu(const string_t& oprava)
{
    Opravy_.push_back(oprava);
    return;
}

string_t Chyba::VratNazevChyby()
{
    return NazevChyby_;
}

string_t Chyba::VratNazevPluginu()
{
    return NazevPluginu_;
}

size_t Chyba::VratCisloTitulku()
{
    return Titulek_;
}

size_t Chyba::VratPozici()
{
    return Pozice_;
}

const StringVector &Chyba::VratOpravy()
{
    return Opravy_;
}
