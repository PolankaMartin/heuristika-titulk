#ifndef KONF_SYSTEM_H
#define KONF_SYSTEM_H

#include "konfigurace.h"

#ifdef _WIN32
#ifdef EXPORT_HEURISTIKA_BUILD
#define EXPORT_KONF __declspec(dllexport)
#else
#define EXPORT_KONF __declspec(dllimport)
#endif
#else
#define EXPORT_KONF
#endif

/**
 * Singleton zpristupnujici objekt Konfigurace, nelze ho tedy zkonstruovat.
 */
class KonfSystem
{
public:
    KonfSystem() = delete;
    KonfSystem(KonfSystem const&) = delete;
    void operator=(KonfSystem const&) = delete;

    /**
     * Staticka funkce, ktera vraci referenci na objekt Konfigurace, ktera bude stejna pro vsechny pluginy.
     */
	static Konfigurace EXPORT_KONF & VratKonfiguraci()
    {
        static Konfigurace Konf_;
        return Konf_;
    }
};

#endif // KONF_SYSTEM_H
