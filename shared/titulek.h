#ifndef TITULEK_H
#define TITULEK_H

// titulek.h obsahuje hlavni typedefs a inkludovane hlavicky

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <fstream>
#include <functional>
#include <memory>
#include <utility>
#include <iomanip>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>

#include "boost/algorithm/string.hpp"
#include "boost/filesystem.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"
#include "boost/property_tree/xml_parser.hpp"
#include "boost/program_options.hpp"
#include "boost/locale.hpp"

#include "../utf8_v2_3_4/source/utf8.h"

#ifdef _WIN32
#define NOMINMAX
#include <windows.h>
#else
#include <dlfcn.h>
#endif

typedef std::string string_t;
typedef std::vector<string_t> StringVector;
typedef std::vector<size_t> SizeVector;

#include "text.h"

/**
 * Trida sdruzujici originalni text titulku a potencionalni dalsi vycistene varianty, napr.: text bez html znacek, bez tecek, carek a dalsich oddelovacu.
 * Originalni text nelze menit, ale vycistenou variantu, ktera se bude klasicky nejvice pouzivat menit lze, ale s pomoci specialnich funkci.
 */
class TextZaznam
{
public:
    /** Defaultni konstruktor zakazan. */
    TextZaznam() = delete;
    /** Copy metody povoleny. */
    TextZaznam(const TextZaznam& source) = default;
    TextZaznam& operator=(const TextZaznam& source) = default;
#ifdef _WIN32
	/* Defaultni move metody neumi Visual Studio 2013 aktualne prelozit a bohuzel ani generovat... */
#else
	/** Move metody povoleny. */
	TextZaznam(TextZaznam&& source) = default;
	TextZaznam& operator=(TextZaznam&& source) = default;
#endif

    /**
     * Konstruktor nahrazujici defaultni konstruktor, pri konstrukci je treba predat vsech 6 parametru, pozdeji zmenit nepujdou.
     * @param text_orig originalni text
     * @param text text vycisteny od html znacek
     * @param text_zmenseny
     * @param pozice_orig pozice originalniho textu v titulku
     * @param pozice pozice vycisteneho textu v titulku
     * @param pozice_zmenseny
     */
    TextZaznam(string_t text_orig, string_t text, string_t text_zmenseny, size_t pozice_orig, size_t pozice, size_t pozice_zmenseny)
        : TextOrig_(text_orig), Text_(text), TextZmenseny_(text_zmenseny), PoziceOrig_(pozice_orig), Pozice_(pozice), PoziceZmenseny_(pozice_zmenseny)
    {}

    /**
     * Vrati referenci na modifikovatelnou stringovou promennou reprezentujici vycisteny text
	 * @note Pri dosazovani do vracene reference je nutne, aby dosazovany text byl v UTF8, jinak hrozi nedefinovane chovani programu!
     */
    string_t& VratModText();
    /**
     * Vraci konstantni referenci na stringovou promennou originalniho textu.
     */
    const string_t& VratOrigText() const;
    /**
     * Vraci const referenci na vycisteny text.
     */
    const string_t& VratText() const;
    /**
     * Zpristupnuje konstantni referenci na vycisteny a zmenseny text.
     */
    const string_t& VratZmensenyText() const;

    /**
     * Vraci pozici originalniho textu v titulku.
     */
    size_t VratPoziciOrig() const;
    /**
     * Vraci bezznamenkove cislo urcujici pozici vycisteneho textu.
     */
    size_t VratPozici() const;
    /**
     * Souvisi s funkci VratZmensenyText(), jehoz pozici vraci.
     */
    size_t VratPoziciZmenseny() const;
private:
    string_t TextOrig_;
    string_t Text_;
    string_t TextZmenseny_;
    size_t PoziceOrig_;
    size_t Pozice_;
    size_t PoziceZmenseny_;
};

typedef std::vector<TextZaznam> TextVector;

/**
 * Trida ukladajici informace o jednotlivych titulcich. Je zde ulozen text titulku, jeho zacatek a konec a jeho ID.
 * ID je kontrolovano a pripadne je hlasena chyba, ale program pokracuje v behu, tudiz dva titulky mohou mit stejna cisla.
 */
class Titulek
{
public:
    /** Defaultni konstroktor neni povolen. */
    Titulek() = delete;
    /** Copy metody povoleny. */
    Titulek(const Titulek& source) = default;
    Titulek& operator=(const Titulek& source) = default;
#ifdef _WIN32
	/* Defaultni move metody neumi Visual Studio 2013 aktualne prelozit a bohuzel ani generovat... */
#else
	/** Move metody povoleny. */
	Titulek(Titulek&& source) = default;
	Titulek& operator=(Titulek&& source) = default;
#endif

    /**
     * Objektu titulek musi byt pri konstrukci predany vsechny 4 parametry, protoze je pozdeji nelze menit.
     * @param uid poradove cislo titulku
     * @param zacatek zacatek titulku pocitany v milisekundach
     * @param konec cas konce titulku v milisekundach
     * @param radky std::vector<std::string> ukladajici jednotlive radky titulku
     */
    Titulek(size_t uid, long zacatek, long konec, StringVector radky);

    /**
     * Vrati poradove cislo titulku.
     */
    size_t VratUID();
    /**
     * Vrati cas zacatku titulku v milisekundach.
     */
    long VratZacatek();
    /**
     * Vrati konec titulku v milisekundach
     */
    long VratKonec();
    /**
     * Vraci pocet znaku, respektive pocet bajtu, ktere text titulku obsahuje.
     * @note Problem s delkou ceskych znaku ulozenych v UTF-8
     */
    size_t PocetZnaku();
	/**
    * Vraci pocet znaku v kodovani UTF8, ktere text titulku obsahuje.
	*/
	size_t PocetUTF8Znaku();

    /**
     * Vrati pocet radku v titulku
     */
    size_t PocetRadku();
    /**
     * Vracena hodnota je reference na objekt TextZaznam, ktery v sobe schranuje jak originalni, tak vycisteny text.
     * Funkce vraci jeden konkretni radek urceny jeho indexem pos.
     * Interne jsou radky ulozeny v std::vector<TextZaznam> a ziskavani prvku se provadi pomoci metody at(pos), tudiz muze vyhazovat vyjimky
     * Priklad pouziti:
     * @code
     * for(size_t i = 0; i < PocetRadku(); ++i)
     * {
     *     const string_t& orig_radek = VratRadek(i).VratOrigText();
     *     const string_t& radek = VratRadek(i).VratText();
     * }
     * @endcode
     * @param pos pozice radku v poli radku
     */
    TextZaznam& VratRadek(size_t pos);

    /**
     * Vrati pocet slov v titulku
     */
    size_t PocetSlov();
    /**
     * Vracena hodnota je reference na objekt TextZaznam, ktery v sobe uklada jak originalni, tak vycisteny text.
     * Funkce vraci jedno konkretni slovo urcene jeho indexem pos.
     * Interne jsou slova ulozeny v std::vector<TextZaznam> a ziskavani slova se provadi pomoci metody at(pos), tudiz muze vyhazovat vyjimky
     * Priklad pouziti:
     * @code
     * for(size_t i = 0; i < PocetSlov(); ++i)
     * {
     *     const string_t& orig_text = VratSlovo(i).VratOrigText();
     *     const string_t& text = VratSlovo(i).VratText();
     * }
     * @endcode
     * @param pos index slova v kontextu celeho titulku
     */
    TextZaznam& VratSlovo(size_t pos);

    /**
     * Staticka metoda, ktera z casu v hodinach, minutach, sekundach a milisekundach udela cas v milisekundach
     * @param hodiny
     * @param minuty
     * @param sekundy
     * @param milisekundy
     * @return vraci long coz je cas v milisekundach
     */
    static long CasNaLong(size_t hodiny, size_t minuty, size_t sekundy, size_t milisekundy);
    /**
     * Staticka metoda prevadejici cas ve formatu: 00:00:00,000 na cas v milisekundach
     * @note Prozatim nejsou nijak odstranovany mezery, tudiz presna delka stringu musi byt 12 a neni nijak osetreno pokud string bude kratsi.
     * @param cas format 00:00:00,100
     * @return cas v milisekundach
     */
    static long StringNaLong(string_t cas);
    /**
     * Staticka funkce prevadejici cas v milisekundach na format "hh:mm:ss,mss"
     * @param cas
     * @return vraci string ve formatu 00:00:00,000
     */
    static string_t LongNaString(long cas);
    /**
     * Staticka funkce, ktera z doby zacatku a doby zobrazeni udela string ve formatu "hh:mm:ss:mss --> hh:mm:ss:mss", pricemz prvni je cas zacatku a druhy cas konce.
     * @param zacatek cas zacatku v milisekundach
     * @param doba_zobrazeni doba zobrazeni v milisekundach
     * @return vraci text ve formatu "hh:mm:ss:mss --> hh:mm:ss:mss"
     */
	static string_t CasNaString(long zacatek, long doba_zobrazeni);
private:
    void Parse(const StringVector &radky, StringVector& slova, SizeVector& pozice_slov); // parametry slova a pozice_slov jsou vystupni
    string_t VycistiText(const string_t& vstup);
    string_t StripHTMLTags(const string_t& vstup);

    void NaplnTextVector(const StringVector& text_orig, const SizeVector& pozice, TextVector& vystup);


    size_t UID_;

    long Zacatek_; //milisekundy
    long Konec_; //milisekundy

    TextVector Radky_;
    TextVector Slova_;

    size_t PocetZnaku_; // problem s delkou oescapovanych znaku
	size_t PocetUTF8Znaku_;
};

#endif // TITULEK_H
