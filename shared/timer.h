#ifndef TIMER_H
#define TIMER_H

#include <chrono>

/**
 * Jednoducha trida zajistujici stopky, zahajeni mereni neni pri konstrukci, ale pri zavolani funkce Start().
 * K zachytavani casu jsou pouzity funkce a struktury z std::chrono (C++11).
 * Trida je otemplatovana, pricemz je na vyber mezi "hodinami", dobou trvani (s,ms,us, apod.) a vracenou hodnotou,
 *  prvni dva parametry musi byt z std::chrono a treti musi byt ciselny typ (neni ale kontrolovano).
 */
template<typename ClockT = std::chrono::high_resolution_clock, typename DurationT = std::chrono::microseconds, typename ReturnT = double>
class Timer
{
public:
    /** Defaultni konstruktor povolen. */
    Timer(){}
    /** Copy metody zakazany. */
    Timer(const Timer& source) = delete;
    Timer& operator=(const Timer& source) = delete;

    /**
     * Zavolanim teto funkce se nastavi cas zapnuti stopek.
     */
    void Start()
    {
        Start_ = ClockT::now();
        return;
    }
    /**
     * Vraci uplynuly cas od zapnuti stopek.
     * @return vraci double, coz je pocet microsekund od zapnuti stopek
     */
    ReturnT Time()
    {
        auto now = ClockT::now();

        auto result = std::chrono::duration_cast<DurationT>(now - Start_);

        return static_cast<ReturnT>(result.count());
    }
private:
    typename ClockT::time_point Start_;
};

#endif // TIMER_H
