#ifndef TITULKY_H
#define TITULKY_H

#include "titulek.h"

typedef std::vector<Titulek> TitulekVector;

/**
 * Kontejner udrzujici pole objektu Titulek.
 */
class Titulky
{
public:
    /** Vsechny defaultni metody jsou povoleny. */
    Titulky();
    Titulky(const Titulky& source) = default;
    Titulky& operator=(const Titulky& source) = default;
#ifdef _WIN32
	/* Defaultni move metody neumi Visual Studio 2013 aktualne prelozit a bohuzel ani generovat... */
#else
	Titulky(Titulky&& source) = default;
	Titulky& operator=(Titulky&& source) = default;
#endif

    /**
     * Pridava objekt Titulek do kontejneru.
     */
    void PushBack(Titulek pridej);
    /**
     * Vraci referenci na prvek na zadane pozici. Chova se jako at(), tzn. kontroluje meze, pripadne vyhodi vyjimku
     */
    Titulek& operator[](size_t pozice);
    /**
     * Vraci referenci na prvek na zadane pozici.
     */
    Titulek& at(size_t pozice);
    /**
     * Vrati pocet objektu Titulek v kontejneru.
     */
    size_t Pocet();
    /**
     * Vynuluje veskere polozky z tohoto kontejneru.
     */
    void Reset();
private:
    TitulekVector Data_;
};

#endif // TITULKY_H
