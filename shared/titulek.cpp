#include "titulek.h"

string_t& TextZaznam::VratModText()
{
    return Text_;
}

const string_t& TextZaznam::VratOrigText() const
{
    return TextOrig_;
}

const string_t& TextZaznam::VratText() const
{
    return Text_;
}

const string_t& TextZaznam::VratZmensenyText() const
{
    return TextZmenseny_;
}

size_t TextZaznam::VratPozici() const
{
    return Pozice_;
}

size_t TextZaznam::VratPoziciOrig() const
{
    return PoziceOrig_;
}

size_t TextZaznam::VratPoziciZmenseny() const
{
    return PoziceZmenseny_;
}

Titulek::Titulek(size_t uid, long zacatek, long konec, StringVector radky)
	: UID_(uid), Zacatek_(zacatek), Konec_(konec), PocetZnaku_(0), PocetUTF8Znaku_(0)
{
    StringVector slova;
    SizeVector pozice_slov;
    SizeVector pozice_radek;

    size_t start = 0;
    for(auto& i : radky) // naplni se SizeVector pozice zacatku radek
    {
        pozice_radek.push_back(start);

        start += i.length();
		PocetUTF8Znaku_ += utf8::distance(i.begin(), i.end());
    }
    PocetZnaku_ = start;

    /* Naplneni samotnych promennych Radky_ a Slova_ */
    NaplnTextVector(radky,pozice_radek,Radky_);

    Parse(radky,slova,pozice_slov);

    NaplnTextVector(slova,pozice_slov,Slova_);
}

size_t Titulek::VratUID()
{
    return UID_;
}

long Titulek::VratZacatek()
{
    return Zacatek_;
}

long Titulek::VratKonec()
{
    return Konec_;
}

size_t Titulek::PocetRadku()
{
    return Radky_.size();
}

size_t Titulek::PocetSlov()
{
    return Slova_.size();
}

TextZaznam& Titulek::VratRadek(size_t pos)
{
    return Radky_.at(pos);
}

TextZaznam& Titulek::VratSlovo(size_t pos)
{
    return Slova_.at(pos);
}

size_t Titulek::PocetZnaku()
{
    return PocetZnaku_;
}

size_t Titulek::PocetUTF8Znaku()
{
	return PocetUTF8Znaku_;
}

void Titulek::Parse(const StringVector &radky, StringVector &slova, SizeVector &pozice_slov)
{
    // boost::split() nelze pouzit nechava prazdne stringy ("")... nezadouci => napsano vlastni
    size_t pocet_znaku = 0;
    size_t soucet_znaku_z_predchozich_radku = 0;

    for(auto &i : radky)
    {
        size_t pozice_v_radku = 0;
        pocet_znaku = i.length();

        string_t radka(i);
        size_t mezera = radka.find(' ');
        size_t delka_predchoziho = 0;

        while(mezera != string_t::npos)
        {
            string_t pridej = radka.substr(0,mezera);
            radka = radka.substr(mezera+1,string_t::npos);

            if(pridej != "")
            {
                slova.push_back(pridej);
                pozice_slov.push_back(pozice_v_radku + soucet_znaku_z_predchozich_radku);
            }

            mezera = radka.find(' ');
            delka_predchoziho = pridej.length();
            pozice_v_radku += delka_predchoziho + 1;
        }

        if(radka.length() != 0) // flush zbytek
        {
            slova.push_back(radka);
            pozice_slov.push_back(pozice_v_radku + soucet_znaku_z_predchozich_radku);
        }

        soucet_znaku_z_predchozich_radku += pocet_znaku;
    }

    return;
}

string_t Titulek::VycistiText(const string_t& vstup)
{
    string_t vysledek;

    vysledek = StripHTMLTags(vstup);

    return vysledek;
}

string_t Titulek::StripHTMLTags(const string_t &vstup)
{
    string_t vysledek = "";
    string_t temp_vstup(vstup);

    size_t tag = temp_vstup.find('<');
    while(tag != string_t::npos)
    {
        string_t dve(temp_vstup.substr(tag+1,2));
        string_t tri(temp_vstup.substr(tag+1,3));
        string_t sest(temp_vstup.substr(tag+1,6));
        string_t jedenact(temp_vstup.substr(tag+1,11));

        if(dve == "b>" || dve == "i>" || dve == "u>" || dve == "B>" || dve == "I>" || dve == "U>")
        {
            vysledek += temp_vstup.substr(0,tag);
            temp_vstup = temp_vstup.substr(tag+3,string_t::npos);
        }
        else if(tri == "/b>" || tri == "/i>" || tri == "/u>" || tri == "/B>" || tri == "/I>" || tri == "/U>")
        {
            vysledek += temp_vstup.substr(0,tag);
            temp_vstup = temp_vstup.substr(tag+4,string_t::npos);
        }
        else if(sest == "/font>" || sest == "/FONT>")
        {
            vysledek += temp_vstup.substr(0,tag);
            temp_vstup = temp_vstup.substr(tag+5,string_t::npos);
        }
        else if(jedenact == "font color=" || jedenact == "FONT COLOR=")
        {
            vysledek += temp_vstup.substr(0,tag);
            size_t konec = temp_vstup.find('>');
            temp_vstup = temp_vstup.substr(konec+1,string_t::npos);
        }
        else
        {
            vysledek += temp_vstup.substr(0,tag);
            temp_vstup = temp_vstup.substr(tag+1,string_t::npos);
        }

        tag = temp_vstup.find('<');
    }

    vysledek += temp_vstup; // flush zbytek

    return vysledek;
}

void Titulek::NaplnTextVector(const StringVector &text_orig, const SizeVector &pozice, TextVector &vystup)
{
    size_t konec = text_orig.size();

    for(size_t i = 0; i < konec; ++i)
    {
        string_t orig(text_orig.at(i));
        size_t poz = pozice.at(i);
        string_t text = VycistiText(orig);
        size_t posun_oproti_orig = orig.find(text);
        string_t zmenseny = Text::NaMala(text);

        //std::cout << orig << ": " << poz << " -> " << text << ": " << (poz + posun_oproti_orig) << std::endl << std::endl;

        vystup.push_back(TextZaznam(orig,text,zmenseny,poz,poz+posun_oproti_orig,poz+posun_oproti_orig));
    }

    return;
}

long Titulek::CasNaLong(size_t hodiny, size_t minuty, size_t sekundy, size_t milisekundy)
{
    long vysledek;
    vysledek = milisekundy;
    vysledek += (sekundy*1000);
    vysledek += (minuty*60*1000);
    vysledek += (hodiny*60*60*1000);
    return vysledek;
}

long Titulek::StringNaLong(string_t cas)
{
    size_t hodiny = stoi(cas.substr(0,1));
    size_t minuty = stoi(cas.substr(3,4));
    size_t sekundy = stoi(cas.substr(6,7));
    size_t milisekundy = stoi(cas.substr(9,11));
    return CasNaLong(hodiny,minuty,sekundy,milisekundy);
}

string_t Titulek::LongNaString(long cas)
{
    long odecti;
    long hodiny;
    long minuty;
    long sekundy;
    long milisekundy;
    std::stringstream vysledek;

    hodiny = cas / (60*60*1000);
    odecti = hodiny * 60 * 60 * 1000;
    cas -= odecti;

    minuty = cas / (60*1000);
    odecti = minuty * 60 * 1000;
    cas -= odecti;

    sekundy = cas / 1000;
    odecti = sekundy * 1000;
    cas -= odecti;

    milisekundy = cas;

    vysledek << std::setfill('0') << std::setw(2) << hodiny << ":" << std::setw(2) << minuty << ":"
             << std::setw(2) << sekundy << "," << std::setw(3) << milisekundy;

    return vysledek.str();
}

string_t Titulek::CasNaString(long zacatek, long doba_zobrazeni)
{
    return LongNaString(zacatek) + " --> " + LongNaString(zacatek + doba_zobrazeni);
}
