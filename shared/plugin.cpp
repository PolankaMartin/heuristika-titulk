#include "plugin.h"

Plugin::Plugin(string_t nazev_chyby, string_t nazev_pluginu)
    : NazevChyby_(nazev_chyby), NazevPluginu_(nazev_pluginu),
      Provadet_(true), Log_(LogSystem::VratLog())
{}

void Plugin::ErrorNastaveni()
{
    // Pokud je tato funkce zavolana, pak nebylo nastaveni prislusneho pluginu
    // nacteno, pricemz je promenna Provadet_ nastavena na false
    // a dany Plugin nebude ulozen do kontejneru Pluginu

    string_t error_msg = "Nastaveni pluginu: \"" + NazevPluginu() + "\" nebylo nacteno! Plugin nebude nacten.";

    Log_.Zapis("plugin", error_msg);

    Provadet_ = false;
    return;
}

boost::property_tree::ptree Plugin::NactiStromNastaveniObecne(string_t nazev_nastaveni, string_t log_text)
{
    boost::property_tree::ptree strom;

    if(KonfSystem::VratKonfiguraci().VratAltKonfiguraci() != "")
    {
        std::stringstream ss(KonfSystem::VratKonfiguraci().VratAltKonfiguraci());

        try
        {
            boost::property_tree::xml_parser::read_xml(ss,strom);
            strom = strom.get_child("heuristika_titulku." + nazev_nastaveni);
            LogSystem::VratLog().Zapis("plugin", "Pro " + log_text + " bude pouzita serializovana konfigurace v XML.");
        }
        catch(...)
        {
            strom.clear();
        }

        if(strom.empty()) // nacteni konfigurace v XML nebylo uspesne, zkusime JSON
        {
            ss.seekg(0, ss.beg);

            try
            {
                boost::property_tree::json_parser::read_json(ss, strom);
                strom = strom.get_child("heuristika_titulku." + nazev_nastaveni);
                LogSystem::VratLog().Zapis("plugin", "Pro " + log_text + " bude pouzita serializovana konfigurace v JSON.");
            }
            catch(...)
            {
                strom.clear();
            }
        }
    }
    if(KonfSystem::VratKonfiguraci().VratAltSoubor() != "" && strom.empty())
    {
        std::ifstream soubor(KonfSystem::VratKonfiguraci().VratAltSoubor());

        try
        {
            boost::property_tree::xml_parser::read_xml(soubor,strom);
            strom = strom.get_child("heuristika_titulku." + nazev_nastaveni);
            LogSystem::VratLog().Zapis("plugin", "Pro " + log_text + " bude pouzita konfigurace z alternativniho souboru v XML.");
        }
        catch(...)
        {
            strom.clear();
        }

        if(strom.empty()) // nacteni konfigurace v XML nebylo uspesne, zkusime JSON
        {
            soubor.seekg(0, soubor.beg);

            try
            {
                boost::property_tree::json_parser::read_json(soubor, strom);
                strom = strom.get_child("heuristika_titulku." + nazev_nastaveni);
                LogSystem::VratLog().Zapis("plugin", "Pro " + log_text + " bude pouzita konfigurace z alternativniho souboru v JSON.");
            }
            catch(...)
            {
                strom.clear();
            }
        }
    }
    if(strom.empty())
    {
        std::ifstream soubor(KonfSystem::VratKonfiguraci().VratDefaultSlozku() + nazev_nastaveni + ".conf");

        try
        {
            boost::property_tree::xml_parser::read_xml(soubor,strom);
            strom = strom.get_child(nazev_nastaveni);
            LogSystem::VratLog().Zapis("plugin", "Pro " + log_text + " bude pouzita defaultni konfigurace v XML.");
        }
        catch(...)
        {
            strom.clear();
        }

        if(strom.empty()) // nacteni konfigurace v XML nebylo uspesne, zkusime JSON
        {
            soubor.seekg(0, soubor.beg);

            try
            {
                boost::property_tree::json_parser::read_json(soubor, strom);
                strom = strom.get_child(nazev_nastaveni);
                LogSystem::VratLog().Zapis("plugin", "Pro " + log_text + " bude pouzita defaultni konfigurace v JSON.");
            }
            catch(...)
            {
                strom.clear();
            }
        }
    }

    return strom;
}

boost::property_tree::ptree Plugin::NactiStromNastaveni(string_t nazev_pluginu)
{
    string_t nazev;
    if(nazev_pluginu == ""){nazev = NazevPluginu();}
    else{nazev = nazev_pluginu;}

    return NactiStromNastaveniObecne(nazev, "plugin \"" + NazevPluginu() + "\" a nastaveni \"" + nazev + "\"");
}
