#include "chyby.h"

Chyby::Chyby()
{
}

size_t Chyby::Pocet()
{
    DataMutex_.lock();
    int vysl = Data_.size();
    DataMutex_.unlock();

    return vysl;
}

void Chyby::PushBack(Chyba pridej)
{
    DataMutex_.lock();
    Data_.push_back(pridej);
    DataMutex_.unlock();
    return;
}

Chyba& Chyby::operator[](size_t pozice)
{
    DataMutex_.lock();
    Chyba& vysledek = Data_.at(pozice);
    DataMutex_.unlock();

    return vysledek;
}

Chyba& Chyby::at(size_t pozice)
{
    DataMutex_.lock();
    Chyba& vysledek = Data_.at(pozice);
    DataMutex_.unlock();

    return vysledek;
}

void Chyby::Reset()
{
    DataMutex_.lock();
    Data_.clear();
    DataMutex_.unlock();
}
