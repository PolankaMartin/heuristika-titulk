#include "pluginy.h"

PluginyNode::PluginyNode()
    : Rodic_(), Plugin_(nullptr), LibHandle_(0), Provadet_(true)
{}

PluginyNode::PluginyNode(std::weak_ptr<PluginyNode> rodic, string_t jmeno, string_t cesta)
    : Jmeno_(jmeno), Rodic_(rodic), Plugin_(nullptr), LibHandle_(0), Provadet_(true)
{
#ifdef _WIN32
    HMODULE lib = LoadLibrary(cesta.data());
    if (!lib)
    {
        std::cerr << "Nepodarilo se otevrit knihovnu: " << cesta << '\n';
        Provadet_ = false;
        return;
    }

    vytvor_t* vytvor_plugin = (vytvor_t*)GetProcAddress(lib, "vytvor");
    if (!vytvor_plugin)
    {
        std::cerr << "Nemuzu nacist symbol vytvor v knihovne: " << cesta << std::endl;
        Provadet_ = false;
        return;
    }

    Plugin* plugin = vytvor_plugin();

    if (plugin->Provadet() == true)
    {
        Plugin_ = plugin;
        LibHandle_ = lib;

        string_t msg = "Plugin \"" + plugin->NazevPluginu() + "\" nacten!";
        LogSystem::VratLog().Zapis("plugin", msg);

        return;
    }
    else
    {
        znic_t* znic_plugin = (znic_t*)GetProcAddress(lib, "znic");
        if (!znic_plugin)
        {
            std::cerr << "Nemuzu nacist symbol znic v knihovne: " << cesta << '\n';
        }

        znic_plugin(plugin);

        FreeLibrary(lib);

        Provadet_ = false;
        return;
    }
#else
    // nacist jednotlive pluginy
    void* lib = dlopen(cesta.data(), RTLD_LAZY);
    if (!lib)
    {
        std::cerr << "Nepodarilo se otevrit knihovnu: " << dlerror() << '\n';
        Provadet_ = false;
        return;
    }
    dlerror();

    vytvor_t* vytvor_plugin = (vytvor_t*)dlsym(lib, "vytvor");
    const char* dlsym_error = dlerror();
    if (dlsym_error)
    {
        std::cerr << "Nemuzu nacist symbol vytvor: " << dlsym_error << '\n';
        Provadet_ = false;
        return;
    }

    Plugin* plugin = vytvor_plugin();

    if (plugin->Provadet() == true)
    {
        Plugin_ = plugin;
        LibHandle_ = lib;

        string_t msg = "Plugin \"" + plugin->NazevPluginu() + "\" nacten!";
        LogSystem::VratLog().Zapis("plugin", msg);

        return;
    }
    else
    {
        znic_t* znic_plugin = (znic_t*)dlsym(lib, "znic");
        dlsym_error = dlerror();
        if (dlsym_error)
        {
            std::cerr << "Nemuzu nacist symbol znic: " << dlsym_error << '\n';
        }

        znic_plugin(plugin);

        dlclose(lib);

        Provadet_ = false;
        return;
    }
#endif
}

PluginyNode::~PluginyNode()
{
    Potomci_.clear();

    if(Plugin_ == nullptr){ return; }

#ifdef _WIN32
        znic_t* znic_plugin = (znic_t*)GetProcAddress(LibHandle_, "znic");
        if (!znic_plugin)
        {
            std::cerr << "Nemuzu nacist symbol znic v pluginu: " << VratJmeno() << '\n';
        }

        znic_plugin(Plugin_);

        FreeLibrary(LibHandle_);
#else
        znic_t* znic_plugin = (znic_t*)dlsym(LibHandle_, "znic");
        const char* dlsym_error = dlerror();
        if (dlsym_error)
        {
            std::cerr << "Nemuzu nacist symbol znic: " << dlsym_error << '\n';
        }

        znic_plugin(Plugin_);

        dlclose(LibHandle_);
#endif
}

int PluginyNode::Run(Titulky titulky, Chyby &chyby)
{
    if(Plugin_ != nullptr)
    {
        size_t pocet = titulky.Pocet();
        for(size_t i = 0; i < pocet; ++i)
        {
            Plugin_->Run(titulky.at(i), chyby);
        }
    }

    if(Potomci_.size() != 0)
    {
        for(size_t i = 1; i < Potomci_.size(); ++i)
        {
            ThreadPool_.push_back(std::thread(std::bind(&PluginyNode::Run, Potomci_.at(i), titulky, std::ref(chyby))));
        }

        Potomci_.at(0)->Run(titulky, chyby);

        for(auto& i : ThreadPool_)
        {
            i.join();
        }
    }

    return 0;
}

void PluginyNode::Reset()
{
    if(Plugin_ != nullptr)
    {
        Plugin_->Reset();
    }

    for(size_t i = 0; i < Potomci_.size(); ++i)
    {
        Potomci_.at(i)->Reset();
    }
}

void PluginyNode::PridejPotomka(std::shared_ptr<PluginyNode> potomek)
{
    if(potomek != nullptr){ Potomci_.push_back(potomek); }
}

bool PluginyNode::Provadet()
{
    return Provadet_;
}

string_t PluginyNode::VratJmeno()
{
    return Jmeno_;
}



/*** Nasleduji definice funkci ze tridy Pluginy ***/

Pluginy::Pluginy() : Root_(nullptr), PocetPluginu_(0)
{}

Pluginy::~Pluginy()
{
    Root_.reset();
}

size_t Pluginy::PocetPluginu()
{
    return PocetPluginu_;
}

boost::property_tree::ptree Pluginy::NactiStromNastaveni()
{
    return Plugin::NactiStromNastaveniObecne("hierarchie_pluginu", "\"hierarchii_pluginu\"");
}

void Pluginy::NactiNastaveni()
{
    bool vysledek = true;
    boost::property_tree::ptree strom = NactiStromNastaveni();
    if(strom.empty() == true){ vysledek = false; }

    try
    {
        for(auto& i : strom)
        {
            string_t jmeno = i.second.get<string_t>("jmeno");
            string_t rodic = i.second.get<string_t>("rodic");
            auto res = NactenaHierarchieSeznam_.insert(jmeno);
            if(res.second == true){ NactenaHierarchie_.insert(std::make_pair(rodic, jmeno)); }
            else
            {
                LogSystem::VratLog().Zapis("pluginy", "Plugin \"" + jmeno
                                           + "\" mel nadefinovano vice rodicu. Vynechavam rodice: \""
                                           + rodic + "\"");
            }
        }
    }
    catch(...)
    {
        vysledek = false;
    }

    if(vysledek == false)
    {
        string_t error_msg = "Nastaveni \"hierarchie_pluginu\" nebylo nacteno! Chyba v souboru nastaveni.";
        LogSystem::VratLog().Zapis("pluginy", error_msg);
    }
}

int Pluginy::Run(Titulky &titulky, Chyby &chyby)
{
    if(Root_ != nullptr)
    {
        Root_->Run(titulky, chyby);
    }

    return 0;
}

void Pluginy::Reset()
{
    if(Root_ != nullptr)
    {
        Root_->Reset();
    }
}

void Pluginy::NactiSlozkuPluginu()
{
#ifdef _WIN32
    string_t dll_extension = ".dll";
    size_t dll_ext_length = 4;
#else
    string_t dll_extension = ".so";
    size_t dll_ext_length = 3;
#endif

    using namespace boost::filesystem;
    path slozka("./pluginy");
    if(exists(slozka))
    {
        if(is_directory(slozka))
        {
            directory_iterator end = directory_iterator();
            for(directory_iterator i = directory_iterator(slozka);
                i != end; ++i)
            {
                if(i->path().string().length() < dll_ext_length){ continue; }

                string_t koncovka = i->path().string().substr(i->path().string().length() - dll_ext_length, string_t::npos);
                string_t nacti = i->path().string(); // plna cesta k souboru
                string_t basename_pl = boost::filesystem::basename(nacti); // basename v boostu rovnou odstrani i koncovky

                koncovka = Text::NaMala(koncovka);

                // pokud souhlasi jak koncovka, tak to, ze basename je delsi nez tri a zacina pismeny lib,
                // pak knihovnu oznacime k nacteni
                if(koncovka == dll_extension && basename_pl.length() > 3 && basename_pl.find("lib") == 0)
                {
                    if(NactenaHierarchieSeznam_.find(basename_pl.substr(3))
                            == NactenaHierarchieSeznam_.end())
                    {
                        SlozkaPluginyBezOtce_.insert(std::make_pair(basename_pl.substr(3), nacti));
                    }
                    else
                    {
                        SlozkaPluginySOtcem_.insert(std::make_pair(basename_pl.substr(3), nacti));
                    }
                }
            }
        }
        else // definovana slozka pluginu neni slozka, ale bud soubor nebo neco jineho
        {
            string_t error_msg = "Objevila se fatalni chyba: ./pluginy neni slozka!";
            LogSystem::VratLog().Zapis("error_system", error_msg);
            std::cerr << error_msg << std::endl;

            exit(1);
        }
    }
    else // ani soubor, ani slozka s definovanym nazvem slozky pluginu neexistuje
    {
        string_t error_msg = "Objevila se fatalni chyba: ./pluginy neni slozka!";
        LogSystem::VratLog().Zapis("error_system", error_msg);
        std::cerr << error_msg << std::endl;

        exit(1);
    }
}

void Pluginy::NactiPluginy(std::set<string_t> include, std::set<string_t> exclude)
{
	NactiNastaveni();

    NactiSlozkuPluginu();

    Root_ = std::make_shared<PluginyNode>();

    std::queue<std::shared_ptr<PluginyNode>> queue_;
    queue_.push(Root_);

    // nejdrive potrebujeme nacist pluginy ktere nejsou zmineny v konfiguracnim souboru a povesit je pod roota
    for(auto& i : SlozkaPluginyBezOtce_)
    {
        if(include.size() != 0)
        {
            if(include.find(i.first) == include.end()) // nasli jsme plugin, ktery nebyl definovan v include
            {
                LogSystem::VratLog().Zapis("pluginy", "Plugin \"" + i.first
                                           + "\" nebyl definovan v include mezi ostatnimi a proto bude ignorovan!");
                continue;
            }
        }
        if(exclude.find(i.first) != exclude.end()) // plugin byl nalezen v exclude
        {
            LogSystem::VratLog().Zapis("pluginy", "Plugin \"" + i.first
                                       + "\" byl definovan v exclude pluginech a proto bude ignorovan!");
            continue;
        }

        auto plugin_node = std::make_shared<PluginyNode>(Root_, i.first, i.second);
        if(plugin_node->Provadet() == true)
        {
            Root_->PridejPotomka(plugin_node);
            queue_.push(plugin_node);
            PocetPluginu_++;
        }
    }
    SlozkaPluginyBezOtce_.clear();


    // zpracujeme pluginy, kterym byli konfigurovani rodice v nastaveni
    while(!queue_.empty())
    {
        auto actual = queue_.front();
        queue_.pop();

        // podle nazvu vrcholu na zacatku fronty nacteme vrcholy, ktere pod nej maji by poveseny
        auto pair_it = NactenaHierarchie_.equal_range(actual->VratJmeno());

        if(pair_it.first == pair_it.second){ continue; } // aby se zbytecne nepokracovalo, pokud se nic nenalezlo

        for(auto i = pair_it.first; i != pair_it.second; ++i)
        {
            if(include.size() != 0)
            {
                if(include.find(i->second) == include.end()) // nasli jsme plugin, ktery nebyl definovan v include
                {
                    LogSystem::VratLog().Zapis("pluginy", "Plugin \"" + i->second
                                               + "\" nebyl definovan v include mezi ostatnimi a proto bude ignorovan!");
                    continue;
                }
            }
            if(exclude.find(i->second) != exclude.end()) // plugin byl nalezen v exclude
            {
                LogSystem::VratLog().Zapis("pluginy", "Plugin \"" + i->second
                                           + "\" byl definovan v exclude pluginech a proto bude ignorovan!");
                continue;
            }


            // zkontrolujeme jestli plugin s danym jmenem existuje ve slozce pluginu
            auto plugin_it = SlozkaPluginySOtcem_.find(i->second);
            if(plugin_it == SlozkaPluginySOtcem_.end()) // neexistuje... pokracujeme ve vypoctu dalsi iteraci for
            {
                LogSystem::VratLog().Zapis("pluginy",
                                           "V nastaveni hierarchie_pluginu byl nalezen odkaz na neexistujici plugin: "
                                           + i->second);
                continue;
            }


            // vytvorime vrchol pluginu, povesime ho pod aktualni nacteny vrchol a soupneme ho do fronty
            auto plugin_node = std::make_shared<PluginyNode>(actual, i->second, plugin_it->second);
            if(plugin_node->Provadet() == true)
            {
                SlozkaPluginySOtcem_.erase(plugin_it);
                actual->PridejPotomka(plugin_node);
                queue_.push(plugin_node);
                PocetPluginu_++;
            }
            else{ continue; }
        }

        // aby jsme nemuseli zjistovat ktere pluginy jsme uz zpracovali a ktere ne, tak je rovnou smazame
        NactenaHierarchie_.erase(pair_it.first, pair_it.second);
    }


    // zpracujeme pluginy z hierarchie ktere nemaji rodice a musi byt poveseni pod roota
    for(auto i = NactenaHierarchie_.begin(); i != NactenaHierarchie_.end(); ++i)
    {
        if(include.size() != 0)
        {
            if(include.find(i->second) == include.end()) // nasli jsme plugin, ktery nebyl definovan v include
            {
                LogSystem::VratLog().Zapis("pluginy", "Plugin \"" + i->second
                                           + "\" nebyl definovan v include mezi ostatnimi a proto bude ignorovan!");
                continue;
            }
        }
        if(exclude.find(i->second) != exclude.end()) // plugin byl nalezen v exclude
        {
            LogSystem::VratLog().Zapis("pluginy", "Plugin \"" + i->second
                                       + "\" byl definovan v exclude pluginech a proto bude ignorovan!");
            continue;
        }


        // zkontrolujeme jestli plugin s danym jmenem existuje ve slozce pluginu
        auto plugin_it = SlozkaPluginySOtcem_.find(i->second);
        if(plugin_it == SlozkaPluginySOtcem_.end()) // neexistuje... pokracujeme ve vypoctu dalsi iteraci for
        {
            LogSystem::VratLog().Zapis("pluginy",
                                       "V nastaveni hierarchie_pluginu byl nalezen odkaz na neexistujici plugin: "
                                       + i->second);
            continue;
        }


        // vytvorime vrchol pluginu, povesime ho pod koren a soupneme ho do fronty
        auto plugin_node = std::make_shared<PluginyNode>(Root_, i->second, plugin_it->second);
        if(plugin_node->Provadet() == true)
        {
            SlozkaPluginySOtcem_.erase(plugin_it);
            Root_->PridejPotomka(plugin_node);
            queue_.push(plugin_node);
            PocetPluginu_++;

            LogSystem::VratLog().Zapis("pluginy", "Otec pluginu \"" + i->second
                               + "\" nebyl nalezen, plugin bude povesen pod koren a nebude mit zadne zavislosti.");
        }
        else{ continue; }
    }
    NactenaHierarchie_.clear();

    //Root_->Print(0);
}
