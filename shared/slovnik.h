#ifndef SLOVNIK_H
#define SLOVNIK_H

#include "titulek.h"

/**
 * Trida zajistujici zakladni slovnik, umoznuje zadat typ klice, podle ktereho se pak vyhledava a datovy typ, nesouci informace o klici.
 * V zakladu jsou informace ukladany do std::map<KeyType,DataType>
 * Vychozim datovym typem je void, ke kteremu je napsana specializace.
 */
template<typename KeyType, typename DataType = void>
class Slovnik
{
public:
    /** Vsechny defaultni metody jsou povoleny. */
    Slovnik(){}
    Slovnik(const Slovnik& source) = default;
    Slovnik& operator=(const Slovnik& source) = default;
#ifdef _WIN32
	/* Defaultni move metody neumi Visual Studio 2013 aktualne prelozit a bohuzel ani generovat... */
#else
	Slovnik(Slovnik&& source) = default;
	Slovnik& operator=(Slovnik&& source) = default;
#endif

    /**
     * Pridava klic a hodnotu do slovniku.
     * @param klic
     * @param hodnota
     */
    void PushBack(KeyType klic, DataType hodnota)
    {
        Data_.insert(std::make_pair(klic,hodnota));

        return;
    }
    /**
     * Hleda ve slovniku hodnotu parametru klic.
     * @param klic vyhledavana hodnota
     * @return true, pokud byla hodnota klice nalezena, false jinak
     */
    bool Hledej(const KeyType& klic)
    {
        typename std::map<KeyType,DataType>::iterator nalezeno = Data_.find(klic);

        if(nalezeno == Data_.end()){ return false; }
        else{ return true; }
    }
    /**
     * Najde ve slovniku klic a vrati jeho poradove cislo.
     * @param klic vyhledavana hodnota
     * @return bezznaminkove cislo, reprezentujici poradove cislo klice ve slovniku
     * @note Pokud byla po volani HledejId() zavolana funkce PushBack(), pak je vracene ID neplatne.
     */
    size_t HledejId(const KeyType& klic)
    {
        typename std::map<KeyType,DataType>::iterator nalezeno = Data_.find(klic);

        if(nalezeno != Data_.end())
        {
            return nalezeno - Data_.begin();
        }
        else
        {
            size_t vysledek = size_t() - 1;
            return vysledek;
        }
    }
    /**
     * Vraci referenci na prvek na zadane pozici.
     */
    DataType& at(size_t pozice)
    {
        return Data_.at(pozice);
    }
    /**
     * Vraci pocet zaznamu ve slovniku.
     */
    size_t Pocet()
    {
        return Data_.size();
    }
private:
    std::map<KeyType, DataType> Data_;
};

/**
 * Castecna specializace tridy Slovnik, ktera nema DataType
 */
template<typename KeyType>
class Slovnik<KeyType, void>
{
public:
    Slovnik() : Setrideno_(false)
    {}
    Slovnik(const Slovnik& source) = default;
    Slovnik& operator=(const Slovnik& source) = default;
#ifdef _WIN32
	/* Defaultni move metody neumi Visual Studio 2013 aktualne prelozit a bohuzel ani generovat... */
#else
	Slovnik(Slovnik&& source) = default;
	Slovnik& operator=(Slovnik&& source) = default;
#endif

    void PushBack(KeyType hodnota)
    {
        Setrideno_ = false;

        Data_.push_back(hodnota);

        return;
    }
    bool Hledej(const KeyType& hodnota)
    {
        Setrid();

        bool nalezeno = std::binary_search(Data_.begin(),Data_.end(),hodnota);

        if(nalezeno == true){ return true; }
        else{ return false; }
    }
    size_t HledejId(const KeyType& hodnota)
    {
        Setrid();

        auto nalezeno = std::lower_bound(Data_.begin(),Data_.end(),hodnota);

        if(nalezeno != Data_.end() && !(hodnota < *nalezeno))
        {
            return nalezeno - Data_.begin();
        }
        else
        {
            size_t vysledek = size_t() - 1;
            return vysledek;
        }
    }
    KeyType at(size_t pozice)
    {
        return Data_.at(pozice);
    }
    size_t Pocet()
    {
        return Data_.size();
    }
private:
    void Setrid()
    {
        if(Setrideno_ == false)
        {
            std::sort(Data_.begin(),Data_.end());

            Setrideno_ = true;
        }
        return;
    }

    bool Setrideno_;
    std::vector<KeyType> Data_;
};

#endif // SLOVNIK_H
