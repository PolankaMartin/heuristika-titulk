#include "titulky.h"

Titulky::Titulky()
{
}

Titulek& Titulky::operator[](size_t pozice)
{
    return Data_.at(pozice);
}

Titulek& Titulky::at(size_t pozice)
{
    return Data_.at(pozice);
}

size_t Titulky::Pocet()
{
    return Data_.size();
}

void Titulky::PushBack(Titulek pridej)
{
    Data_.push_back(pridej);
    return;
}

void Titulky::Reset()
{
    Data_.clear();
}
