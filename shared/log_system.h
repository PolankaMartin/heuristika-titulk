#ifndef LOG_SYSTEM_H
#define LOG_SYSTEM_H

#include "logger.h"

#ifdef _WIN32
#ifdef EXPORT_HEURISTIKA_BUILD
#define EXPORT_LOG __declspec(dllexport)
#else
#define EXPORT_LOG __declspec(dllimport)
#endif
#else
#define EXPORT_LOG
#endif

/**
 * Singleton zpristupnujici tridu Log, nelze ho zkonstruovat.
 */
class LogSystem
{
public:
	LogSystem() = delete;
	LogSystem(LogSystem const&) = delete;
	void operator=(LogSystem const&) = delete;

	/**
	 * Vraci referenci na staticky objekt Log.
	 */
	static Log EXPORT_LOG & VratLog()
	{
		static Log Log_;
		return Log_;
	}
};

#endif // LOG_SYSTEM_H
