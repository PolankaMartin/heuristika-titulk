#ifndef CHYBY_H
#define CHYBY_H

#include "chyba.h"

class Engine;

typedef std::vector<Chyba> VectorChyb;

/**
 * Kontejner obsahujici pole objektu Chyba a souvisejici operaci na nem.
 * Pole chyb neni nijak razeno, poradi je tedy dano casem prichodu.
 * Veskere operace na tomto kontejneru jsou uzamceny pomoci zamku a trida je tedy thread-safe.
 */
class Chyby
{
public:
    /** Defaultni konstruktor je povolen. */
    Chyby();
    Chyby(const Chyby& source) = default;
    Chyby& operator=(const Chyby& source) = default;
#ifdef _WIN32
	/* Defaultni move metody neumi Visual Studio 2013 aktualne prelozit a bohuzel ani generovat... */
#else
	Chyby(Chyby&& source) = default;
	Chyby& operator=(Chyby&& source) = default;
#endif

    /**
     * Prida objekt Chyba na konec pole chyb.
     * @param pridej objekt typu Chyba, ktery pokud je pridan, pak uz ho nelze smazat.
     */
    void PushBack(Chyba pridej);
    /**
     * Vraci referenci na chybu, chova se jako funkce at(). Pri prekroceni hranice pole je vyhozena vyjimka.
     * @param pozice bezznamenkove cislo, urcujici pozici v poli
     * @return reference na objekt Chyba umisteneho v poli chyb
     */
    Chyba& operator[](size_t pozice);
    /**
     * Klasicka funkce at zpristupnujici objekt Chyba na zadane pozici
     * @param pozice bezznamenkove cislu urcujici pozici v poli
     * @return reference na objekt Chyba umisteneho v poli chyb
     */
    Chyba& at(size_t pozice);
    /**
     * Vraci pocet objektu typu Chyba umistenych v poli.
     */
    size_t Pocet();
private:
    friend class Engine;
    void Reset();

    VectorChyb Data_;
    std::mutex DataMutex_;
};

#endif // CHYBY_H
