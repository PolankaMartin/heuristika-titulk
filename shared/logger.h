#ifndef LOGGER_H
#define LOGGER_H

#include "titulek.h"
#include "timer.h"

class Engine;

/**
 * Trida, ktera zajistuje udrzbu a zapis do log souboru. Povolit samotne logovani lze pouze z tridy Engine.
 * Log je dostupny ze singletonu LogSystem, protoze musi byt stejny pro vsechny soucasti programu.
 */
class Log
{
public:
    /** Defaultni konstruktor je povolen. */
    Log();
    /** Zakazany copy konstruktor. */
    Log(const Log& source) = delete;
    /** Zakazana copy operator= */
    Log& operator=(const Log& source) = delete;
    /** Zakazan move konstruktor. */
    Log(Log&& source) = delete;
    /** Zakazan move operator= */
    Log& operator=(Log&& source) = delete;

    /**
     * Pokud je zapnuto logovani, pak zapise informace zadane parametry do logovaciho souboru.
     * @param soucast definuje ze ktere soucasti programu/ze ktereho pluginu byla zavolana funkce Zapis()
     * @param data samotna zprava, ktera se ma zapsat do logu
     */
    void Zapis(const string_t& soucast, const string_t &data);
private:
    friend class Engine;
    void Otevrit(const string_t& soubor);
    string_t CasNaString(double cas);

    std::ofstream LogSoubor_;
    Timer<> Timer_;
    std::mutex WriteMutex_;
};

#endif // LOGGER_H
