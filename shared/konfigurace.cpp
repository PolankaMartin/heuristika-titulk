#include "konfigurace.h"

Konfigurace::Konfigurace() : DefaultNastaveni_("./nastaveni/"),
    AltKonfSoubor_(""), AltKonfigurace_("")
{
}

const string_t& Konfigurace::VratDefaultSlozku()
{
    return DefaultNastaveni_;
}

const string_t& Konfigurace::VratAltSoubor()
{
    return AltKonfSoubor_;
}

const string_t& Konfigurace::VratAltKonfiguraci()
{
    return AltKonfigurace_;
}
