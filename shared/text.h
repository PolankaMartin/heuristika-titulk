#ifndef TEXT_H
#define TEXT_H

#include "titulek.h"

/**
 * Singleton ktery zpristupnuje ceske std::locale tride Text.
 * Byl vytvoren proto, ze kontrukce std::locale pomoci boostu, je relativne pomala a neni pripustne ji delat v kazde volane funkci.
 */
class TextData
{
public:
    static std::locale& VratUTF8CsLocale()
    {
        static std::locale Data_ = boost::locale::generator().generate("cs_CZ.utf8");
        return Data_;
    }

private:
    TextData() = delete;
    TextData(TextData const&) = delete;
    void operator=(TextData const&) = delete;
};

/**
 * Jmenny prostor, ktery zajistuje uzitecne funkce pro praci s textem.
 * Tuto tridu nelze zkonstruovat.
 */
class Text
{
public:
    Text() = delete;
    Text(const Text& source) = delete;
    Text& operator=(const Text& source) = delete;
    Text(Text&& source) = delete;
    Text& operator=(Text&& source) = delete;

    /**
     * Pro dve zadana slova spocita jejich levenshteinovu vzdalenost.
     * Pocita se slovy kodovanymi v UTF8, ktere pokud nejsou validni, tak vraci std::string::npos.
     * Je zapojena drobna heuristika, pokud vzdalenost dvou slov prekroci hodnotu parametru max_distance, pak je funkce hned ukoncena a vraci string_t::npos.
     * @param word_1 prvni slovo
     * @param word_2 druhe slovo
     * @param max_distance maximalni prijatelna vzdalenost dvou zadanych slov
     * @return vraci vzdalenost dvou zadanych slov
     */
    static size_t LevenshteinDistance(const string_t& word_1, const string_t& word_2, size_t max_distance = string_t::npos);

    /**
     * Pomoci algoritmu KMP projde cely text a hleda v nem jehlu, pro kterou najde vsechny jeji vyskyty.
     * Funkce je UTF8 validni a vraci pozici UTF8 znaku.
     * @param text text, ve kterem se vyhledava
     * @param jehla vyhledavany text
     * @return pole iteratoru na jednotlive pocatky vyskytu textu jehla
     */
    static std::vector<string_t::const_iterator> UTF8HledejVsechny(const string_t& text, const string_t& jehla);

    /**
     * Z malych pismen v parametru slovo udela velka od pozice zacatek v zadane delce.
     * Pocita se slovem ve validnim UTF8 kodovani.
     * @note Funkce pocita s plnou UTF8 tabulkou a umi prevest vsechna pismena, diky tomu je jeji provadeni pomale!
     * @param slovo const reference na slovo, ktere se bude transformovat
     * @param zacatek urcuje pozici od ktere se bude transformovat, vychozi je 0
     * @param delka urcuje delku transformace, vychozi je maximalni hodnota typu size_t
     * @return vraci vysledne pretransformovane slovo
     */
    static string_t NaVelka(const string_t& slovo, size_t zacatek = 0, size_t delka = string_t::npos);
    /**
     * Z velkych pismen v parametru slovo udela mala od pozice zacatek v urcene delce.
     * Pocita se slovem ve validnim UTF8 kodovani.
     * @note Funkce pocita s plnou UTF8 tabulkou a umi prevest vsechna pismena, diky tomu je jeji provadeni pomale!
     * @param slovo const reference na slovo, ktere se bude transformovat
     * @param zacatek urcuje pozici od ktere se bude transformovat, vychozi hodnota je 0
     * @param delka urcuje delku transformace, vychozi je maximalni hodnota typu size_t
     * @return vraci vysledne pretransformovane slovo ve stringu
     */
    static string_t NaMala(const string_t& slovo, size_t zacatek = 0, size_t delka = string_t::npos);

    /**
     * Zjisti jestli je pismeno na urcite pozici velke.
     * Pocita se slovem ve validnim UTF8 kodovani.
     * @note Znaky, ktere k sobe nemaji definovany velikostni protejsek jsou povazovana zaroven za mala a velka
     * @param slovo text, kde se bude zjistovat velikost pismena
     * @param pozice urcuje jake pismeno bude prosetrovano
     * @return pokud je pismeno na pozici velke, pak vraci true, jinak false
     */
    static bool JeVelke(const string_t& slovo, size_t pozice);
    /**
     * Zjisti jestli je pismeno na urcite pozici male.
     * Pocita se slovem ve validnim UTF8 kodovani.
     * @note Znaky, ktere k sobe nemaji definovany velikostni protejsek jsou povazovana zaroven za mala a velka
     * @param slovo text, kde se bude zjistovat velikost pismena
     * @param pozice urcuje jake pismeno bude prosetrovano
     * @return pokud je pismeno male, pak vraci true, jinak false
     */
    static bool JeMale(const string_t& slovo, size_t pozice);

    /**
     * Zjisti jestli na konci daneho slova bylo nalezeno nejake ukonceni vety (tecka, otaznik, apod).
     * @param slovo prozkoumavany text
     * @return true, pokud byl nalezen konec vety, false v opacnem pripade
     */
    static bool JeKonecVety(const string_t& slovo);
    /**
     * Ze zadaneho slova odstrani libovolne ukonceni vety.
     * @param slovo zdrojovy text (const reference => nebude menen)
     * @return vraci slovo bez pripadneho konce vety
     */
    static string_t OdstranKonecVety(const string_t& slovo);

    /**
     * Zjistuje jestli v danem parametru slovo byl na konci nalezen konec bloku (tecka, carka, otaznik, apod.)
     * @param slovo prozkoumavany text
     * @return true, pokud byl nalezen konec bloku, jinak false
     */
    static bool JeKonecBloku(const string_t& slovo);
    /**
     * Ze zadaneho parametru slovo odstrani konec bloku a vrati novou stringovou promennou.
     * @param slovo const reference na slovo ze ktereho se bude odstranovat konec bloku
     * @return text slova bez konce bloku
     */
    static string_t OdstranKonecBloku(const string_t& slovo); // odstranuje tecky, carky, ... a uvozovky na konci slova

    /**
     * Rekne jestli v zadanem slove byl nalezen zacatek bloku (uvozovky na zacatku slova apod.)
     * @param slovo prozkoumavane slovo
     * @return true, pokud byl nalezen zacatek bloku, jinak false
     */
    static bool JeZacatekBloku(const string_t& slovo);
    /**
     * Odstrani zacatek bloku ze zacatku slova.
     * @param slovo zdrojove slovo
     * @return novy string bez zacatku bloku
     */
    static string_t OdstranZacatekBloku(const string_t& slovo); // odstranuje uvozovky ci pomlcky

    /**
     * Zjisti jestli slovo obsahuje na konci tri tecky
     * @param slovo
     * @return true pokud jsou na konci tri tecky, false jinak
     */
    static bool ObsahujeTriTecky(const string_t& slovo);
    /**
     * Smaze ze zadaneho slova tri tecky na konci, pokud byly v puvodnim slove
     * @param slovo
     * @return text bez tri tecek na konci, pokud je puvodni slovo obsahovalo
     */
    static string_t OdstranTriTecky(const string_t& slovo);

private:
    enum TransfInternalFlags{ Upper, Lower };

    static string_t TransformaceInternal(TransfInternalFlags flag, const string_t& slovo,
                                         size_t zacatek = 0, size_t delka = string_t::npos);
    static bool VelikostPismenaInternal(TransfInternalFlags flag, const string_t& slovo, size_t pozice);
};

#endif // TEXT_H
