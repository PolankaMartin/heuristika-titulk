#ifndef PLUGIN_H
#define PLUGIN_H

#include "titulek.h"
#include "chyby.h"
#include "slovnik.h"
#include "log_system.h"
#include "konf_system.h"
#include "text.h"
#include "titulky.h"

/**
 * Abstraktni objekt sjednocujici rozhrani jednotlivych pluginu.
 * Obsahuje nektera zakladni data spolecna pro vsechny pluginy a uzitecne funkce, ktere lze pouzit.
 */
class Plugin
{
public:
    /** Defaultni konstruktor a vsechny copy a move metody jsou zakazany. */
    Plugin() = delete;
    Plugin(const Plugin& source) = delete;
    Plugin& operator=(const Plugin& source) = delete;
    Plugin(Plugin&& source) = delete;
    Plugin& operator=(Plugin&& source) = delete;

    /**
     * Jediny pouzitelny konstruktor k vytvoreni objektu.
     * @param nazev_chyby obecny nazev chyby, kterou plugin vyhledava
     * @param nazev_pluginu nazev pluginu, tedy ta cast nazvu nacteneho souboru mezi lib---.so
     */
    Plugin(string_t nazev_chyby, string_t nazev_pluginu);
    virtual ~Plugin(){}

    /**
     * Abstraktni metoda, kterou je nutne definovat v kazdem pluginu.
     * Zajistuje samotne provedeni akce na jednotlivych titulcich.
     * @param titulek reference na titulek, na kterem bude provedena detekce
     * @param chyby reference na kontejner chyb do ktereho se v pripade potreby pridavaji chyby
     * @return vraci 0 v pripade uspechu, jinak 1, apod.
     */
    virtual int Run(Titulek& titulek, Chyby& chyby) = 0;
    /**
     * Abstrakni metoda, kterou si definuji samotne pluginy. Jeji funkcnost by mela byt resetovani pluginu do stavu tesne po nacteni konfigurace.
     * To je nutne hlavne pro pluginy, ktere si udrzuji urcity kontext i mezi jednotlivym volanim funkce Run().
     * Tato funkce by mohla byt nutna, kdyby se s aktualne nactenou instanci pluginu mel analyzovat dalsi (jiny) titulkovy soubor.
     */
    virtual void Reset() = 0;
    /**
     * Vraci const referenci na obecny nazev chyby, kterou plugin detekuje.
     */
    const string_t& NazevChyby(){ return NazevChyby_; }
    /**
     * Vraci const referenci na nazev aktualniho objektu Plugin.
     */
    const string_t& NazevPluginu(){ return NazevPluginu_; }
    /**
     * Vrati true, pokud lze na objektu Plugin zavolat funkci Run, pokud nelze pak vrati false.
     */
    bool Provadet(){ return Provadet_; }
    /**
     * Vola se pokud na aktualnim Pluginu nelze provadet funkci Run, napriklad v dusledku nenactene konfigurace.
     */
    void ErrorNastaveni();

    /**
     * Zajistuje sjednocene nacitani konfigurace z jednoho ze tri zdroju (default, alt.soubor, alt.konfigurace).
     * Poradi nacitani: serializovana konfigurace, alternativni soubor, defaultni slozka.
     * @param nazev_nastaveni jmeno konfiguracniho souboru/hlavniho objektu v konfiguraci
     * @param log_text informace o nazvu nastaveni a jmene pluginu, ktery toto nastaveni potrebuje
     * @return vraci strom, bez hlavniho XML objektu pluginu
     */
    static boost::property_tree::ptree NactiStromNastaveniObecne(string_t nazev_nastaveni, string_t log_text);

    /**
     * Zajistuje sjednocene nacitani konfigurace z jednoho ze tri zdroju (default, alt.soubor, alt.konfigurace).
     * Poradi nacitani: serializovana konfigurace, alternativni soubor, defaultni slozka.
     * @param nazev_pluginu pokud se nacita nastaveni, kde hlavni objekt v XML ma stejny nazev jako plugin, pak tento parametr neni treba zadavat. Tento argument je pritomen hlavne pokud by v nejakem pluginu bylo treba nacist nastaveni jineho
     * @return vraci strom, bez hlavniho XML objektu pluginu
     */
    boost::property_tree::ptree NactiStromNastaveni(string_t nazev_pluginu = "");

private:
    string_t NazevChyby_; // Obecny nazev chyby, kterou plugin hleda
    string_t NazevPluginu_; // Nazev samotne knihovny (tedy ta cast mezi lib***.so)
    bool Provadet_; // Urcuje zda se plugin bude provadet ci ne (pokud ne, pak se ani nepridava do kontejneru pluginu)
        // pokud je nastaveno na false, neni zarucena konzistence pluginu...
protected:
    Log& Log_;
};

typedef Plugin* vytvor_t();
typedef void znic_t(Plugin*);

#endif // PLUGIN_H
