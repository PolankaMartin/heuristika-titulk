#include "logger.h"

Log::Log()
{
    LogSoubor_.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    Timer_.Start();
}

void Log::Otevrit(const string_t &soubor)
{
    try
    {
        LogSoubor_.open(soubor);
    }
    catch(std::ifstream::failure e)
    {
        std::cerr << "Fatalni chyba pri otevirani log souboru: " << e.what() << std::endl;
        exit(1);
    }
    return;
}

void Log::Zapis(const string_t& soucast, const string_t &data)
{
    WriteMutex_.lock();

    if(LogSoubor_.is_open() == true)
    {
        try
        {
            LogSoubor_ << std::resetiosflags(std::ios::right) << std::setiosflags(std::ios::left) << std::setw(6) << (Timer_.Time() / 1000) << " ms; ";
            LogSoubor_ << std::setiosflags(std::ios::right) << std::setw(22) << soucast << "; " << data << std::endl;
        }
        catch(std::ifstream::failure e)
        {
            std::cerr << "Fatalni chyba pri zapisovani do log souboru: " << e.what() << std::endl;
            exit(1);
        }
    }

    //std::cout << "Byl pouzit LogSystem s adresou: " << this << "; Na miste: " << soucast << std::endl;
    //std::cout << "    Stav otevreni: " << LogSoubor_.is_open() << std::endl;

    WriteMutex_.unlock();

    return;
}

string_t Log::CasNaString(double cas)
{ // kdyby nahodou bylo treba lepsiho formatu casu
    string_t vysledek;

    vysledek = std::to_string(cas);

    return vysledek;
}
